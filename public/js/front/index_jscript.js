function submitForm(data, route,next_tab)
{
    removeMsgValidation();
    $.ajax({
        url      : route,
        headers  : {'X-CSRF-TOKEN': csrf_token },
        type     : 'POST',
        data     : data,
        timeout  : 5000,
        success  : function(response)
        {
            msgSuccess('Datos actualizados correctamente.');
            focusTab(next_tab);
        },
        error: function(xhr, textStatus, thrownError)
        {
            if(xhr.status=422)
                addMsgValidation(xhr);
            else
                messagesxhr(xhr,textStatus);
    
               // mensajesxhr( xhr, textStatus );
        }
    });
}

function focusTab(tab)
{
    $('#pdf_ficha').hide();
    if(tab== 'tab6')
    {
       $('#pdf_ficha').show(1000);
       $('html, body').animate( { scrollTop : 1000 }, 1000 );
    }
    else
    {
        $('div.tab-pane').removeClass('active show');
        $('div#'+tab).addClass('active show');
    
        let number = tab.replace('tab','');
        $('ul#menu li').removeClass('active');
       
        for(i =1;i<=number;i++)
        {   
            
            $('li#step'+i).addClass('active');
        }
       
        $('html, body').animate( { scrollTop : 0 }, 700 );
    }
   
}

function getDataEstudent()
{
   
  $.ajax({
    url         : base_url+'/fichaEstudiante',
    headers     : {'X-CSRF-TOKEN': csrf_token },
    type        : 'GET',
    timeout     : 5000,
    success     : function(response)
    {
       
      loadDataEstudent(response.estudent,response.file);
      //loadDataFile(response.file);
    },
    error: function(xhr, textStatus, thrownError)
    {
        location.reload();
    }
  });
}

function loadDataEstudent(estudent,file)
{
    //datos tab1
    $('#nombres').val(estudent.nombres);
    $('#apellidos').val(estudent.apellidos);
    $('#dni').val(estudent.dni);
    $('#nro_celular').val(estudent.nro_celular);
    $('#email').val(estudent.email);
    $('#sexo').val(estudent.sexo).trigger('change');;
    $('#fecha_nacimiento').val(estudent.fecha_nacimiento);
    $('#estado_civil').val(estudent.estado_civil);
    $('#escuela').val(estudent.escuela.nombre); 
    
    if(file != null)
    {
       
        if(file.contacto_emergencia != null )
        {
            let contacto = JSON.parse(file.contacto_emergencia);
            $('input#nombre_contacto').val(contacto.nombre_contacto);
            $('input#telefono_contacto').val(contacto.telefono_contacto);
        }  
        
    
       
        if(file.ubicacion_domicilio != null)
        {
            let ubicacion = JSON.parse(file.ubicacion_domicilio);
            $('select#distrito_vivienda').val(ubicacion.id_distrito).trigger('change.select2');
            $('input#anexo_vivienda').val(ubicacion.anexo_vivienda);
            $('input#direccion_vivienda').val(ubicacion.direccion_vivienda);
        }
        
        //DAtos tab2
        if(file.lugar_nacimiento)
        {
            console.log('cargando lugar de nacimiento');
            let nacimiento = JSON.parse(file.lugar_nacimiento);
            $('select#departamento_nacimiento').val(nacimiento.id_departamento).trigger('change');
            $('select#provincia_nacimiento').val(nacimiento.id_provincia).trigger('change');
            $('select#distrito_nacimiento').val(nacimiento.id_distrito).trigger('change');
            $('input#comunidad_nacimiento').val(nacimiento.comunidad);
        }
      
        if(file.lugar_procedencia )
        {
            let procedencia = JSON.parse(file.lugar_procedencia);
            $('select#departamento_procedencia').val(procedencia.id_departamento).trigger('change');
            $('select#provincia_procedencia').val(procedencia.id_provincia).trigger('change');
            $('select#distrito_procedencia').val(procedencia.id_distrito).trigger('change');
            $('input#comunidad_procedencia').val(procedencia.comunidad);
        }
        

        $('select#primer_idioma').val(file.primer_idioma).trigger('change');
        $('select#limitacion_fisica').val(file.limitacion_fisica).trigger('change');
        $('select#vive_con').val(file.vive_con).trigger('change');

        if(file.transporte)
        {
            let transporte = JSON.parse(file.transporte);
            $('input#tiempo_horas').val(transporte.tiempo_horas);
            $('input#tiempo_minutos').val(transporte.tiempo_minutos);
            $('select#medio_transporte').val(transporte.medio_transporte).trigger('change');
        }
      

        //DAtos tab3
        if(file.vivienda != null)
        {
            let vivienda = JSON.parse(file.vivienda);
            $('select#vivienda_tenencia').val(vivienda.tenencia).trigger('change');
            $('select#vivienda_tipo').val(vivienda.tipo).trigger('change');
            $('select#vivienda_material').val(vivienda.material).trigger('change');
            $('select#nro_habitaciones').val(vivienda.nro_habitaciones).trigger('change');
        }
      

        let servicios = file.servicios_vivienda;
            if(servicios)
            {
                servicios = servicios.split(',');
                //marcar servicios
                $('#content_servicios input').each(function()
                {
                    let $input = $(this);

                    servicios.forEach(function(element) 
                    {
                        if($input.val()===element)
                            $input.prop('checked',true);
                    });
                });
            }
            
        let posesiones = file.posesiones;
            if(posesiones)
            {
                posesiones = posesiones.split(',');
                //marcar posesiones
                $('#content_posesiones input').each(function()
                {
                    let $input = $(this);
        
                    posesiones.forEach(function(element) 
                    {
                        if($input.val()===element)
                            $input.prop('checked',true);
                    });
                });
            }

        //DAtos tab4
        if(file.situacion_familiar)
        {
            let familia = JSON.parse(file.situacion_familiar);
            $('select#situacion_padres').val(familia.situacion_padres).trigger('change');
            $('select#situacion_orfandad').val(familia.situacion_orfandad).trigger('change');
        }  
      
        if(file.violencia_politica)
        {
            let terrorismo = JSON.parse(file.violencia_politica);
            $('select#victima_terrorismo').val(terrorismo.victima_terrorismo).trigger('change');
            $('input#resolucion_terrorismo').val(terrorismo.resolucion_terrorismo);
        }  
       
        if(file.alimentacion)
        {
            let alimentacion = JSON.parse(file.alimentacion);
            $('select#desayuno').val(alimentacion.desayuno).trigger('change');
            $('select#almuerzo').val(alimentacion.almuerzo).trigger('change');
            $('select#cena').val(alimentacion.cena).trigger('change');
        }
       

        //seleccionar radios
        if(file.economia_familiar)
        {
            let economia = JSON.parse(file.economia_familiar);
            $('#grupo_dependencia input').each(function()
            {
                let $input = $(this);
                if($input.attr('value')==economia.dependencia_economica)
                    $input.attr('checked',true);
            });
    
            $('#grupo_monto input').each(function()
            {
                let $input = $(this);
                if($input.attr('value')==economia.ingreso_familiar)
                    $input.attr('checked',true);
            });
        } 
       
        //cargando gastos
        if(file.gastos)
        {
            let gastos = JSON.parse(file.gastos);
            $('#list_gastos tr').each(function()
            {
                let $tr = $(this);
                gastos.forEach(function(element) 
                {
                    if($tr.children('td.concepto').text() == element.concepto)
                    $tr.children('td').children('input').val(element.monto);
                });
            });  
        }  
      
        //seguro de salud
        if(file.seguro_salud)
        {
            let seguro = JSON.parse(file.seguro_salud);
            $('select#seguro_salud').val(seguro.seguro_salud).trigger('change');
            $('input#especificacion_salud').val(seguro.especificacion_salud);
        }
      

        if(file.salud_familia)
        {
            let salud = JSON.parse(file.salud_familia);
            $('#list_salud_familiar tr').each(function()
            {
                let $tr = $(this);
                salud.forEach(function(element) 
                {
                    if($tr.children('td.miembro').text() == element.miembro)
                    $tr.children('td').children('select').val(element.enfermedad).trigger('change');
                });
            });  
    
        }
       
        if(file.discapacidad_familiar)
        {
            let discapacidad = JSON.parse(file.discapacidad_familiar);
            $('select#discapacidad').val(discapacidad.discapacidad).trigger('change');
            $('input#especificacion_discapacidad').val(discapacidad.especificacion_discapacidad);
            $('select#carne_conadis').val(discapacidad.carne_conadis).trigger('change');
            $('input#nro_conadis').val(discapacidad.nro_conadis);
            $('input#resolucion_conadis').val(discapacidad.resolucion_conadis);
    
        }
       
        //
        if(file.embarazo)
        {
            let embarazo = JSON.parse(file.embarazo);
            $('select#embarazo').val(embarazo.embarazo).trigger('change');
            $('select#meses_embarazo').val(embarazo.meses_embarazo).trigger('change');
        }  
    

        //datos tab5
       
        if(file.antecedente_academico != null)
        {
            let antecedente = JSON.parse(file.antecedente_academico);
        
            $('select#semestre').val(antecedente.semestre).trigger('change');
            $('select#modalidad_ingreso').val(antecedente.modalidad_ingreso).trigger('change');
            $('input#ultimo_promedio').val(antecedente.ultimo_promedio);
            $('input#penultimo_promedio').val(antecedente.penultimo_promedio);
            $('input#total_creditos').val(antecedente.total_creditos);
        }  
    }    
}

function loadDataSelect2( $selector,url) 
{ 
  $.ajax({
    url     : url,
    headers : {'X-CSRF-TOKEN': csrf_token },
    type    : 'GET',
    timeout : 5000,
    async   : false,
    success : function (response)
    { 
        data = $.map(response.data, function (obj) {
          return tmp = {
          'id' 	  : obj.id,
          'text'  : obj.nombre
          };
        });
        $selector.select2('destroy');
        
        var config 		= {};
        config.data 	= data;
        $selector.select2(config);
    },
    error: function(xhr, textStatus, thrownError)
    {
      //messagesXhr(xhr, textStatus);
      console.log('error al cargar datos select2 '+textStatus);
      loadDataSelect2( $selector,url);
    }
  });
	
}

function getListaComposicionFamiliar()
{
    let list_composicion = [];
        $('#list_composicion tr').each(function()
        {
            let row  = $(this); 
            let item =  {
                            "parentesco":        row.find('select.parentesco').val(),
                            "nombre":            row.find('input.nombre').val(),
                            "edad":              row.find('input.edad').val(),
                            "dni":               row.find('input.dni').val(),
                            "estado_civil":      row.find('select.estado_civil').val(),
                            "centro_trabajo":    row.find('input.centro_trabajo').val(),
                            "grado_instruccion": row.find('select.grado_instruccion').val(),
                        };
            list_composicion.push(item);
        });

    return list_composicion;    
}

function getListaNucleoFamiliar()
{
    let list_nucleo = [];
    $('#list_nucleo tr').each(function()
    {
        let row  = $(this); 
        let item =  {
                        "nombre":            row.find('input.nombre').val(),
                        "edad":              row.find('input.edad').val(),
                        "dni":               row.find('input.dni').val(),
                        "estado_civil":      row.find('select.estado_civil').val(),
                        "centro_trabajo":    row.find('input.centro_trabajo').val(),
                        "grado_instruccion": row.find('select.grado_instruccion').val(),
                    };
        
        list_nucleo.push(item);
    });

    return list_nucleo;
}

function getListaGastosEstudiante()
{
    let list = [];
    $('#list_gastos tr').each(function()
    {
        let row  = $(this); 
        let item =  {
                        "nro":      row.find('td.nro').text(),
                        "concepto": row.find('td.concepto').text(),
                        "monto":    row.find('input').val(),
                    };
        
        list.push(item);
    });

    return list;
}

function getListaSaludFamiliar()
{
    let list = [];
    $('#list_salud_familiar tr').each(function()
    {
        let row  = $(this); 
        let item =  {
                        "miembro": row.find('td.miembro').text(),
                        "enfermedad":   row.find('select').val(),
                    };
        
        list.push(item);
    });

    return list;
}

function addMsgValidation(xhr)
{
    let messages = xhr.responseJSON;
    
    msgError('Se encontraron errores, revice el formulario ó revice su conexión a internet');
    $.each(messages, function (ind, elem) 
    {
        let index = ind.split('.',1);
        if(index[0] == 'lista_composicion')
        {
            $('#alert_composicion').show();
            $('#msg_composicion').text(elem);
        }
        if(index[0] == 'lista_nucleo')
        {
            $('#alert_nucleo').show();
            $('#msg_nucleo').text(elem);
        }
       
        $('#'+ind).addClass('is-invalid');
        $('#'+ind+'-error').text(elem);
    });
}

function removeMsgValidation()
{
    $('input').removeClass('is-invalid');
    $('select').removeClass('is-invalid');
    $('em').text('');
}

function createtableRowNucleo($selector)
{ 
    $selector.append(
        $('<tr>').append(
            $('<td>').append(
                $('<input>',{
                    'type'  : 'text',
                    'class' : 'form-control input-sm nombre',
                    'required' : 'required',
                    'placeholder': 'Ingrese nombre completo'
                })
            ),
            $('<td>').append(
                $('<input>',{
                    'type'     : 'number',
                    'class'    : 'form-control input-sm edad',
                    'required' : 'required',
                    'min'      :'0',
                    'width'    : '80px',
                    'placeholder': 'Edad'
                })
            ),
            $('<td>').append(
                $('<input>',{
                    'type'     : 'number',
                    'class'    : 'form-control input-sm dni',
                    'min'      : '0',
                    'width'    : '120px',
                    'placeholder': 'Dni'
                })
            ),
            $('<td>').append(
                $('<select>',{
                    'class'    : 'form-control input-sm estado_civil',
                    'required' : 'required'
                }).append(
                    $('<option>',{
                        'value' : 'Soltero',
                    }).text('Soltero'),
                    $('<option>',{
                        'value' : 'Casado'
                    }).text('Casado'),
                    $('<option>',{
                        'value' : 'Conviviente'
                    }).text('Conviviente'),
                    $('<option>',{
                        'value' : 'Divorciado'
                    }).text('Divorciado'),
                    $('<option>',{
                        'value' : 'Viudo'
                    }).text('Viudo'),
                    $('<option>',{
                        'value' : 'Fallecido'
                    }).text('Fallecido')   
                )
            ),
            $('<td>').append(
                $('<input>',{
                    'type'  : 'text',
                    'class' : 'form-control input-sm centro_trabajo',
                 
                    'placeholder' : 'Ingrese centrotro de Estudio/Trabajo'
                })
            ),
            $('<td>').append(
                $('<select>',{
                    'class'    : 'form-control input-sm grado_instruccion',
                    'required' : 'required'
                }).append(
                    $('<option>',{
                        'value' : 'Ninguno',
                    }).text('Ninguno'),
                    $('<option>',{
                        'value' : 'Superior universitario',
                    }).text('Superior universitario'),
                    $('<option>',{
                        'value' : 'Superior técnico'
                    }).text('Superior técnico'),
                    $('<option>',{
                        'value' : 'Secundaria'
                    }).text('Secundaria'),
                    $('<option>',{
                        'value' : 'Primaria'
                    }).text('Primaria')   
                )
            ),
            $('<td>').append(
                $('<button>',{
                    'type'  : 'button',
                    'class' : 'btn btn-xs btn-danger',
                    'title' : 'Eliminar'    
                }).append(
                    $('<i>',{
                        'class' : 'fa fa-trash fa-lg'
                    })
                )
            )
        )
    );
}

function createtableRowComposicion($selector)
{ 
    $selector.append(
        $('<tr>').append(
            $('<td>').append(
                $('<select>',{
                    'class'    : 'form-control input-sm parentesco',
                    'required' : 'required'
                }).append(
                    $('<option>',{
                        'value' : '',
                    }).text('Seleccione'),
                    $('<option>',{
                        'value' : 'Padre',
                    }).text('Padre'),
                    $('<option>',{
                        'value' : 'Madre',
                    }).text('Madre'),
                    $('<option>',{
                        'value' : 'Hermano',
                    }).text('Hermano'),
                    $('<option>',{
                        'value' : 'Yo'
                    }).text('Yo'),
                    $('<option>',{
                        'value' : 'Otros'
                    }).text('Otros')
                )
            ),
            $('<td>').append(
                $('<input>',{
                    'type'     : 'text',
                    'class'    : 'form-control input-sm nombre',
                    'required' : 'required',
                    'placeholder' : 'Ingrese nombre completo'
                })
            ),
            $('<td>').append(
                $('<input>',{
                    'type'     : 'number',
                    'class'    : 'form-control input-sm edad',
                    'required' : 'required',
                    'min'      : '0',
                    'width'    : '80px',
                    'placeholder' : 'Ingrese edad'
                })
            ),
            $('<td>').append(
                $('<input>',{
                    'type'     : 'number',
                    'class'    : 'form-control input-sm dni',
                  
                    'min'      : '0',
                    'width'    : '120px',
                    'placeholder' : 'Ingrese dni'
                })
            ),
            $('<td>').append(
                $('<select>',{
                    'class'    : 'form-control input-sm estado_civil',
                    'required' : 'required'
                }).append(
                    $('<option>',{
                        'value' : 'Soltero',
                    }).text('Soltero'),
                    $('<option>',{
                        'value' : 'Casado'
                    }).text('Casado'),
                    $('<option>',{
                        'value' : 'Conviviente'
                    }).text('Conviviente'),
                    $('<option>',{
                        'value' : 'Divorciado'
                    }).text('Divorciado'),
                    $('<option>',{
                        'value' : 'Viudo'
                    }).text('Viudo'),
                    $('<option>',{
                        'value' : 'Fallecido'
                    }).text('Fallecido')   
                )
            ),
            $('<td>').append(
                $('<input>',{
                    'type'  : 'text',
                    'class' : 'form-control input-sm centro_trabajo',
                
                    'placeholder' : 'Ingrese centro de Trabajo/Estudio'
                })
            ),
            $('<td>').append(
                $('<select>',{
                    'class'    : 'form-control input-sm grado_instruccion',
                    'required' : 'required'
                }).append(
                    $('<option>',{
                        'value' : 'Ninguno',
                    }).text('Ninguno'),
                    $('<option>',{
                        'value' : 'Superior universitario',
                    }).text('Superior universitario'),
                    $('<option>',{
                        'value' : 'Superior técnico'
                    }).text('Superior técnico'),
                    $('<option>',{
                        'value' : 'Secundaria'
                    }).text('Secundaria'),
                    $('<option>',{
                        'value' : 'Primaria'
                    }).text('Primaria')   
                )
            ),
            $('<td>').append(
                $('<button>',{
                    'class' : 'btn btn-xs btn-danger',
                    'title' : 'Eliminar'
                }).append(
                    $('<i>',{
                        'class' : 'fa fa-trash fa-lg'
                    })
                )
            ),
        )
    );
}