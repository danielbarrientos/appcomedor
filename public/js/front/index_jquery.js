$(document).ready(function(){

    var $slcDistritoVivienda        = $('select#distrito_vivienda');
    var $slcDepartamentoProcedencia = $('select#departamento_procedencia');
    var $slcProvinciaProcedencia    = $('select#provincia_procedencia');
    var $slcDistritoProcedencia     = $('select#distrito_procedencia');
    var $slcDepartamentoNacimiento  = $('select#departamento_nacimiento');
    var $slcProvinciaNacimiento     = $('select#provincia_nacimiento');
    var $slcSexo                    = $('select#sexo');
    var $slcDiscapacidad            = $('select#discapacidad');
    var $slcCarneConadis            = $('select#carne_conadis');
    var $slcEmbarazo                = $('select#embarazo');
    var $slcMesesEmbarazo           = $('select#meses_embarazo');
    var $slcDistritoNacimiento      = $('select#distrito_nacimiento');
    var $btnAddRowComposiscion      = $('button#btn_fila_composicion');
    var $btnAddRowNucleo            = $('button#btn_fila_nucleo');
    var $listNucleo                 = $('tbody#list_nucleo');
    var $listcomposicion            = $('tbody#list_composicion');
    var $btnReturnTab1              = $('button#return_tab1');
    var $btnReturnTab2              = $('button#return_tab2');
    var $btnReturnTab3              = $('button#return_tab3');
    var $btnReturnTab4              = $('button#return_tab4');
    var $slcVictimaTerrorismo       = $('select#victima_terrorismo');
    var $txtResolucionTerrorismo    = $('input#resolucion_terrorismo');
    var $slcSeguroSalud             = $('select#seguro_salud');
    var $txtEspecificacionSalud     = $('input#especificacion_salud');

    
    getDataEstudent();
    $slcDistritoVivienda.select2();
    $slcDepartamentoProcedencia.select2();
    $slcProvinciaProcedencia.select2();
    $slcDistritoProcedencia.select2();
    $slcDepartamentoNacimiento.select2();
    $slcProvinciaNacimiento.select2();
    $slcDistritoNacimiento.select2();

    // ******|cargar datos tab por defecto |*******
   
    //cargar datos de tab 1
    let route = base_url+'/fichaEstudiante/listDistritos/'+'030100'; 
    loadDataSelect2($slcDistritoVivienda,route );
    //cargando datos de tab2
    $slcDepartamentoProcedencia.on('change',function()
    {
        let id_departamento = $(this).val();
        let route = base_url+'/fichaEstudiante/listProvincias/'+id_departamento; 
        $slcProvinciaProcedencia.html('<option value="" selected disabled><- Seleccione -></option>');
        loadDataSelect2($slcProvinciaProcedencia,route );
    });

    $slcProvinciaProcedencia.on('change',function()
    {
        let id_provincia = $(this).val();
        let route = base_url+'/fichaEstudiante/listDistritos/'+id_provincia; 
        $slcDistritoProcedencia.html('<option value="" selected disabled><- Seleccione -></option>');
        loadDataSelect2($slcDistritoProcedencia,route );
    });

    $slcDepartamentoNacimiento.on('change',function()
    {
        let id_departamento = $(this).val();
        let route = base_url+'/fichaEstudiante/listProvincias/'+id_departamento; 
        $slcProvinciaNacimiento.html('<option value="" selected disabled><- Seleccione -></option>');
        loadDataSelect2($slcProvinciaNacimiento,route );
    });

    $slcProvinciaNacimiento.on('change',function()
    {
        let id_provincia = $(this).val();
        let route = base_url+'/fichaEstudiante/listDistritos/'+id_provincia; 
        $slcDistritoNacimiento.html('<option value="" selected disabled><- Seleccione -></option>');
        loadDataSelect2($slcDistritoNacimiento,route );
    });
    // ******|fin cargar datos tab por defecto |*******
    
    $('form.general').on('submit', function(e)
    {
        e.preventDefault();

        let $form = $(this);
        let actual_tab = $form.attr('id');
            actual_tab = parseInt(actual_tab.replace('form_tab',''));
        let next_tab   = actual_tab+1;   
            next_tab   = 'tab'+next_tab;

        let data = $form.serialize(); 
        let route = $form.attr('action');       

        submitForm(data,route,next_tab);
    });

    $('form.especial').on('submit', function(e)
    {
        e.preventDefault();
        $('#alert_composicion').hide();
        $('#alert_nucleo').hide();
        let $form = $(this);
        let actual_tab = $form.attr('id');
            actual_tab = parseInt(actual_tab.replace('form_tab',''));
        let next_tab   = actual_tab+1;   
            next_tab   = 'tab'+next_tab;

        let lista_composicion = [];
            lista_composicion = getListaComposicionFamiliar();
       
        let lista_nucleo = [];
            lista_nucleo = getListaNucleoFamiliar();

        let lista_gastos = [];
            lista_gastos = getListaGastosEstudiante();
            
        let lista_salud = [];
            lista_salud = getListaSaludFamiliar();    
      
        let data =  { 
                        lista_composicion: lista_composicion,
                        lista_nucleo: lista_nucleo,
                        lista_gastos: lista_gastos,
                        lista_salud: lista_salud,
                        situacion_padres: $form.find('#situacion_padres').val(),
                        situacion_orfandad : $form.find('#situacion_orfandad').val(),
                        victima_terrorismo : $form.find('#victima_terrorismo').val(),
                        resolucion_terrorismo : $form.find('#resolucion_terrorismo').val(),
                        desayuno : $form.find('#desayuno').val(),
                        almuerzo : $form.find('#almuerzo').val(),
                        cena : $form.find('#cena').val(),
                        dependencia_economica: $form.find('input:radio[name=dependencia_economica]:checked').val(),
                        ingreso_familiar: $form.find('input:radio[name=ingreso_familiar]:checked').val(),
                        seguro_salud: $form.find('#seguro_salud').val(),
                        especificacion_salud: $form.find('#especificacion_salud').val(),
                        discapacidad: $form.find('#discapacidad').val(),
                        especificacion_discapacidad: $form.find('#especificacion_discapacidad').val(),
                        carne_conadis: $form.find('#carne_conadis').val(),
                        nro_conadis: $form.find('#nro_conadis').val(),
                        resolucion_conadis: $form.find('#resolucion_conadis').val(),
                        embarazo: $form.find('select#embarazo').val(),
                        meses_embarazo: $form.find('select#meses_embarazo').val()
                    };
            
        let route = $form.attr('action');       

        submitForm(data,route,next_tab);
    });

    $btnAddRowNucleo.on('click', function()
    {
       
        createtableRowNucleo($listNucleo);
    });
    
    $listNucleo.on('click','button' , function()
    {
        let row = $(this).parents('tr');
            row.remove();
    });

    $btnAddRowComposiscion.on('click', function()
    {
        createtableRowComposicion($listcomposicion );
    });

    $listcomposicion .on('click','button' , function()
    {
        let row = $(this).parents('tr');
            row.remove();
    });

    $btnReturnTab1.on('click',function()
    {
        focusTab('tab1');
    });

    $btnReturnTab2.on('click',function()
    {
        focusTab('tab2');
    });

    $btnReturnTab3.on('click',function()
    {
        focusTab('tab3');
    });

    $btnReturnTab4.on('click',function()
    {
        focusTab('tab4');
    });

    $slcSexo.on('change', function()
    {
        let option = $(this).val();
        
        if(option == 'M')
            $('fieldset#embarazo').hide();
        else
            $('fieldset#embarazo').show();
    });

    $slcDiscapacidad.on('change', function()
    {
        let option = $(this).val();
        
        if(option == 'Ninguna')
        {
            $('#especificacion_discapacidad').prop('readonly',true);
            $('#especificacion_discapacidad').prop('required',false);
        } 
        else
        {
            $('#especificacion_discapacidad').prop('readonly',false);
            $('#especificacion_discapacidad').prop('required',true);
        }
           
    });
    $slcCarneConadis.on('change', function()
    {
        let option = $(this).val();
        
        if(option == 'No')
        {
            $('#nro_conadis').prop('readonly',true);
            $('#nro_conadis').prop('required',false);
            $('#resolucion_conadis').prop('required',false);
            $('#resolucion_conadis').prop('readonly',true);
        } 
        else
        {
            $('#nro_conadis').prop('readonly',false);
            $('#nro_conadis').prop('required',true);
            $('#resolucion_conadis').prop('required',true);
            $('#resolucion_conadis').prop('readonly',false);
        }
           
    });
    $slcEmbarazo.on('change', function()
    {
        let option = $(this).val();
        
        if(option == 'No')
        {
            $slcMesesEmbarazo.val('');
            $slcMesesEmbarazo.prop('required',false);
            $slcMesesEmbarazo.hide();
            
        } 
        else
        {
            $slcMesesEmbarazo.val('1');
            $slcMesesEmbarazo.prop('required',true);
            $slcMesesEmbarazo.show();
        }
           
    });
    $slcVictimaTerrorismo.on('change', function()
    {
        let option = $(this).val();
        
        if(option == 'No')
        {
            $txtResolucionTerrorismo.val('');
            $txtResolucionTerrorismo.prop('required',false);
            $txtResolucionTerrorismo.prop('readonly',true); 
        } 
        else
        {
            $txtResolucionTerrorismo.prop('required',true);
            $txtResolucionTerrorismo.prop('readonly',false);
        }
           
    });

    $slcSeguroSalud.on('change', function()
    {
        let option = $(this).val();
        
        if(option == 'Otros')
        {
            $txtEspecificacionSalud.prop('required',true);
            $txtEspecificacionSalud.prop('readonly',false); 
        } 
        else
        {
            $txtEspecificacionSalud.prop('required',false);
            $txtEspecificacionSalud.prop('readonly',true); 
        }
           
    });

    $('#alert_composicion').hide();
    $('#alert_nucleo').hide();

    // function mostrarImagenSeleccionada(input) 
    // {
    //     if (input.files && input.files[0]) 
    //     {
    //         var reader = new FileReader();
    //         reader.onload = function (e) 
    //         {
    //             $('#foto').attr('src', e.target.result);
    //         }
    //      reader.readAsDataURL(input.files[0]);
    //     }
    // }

    // $("#nombre_foto").change(function(){
    //     mostrarImagenSeleccionada(this);
    // });
});