jQuery(document).ready(function () 
{
    var $frmRegistro           = $('#frmRegistro');
    var $modalRegistro         = $('#modalRegistro');
    var $frmBuscar             = $('#frmBuscar');
    var $modalEliminar         = $('#modalEliminar');
    var $frmEliminar           = $('#formEliminar');
    var $frmBusquedaIndividual = $('#frmBusquedaIndividual');
    var $btnAbrirModal         = $('#btnAbrirModal');
    var $slcCampoBusqueda      = $('#campo_busqueda');
    var $contenedorLista       = $('#contenedor_lista');
    var $slcVerEscuela         = $('select#ver_escuela')

    caragador($('#contenedor_lista'));
    listarBeneficiarios();
    //Eventos
    $btnAbrirModal.click(function () 
    {
      
        resetform();
        $modalRegistro.modal("show");
        $frmRegistro.attr('action', base_url + '/beneficiario/insertar');

    });
    Promedio
    $slcCampoBusqueda.on('change', function () {
        let campo = $(this).val();
        $('#parametro').attr('name', campo);
    });

    $contenedorLista.on('click', '#lista_beneficiarios button.eliminar', function () 
    {
        let id = $(this).parents('tr').attr('id');
        let nombre = $(this).parent('td').siblings('td.nombre_completo').text();
        $('b#nombreBeneficiario').text(nombre);
        $('input#id_beneficiario').val(id);
        $modalEliminar.modal("show");
    });

    $contenedorLista.on('change', '#lista_beneficiarios select', function () 
    {
        let id = $(this).parents('tr').attr('id'); 
        let nuevo_tipo = $(this).val();
        let datos = {id_beneficiario:id,tipo:nuevo_tipo};
        cambiarTipo(datos);  
        
    });
   

    $frmEliminar.on("submit", function (e) 
    {
        e.preventDefault();
        let route = $frmEliminar.attr('action');
        let datos = $frmEliminar.serialize();
        eliminarBeneficiario(route,datos);

    });

    $frmRegistro.on("submit", function (e) 
    {
        e.preventDefault();
        cargadorBoton(true);
     
        var datos = $frmRegistro.serialize();
        var route = $frmRegistro.attr('action');

        registroBeneficiario(route, datos);
    });

    $frmBuscar.on('submit', function (e) 
    {
        e.preventDefault();

        let route = $(this).attr('action');
        let datos = $(this).serialize();

        listarBeneficiarios(route, datos);
    });

    $frmBusquedaIndividual.on('submit', function (e) 
    {
        e.preventDefault();

        let route = $(this).attr('action');
        let datos = $(this).serialize();

        buscarEstudiante(route, datos);
    });

    $(document).on('click', '.pagination li a', function (e) 
    {
        e.preventDefault();

        route = $(this).attr('href');

        listarBeneficiarios(route);
    });

    $('button.btn-refresh').on('click', function () 
    {
        location.reload();
    });

    $('#buscar_codigo').keypress(function (event) 
    {
       // e.preventDefault();
        let keycode = (event.keyCode ? event.keyCode : event.which);
        
        if(keycode == '32')
        {
            let dni = $.trim($('#dni').val());
            if(dni.length ==8 )
                $frmRegistro.trigger('submit'); 
        }
        else
            return true;
    });

    $slcVerEscuela.on('change', function()
    {
        let id_escuela = $(this).val();
        location.href = base_url+'/beneficiario/escuela/'+id_escuela;
    });
});

//funciones

function buscarBeneficiario(dni) 
{
    $.ajax({
        url: base_url + '/estudiante/' + dni + '/buscar',
        headers: { 'X-CSRF-TOKEN': csrf_token },
        type: 'GET',
        datatype: 'json',
        timeout: 10000,
        success: function (respuesta) 
        {
            cargarDatosEstudiante(respuesta);
        },
        error: function (xhr, textStatus, thrownError) 
        {
            messagesxhr(xhr, textStatus);
        }

    });
}
function cambiarTipo(datos) {
    $.ajax({
        url      : base_url + '/beneficiario/cambiarTipo',
        headers  : { 'X-CSRF-TOKEN': csrf_token },
        type     : 'POST',
        datatype : 'json',
        data     : datos,
        timeout  : 10000,
        success  : function (respuesta) 
        {
           // msgSuccess('Cambio de tipo realizado correctamente.');
        },
        error: function (xhr, textStatus, thrownError) 
        {
            messagesxhr(xhr, textStatus);
        }

    });
}

function buscarEstudiante(url, datos) 
{
    $.ajax({
        url: url,
        headers: { 'X-CSRF-TOKEN': csrf_token },
        type: 'GET',
        datatype: 'json',
        data: datos,
        timeout: 10000,
        success: function (respuesta) {
            cargarDatosEstudiante(respuesta);
        },
        error: function (xhr, textStatus, thrownError) {
            messagesxhr(xhr, textStatus);
        }

    });
}

function eliminarBeneficiario(url, datos) 
{
    $.ajax({
        url      : url,
        headers  : { 'X-CSRF-TOKEN': csrf_token },
        type     : 'POST',
        datatype : 'json',
        data     : datos,
        timeout  : 10000,
        success  : function (respuesta) 
        {
            $('#modalEliminar').modal("hide");
            $('#frmBuscar').trigger('submit');
            msgSuccess(respuesta.message);
            

        },
        error: function (xhr, textStatus, thrownError) 
        {
            messagesxhr(xhr, textStatus);
        }

    });
}

function cargarDatosEstudiante(data) 
{
    resetform();
    let datos = data.datos;

    if (datos == null)
        msgWarning('No se encontro ningun estudiante con el codigo ingresado');
    else 
    {
        if(data.beneficiario == null)
        {
            if (datos.id_escuela != $('#id_escuela').val())
                msgInfo( datos.apellidos + ' ' + datos.nombres +' pertenece a la escuela de: ' + datos.escuela.nombre);
            if (datos.matricula == 0)
                msgInfo( datos.apellidos + ' ' + datos.nombres + ' no se encuentra matriculado');
            if (datos.id_escuela == $('#id_escuela').val() && datos.matricula == 1) 
            {
                $('#contenedor_documentos').show();
                $('#link_historial').attr('href',base_url+'/estudiante/'+datos.codigo+'/historial');
                $('#link_ficha').show();
                if(data.ficha == true)
                {
                    $('#link_ficha').show();
                    $('#link_ficha').attr('href',base_url+'/estudiante/'+datos.dni+'/ficha');
                }
                else
                {
                    $('#link_ficha').hide();
                    $('#link_ficha').attr('href','');
                }
                $('#dni').val(datos.dni);
                $('#nombre_escuela').val(datos.escuela.nombre);
                $('#nombre_completo').val(datos.apellidos + ' ' + datos.nombres);
                $('#btnGuardar').attr('disabled',false);
            }
        }
        else
            msgInfo( datos.apellidos + ' ' + datos.nombres+' ya se encuentra registrado en la lista de beneficiarios de la escuela de: '+datos.escuela.nombre);       
    }
}

function listarBeneficiarios(route = null, datos = null) {
    if (route == null)
        route = base_url + '/beneficiario/escuela/' + id_escuela_actual;

    $.ajax({
        url: route,
        headers: { 'X-CSRF-TOKEN': csrf_token },
        type: 'GET',
        datatype: 'json',
        data: datos,
        timeout: 10000,
        success: function (respuesta) {
            $('#contenedor_lista').html(respuesta.vista);
        },
        error: function (xhr, textStatus, thrownError) {
            messagesxhr(xhr, textStatus);
        }

    });
}

function registroBeneficiario(route, datos) 
{
    $.ajax({
        url      : route,
        headers  : { 'X-CSRF-TOKEN': csrf_token },
        type     : 'POST',
        datatype : 'json',
        data     : datos,
        timeout  : 10000,
        success  : function (respuesta) 
        {
            $('#frmBuscar').trigger('submit');
            if(respuesta.status == 'success')
                msgSuccess(respuesta.message);
            else
                msgWarning(respuesta.message);
        },
        error: function (xhr, textStatus, thrownError) 
        {
            messagesxhr(xhr, textStatus);
        },
        complete: function () 
        {
            cargadorBoton(false);
            resetform();
        },
    });
}

function cargadorBoton(estado) {
    if (estado == false) {
        $('#btnGuardar').children('i').removeClass("fa fa-spinner fa-spin");
        $('#btnGuardar').attr("disabled", false);
    }
    else {
        $('#btnGuardar').children('i').addClass("fa fa-spinner fa-spin");
        $('#btnGuardar').attr("disabled", true);
    }
}

function resetform()
{
    $('#contenedor_documentos').hide();
    $('#buscar_codigo').val('');
    $('#buscar_codigo').focus();
    $('#dni').val('');
    $('#nombre_escuela').val('');
    $('#nombre_completo').val('');
    $('#btnGuardar').attr('disabled',true);
}


