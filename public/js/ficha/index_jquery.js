jQuery(document).ready(function() 
{
    var $slcDepartamentoProcedencia = $('select#id_departamento_procedencia');
    var $formProcedencia            = $('#formProcedencia');
    var $formModalidad              = $('#formModalidad');
    var $formEdad                   = $('#formEdad');

    $slcDepartamentoProcedencia.select2();
   

    //eventos
  
    $formProcedencia.on('submit',function(e)
    {
        e.preventDefault();
        let data = $(this).serialize();
        let route = $(this).attr('action');

        generarReporteProcedencia(route,$('#tab_tabla_procedencia') ,'grafico_bar_procedencia', data);
    });

    $formModalidad.on('submit',function(e)
    {
        e.preventDefault();
        let data = $(this).serialize();
        let route = $(this).attr('action');

        generarReporteModalidad(route,$('#tab_tabla_modalidad') ,'grafico_bar_modalidad', data);
    });

    $formEdad.on('submit',function(e)
    {
        e.preventDefault();
        let data = $(this).serialize();
        let route = $(this).attr('action');

        generarReporteEdad(route,$('#tab_tabla_edad') ,'grafico_bar_edad', data);
    });

    $('button#btnImprimirSeccionEdad').click(function()
    {

        let hoja;
        //edad promedio
        
        if ($('#ul_nav_edad li a.active').attr("href") === "#tab_tabla_edad") 
        {   
            hoja = $('.tab-content #tab_tabla_edad .hoja_report').clone();
        }
        if ($('#ul_nav_edad li a.active').attr("href") === "#tab_grafico_edad") 
        {
            hoja = $('.tab-content #tab_grafico_edad .hoja_report').clone();
        }
       
        print_area(hoja);
    });
    $('button#btnImprimirSeccionModalidad').click(function()
    {

        let hoja;
        //modalidad de ingreso
        if ($('#ul_nav_modalidad li a.active').attr("href") === "#tab_tabla_modalidad") 
        {
            hoja = $('.tab-content #tab_tabla_modalidad .hoja_report').clone();
        }
        if ($('#ul_nav_modalidad li a.active').attr("href") === "#tab_grafico_modalidad") 
        {
            hoja = $('.tab-content #tab_grafico_modalidad .hoja_report').clone();
        }
    
        print_area(hoja);
    });

    $('button#btnImprimirSeccionProcedencia').click(function()
    {
        let hoja;
    
        //lugar de procedencia
        if ($('#ul_nav_procedencia li a.active').attr("href") === "#tab_tabla_procedencia") 
        {
            hoja = $('.tab-content #tab_tabla_procedencia .hoja_report').clone();
        }
        if ($('#ul_nav_procedencia li a.active').attr("href") === "#tab_grafico_procedencia") 
        {
            hoja = $('.tab-content #tab_grafico_procedencia .hoja_report').clone();
        }
      
        print_area(hoja);
    });
});