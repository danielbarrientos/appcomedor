

function generarReporteProcedencia(url, $contenedor_tabla,$contenedor_grafico,datos) 
{ 
    $.ajax({
        url     : url,
        headers : {'X-CSRF-TOKEN': csrf_token },
        type    : 'GET',
        timeout : 5000,
        data    : datos,
        success : function (respuesta)
        { 
            let datos = respuesta.datos;
            $contenedor_tabla.html(datos['vista']);

            let data      =  formatoDatosPorcentaje(datos['query']);
            let titulo    = ' (Reporte de estudiantes por lugar de procedencia )';
            let subtitulo = 'DEPARTAMENTO: Todos';

            if(datos['departamento'] != null)
            {
                subtitulo = 'Departamento: '+datos['departamento'].nombre;
            }
           
            graficoBarraPorcentaje($contenedor_grafico, data['bar'],titulo,subtitulo)
        
        },
        error: function(xhr, textStatus, thrownError)
        {
            msgWarning('No se pudierin cargar los datos, inténtelo otra vez.');
        }
    });
}

function generarReporteModalidad(url, $contenedor_tabla,$contenedor_grafico,datos) 
{ 
    $.ajax({
        url     : url,
        headers : {'X-CSRF-TOKEN': csrf_token },
        type    : 'GET',
        timeout : 5000,
        data    : datos,
        success : function (respuesta)
        { 
            let datos = respuesta.datos;
            $contenedor_tabla.html(datos['vista']);

            let data      =  formatoDatosPorcentaje(datos['query']);
            let titulo    = ' (Reporte de estudiantes por modalidad de ingreso)';
           
            graficoBarraPorcentaje($contenedor_grafico, data['bar'],titulo)
        
        },
        error: function(xhr, textStatus, thrownError)
        {
            msgWarning('No se pudieron cargar los datos, inténtelo otra vez.');
        }
    });
}

function generarReporteEdad(url, $contenedor_tabla,$contenedor_grafico,datos) 
{ 
    $.ajax({
        url     : url,
        headers : {'X-CSRF-TOKEN': csrf_token },
        type    : 'GET',
        timeout : 5000,
        data    : datos,
        success : function (respuesta)
        { 
            let datos = respuesta.datos;
            $contenedor_tabla.html(datos['vista']);

            let data      =  formatoDatosPromedio(datos['query']);
            let titulo    = ' (Reporte de estudiantes por edad)';
           
            graficoBarraCantidad($contenedor_grafico, data['bar'],titulo)
        
        },
        error: function(xhr, textStatus, thrownError)
        {
            msgWarning('No se pudieron cargar los datos, inténtelo otra vez.');
        }
    });
}


function graficoBarraPorcentaje(contenedor,datos,title=null,subtitle=null)
{
    configHighcharts();

    Highcharts.chart(contenedor, {
        chart: {
            type: 'column'
        },
        title: {
            text: 'OFICINA DE BIENESTAR UNIVERSITARIO <br>'+title
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Porcentaje total'
            }
    
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
    
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> del total<br/>'
        },
    
        series: [
            {
                name: "Estudiantes",
                colorByPoint: true,
                data: datos
            }
        ]
    });
}

function graficoBarraCantidad(contenedor,datos,title=null,subtitle=null)
{
    configHighcharts();

    Highcharts.chart(contenedor, {
        chart: {
            type: 'column'
        },
        title: {
            text: 'OFICINA DE BIENESTAR UNIVERSITARIO <br>'+title
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Edad promedio'
            }
    
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },
    
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
        },
    
        series: [
            {
                name: "Estudiantes",
                colorByPoint: true,
                data: datos
            }
        ]
    });
}

function print_area(area) 
{
    $('body').hide();
    $('body').after(area);
    window.focus();
    window.print();
    window.close();
    area.remove();
    $('body').show();
}

function formatoDatosPorcentaje(datos)
{   
    let datos_bar =[];
    let data=[];
    let cantidad_total= 0;
    //calculamos la cantidad total
  
    datos.forEach(function(grupo) 
    {
        let groupeddata = grupo.groupeddata;
        groupeddata.forEach(function(subgrupo)
        {
            cantidad_total +=  parseInt(subgrupo.cantidad, 10);;  
        });
    });

    datos.forEach(function(grupo) 
    {
        
        let groupeddata =[];
        let cantidad_subgrupo = 0;
        groupeddata = grupo.groupeddata;
        
        groupeddata.forEach(function(subgrupo)
        {
            cantidad_subgrupo +=  parseInt(subgrupo.cantidad, 10);;    
        });
       
            let porcentaje_grupo = Math.round((cantidad_subgrupo/cantidad_total)*100*100)/100;
            
            let item_bar =  {
                        "name": grupo.grupo,
                        "y"   : porcentaje_grupo,
                        "drilldown": null
                    };            

        datos_bar.push(item_bar);
       
    });
    data['bar'] = datos_bar;
    
    return data;
}

function formatoDatosPromedio(datos)
{   
    let datos_bar =[];
    let data=[];
     
    datos.forEach(function(grupo) 
    {
       
        let groupeddata =[];
        let cantidad_grupo = 0;
        let edad_acumulada = 0;
        groupeddata = grupo.groupeddata;
        
        groupeddata.forEach(function(subgrupo)
        {
            cantidad_grupo++;
            edad_acumulada +=  parseInt(subgrupo.promedio, 10);;    
        });
       
            let promedio_grupo = (edad_acumulada/cantidad_grupo);
            
            let item_bar =  {
                        "name": grupo.grupo,
                        "y"   : promedio_grupo,
                        "drilldown": null
                    };            

        datos_bar.push(item_bar);
       
    });
    data['bar'] = datos_bar;
    
    return data;
}