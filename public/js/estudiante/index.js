jQuery(document).ready(function() 
{
    var $frmRegistro      = $('#frmRegistro');
    var $modalRegistro    = $('#modalRegistro');
    var $frmBuscar        = $('#frmBuscar');
    var $btnAbrirModal    = $('#btnAbrirModal');
    var $slcCampoBusqueda = $('#campo_busqueda');
    var $contenedorLista  = $('#contenedor_lista');

    caragador($('#contenedor_lista'));
    listarEstudiantes();
    //Eventos
    $btnAbrirModal.click(function()
    {
        resetform(); 
        removeMsgValidation();
        $modalRegistro.modal("show");
        $frmRegistro.attr('action',base_url+'/estudiante/insertar');

    });

    $slcCampoBusqueda.on('change',function()
    {
        let campo = $(this).val();
        $('#parametro').attr('name',campo);
    });

    $contenedorLista.on('click' ,'#lista_estudiantes a.editar',function(e)
    {
        e.preventDefault();
        let dni = $(this).attr('id');
        $frmRegistro.attr('action',base_url+'/estudiante/'+dni+'/actualizar');
        buscarEstudiante(dni);
        $modalRegistro.modal("show");
    });

    $frmRegistro.on("submit", function(e)
    {
        e.preventDefault();
        cargadorBoton(true);
        removeMsgValidation();
        
        var datos = $frmRegistro.serialize();
        var route = $frmRegistro.attr('action');

        registroEstudiante(route,datos);
    });

    $frmBuscar.on('submit',function(e)
    {
        e.preventDefault();

        let route = $(this).attr('action');
        let datos = $(this).serialize();
        
        listarEstudiantes(route, datos);
    });

    $(document).on('click','.pagination li a', function(e){

        e.preventDefault();
        
        route = $(this).attr('href');
    
        listarEstudiantes(route);
    }); 
});

//funciones
function cargarDatosEstudiante(estudiante)
{
    removeMsgValidation();
    resetform(); 
    $("#codigo").val(estudiante.codigo);
    $("#codigo_rfid").val(estudiante.codigo_rfid);
    $('#dni').val(estudiante.dni);
    $('#apellidos').val(estudiante.apellidos);
    $('#nombres').val(estudiante.nombres);
    $('#escuela').val(estudiante.id_escuela);
    $('#matricula').val(estudiante.matricula);
    

}

function buscarEstudiante(dni)
{
    $.ajax({
        url      : base_url+'/estudiante/'+dni+'/buscar',
        headers  : {'X-CSRF-TOKEN': csrf_token },
        type     : 'GET',
        datatype : 'json',
        timeout  : 15000,  
        success  : function(respuesta)
        {
            cargarDatosEstudiante(respuesta.datos);
        },
        error: function(xhr, textStatus, thrownError)
        {    
            messagesxhr(xhr,textStatus);
        }

    });
}

function listarEstudiantes(route =null,datos = null)
{
    if( route == null)
        route = base_url+'/estudiante';

    $.ajax({
        url      : route,
        headers  : {'X-CSRF-TOKEN': csrf_token },
        type     : 'GET',
        datatype : 'json',
        data     : datos,
        timeout  : 15000,  
        success  : function(respuesta)
        {
           $('#contenedor_lista').html(respuesta.vista);
        },
        error: function(xhr, textStatus, thrownError)
        {    
            messagesxhr(xhr,textStatus);
        }

    });
}

function registroEstudiante(route,datos)
{
    $.ajax({
        url      : route,
        headers  : {'X-CSRF-TOKEN': csrf_token },
        type     : 'POST',
        datatype : 'json',
        data     : datos,
        timeout  : 10000,  
        success  : function(respuesta)
        {
             $('#frmBuscar').trigger('submit');
            if (respuesta.accion == 'edicion')
                $('#cerrarModalRegistro').trigger('click');
                
                resetform();

            msgSuccess(respuesta.mensaje);    
        },
        error: function(xhr, textStatus, thrownError)
        {    
            if(xhr.status=422)
                addMsgValidation(xhr);
            else
                messagesxhr(xhr,textStatus);
        },
        complete: function()
        {    
            cargadorBoton(false);
           
        },

    });
}

function cargadorBoton(estado)
{
    if (estado==false) {
        $('#btnGuardar').children('i').removeClass("fa fa-spinner fa-spin");
        $('#btnGuardar').attr("disabled", false);
    }
    else{
        $('#btnGuardar').children('i').addClass("fa fa-spinner fa-spin");
        $('#btnGuardar').attr("disabled", true);
    }
}

function addMsgValidation(xhr)
{
    let messages = xhr.responseJSON.errors;
    
    msgError('Se encontraron errores, revice el formulario');

    $.each(messages, function (ind, elem) 
    {  
        $('#'+ind).addClass('is-invalid');
        $('#'+ind+'-error').text(elem);
    });
}

function resetform()
{
    $("#codigo").val('')
    $('#dni').val('');//asiganr un valor
    $('#apellidos').val('');//asiganr un valor
    $('#nombres').val('');
    $('#escuelas').val('0');
    $('#matricula').val('0');
}

function removeMsgValidation()
{
    $('input').removeClass('is-invalid');
    $('select').removeClass('is-invalid');
    $('em').text('');
}