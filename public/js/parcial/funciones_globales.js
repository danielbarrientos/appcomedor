
function msgSuccess(mensaje){
 toastr.success(mensaje,"",{
    "timeOut":6000,
    "progressBar": true,
    "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

  })
}
 function msgError(mensaje){
 toastr.error(mensaje,"",{
    "timeOut":6000,
    "progressBar": true,
    "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

  })
}
function msgInfo(mensaje){
 toastr.info(mensaje,"",{
    "timeOut":6000,
    "progressBar": true,
    "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

  })
}
function msgWarning(mensaje){
 toastr.warning(mensaje,"",{
    "timeOut":6000,
    "progressBar": true,
    "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

  })
}
function alertDanger(mensaje){
  $("#mensajes").html("<div class=' alert alert-danger alert-dismissible ' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='close'><span aria-hidden='true'>&times;</span></button><strong>Se produjo un error:</strong><br>"+mensaje+"</div>");
}
function messagesxhr(xhr,textStatus)
{
    if (xhr.status == 0) 
    {
         msgError('Revice su conexión a internet');
    }
    else if (xhr.status==422) 
    {
         //addMsjValidacion(xhr);
    }
    else if (xhr.status==500) 
    {
        msgError('Error 500, se produjo un error en el servidor.');
    }
    else if (textStatus === 'timeout') 
    {
        msgError('El proceso excedio el limite de tiempo permitido.');
    }
    else
    {
        msgError('Ocurrio un error, intentelo otra vez');
    } 
    
    return xhr;
}

$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
      
});

function caragador($selector)
{
  $selector.html('<div style="height: 200px; width: 100%; text-align: center; padding-top: 50px;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><p>Cargando Datos</p></div>');
}

function configHighcharts()
{
  Highcharts.setOptions({
		lang: {
            
            viewFullscreen: "Ver pantalla completa",
            printChart: "Imprimir Gráfico",
            downloadPDF: "Descargar PDF",
            downloadJPEG: "Descargar JPEG",
            downloadPNG: "Descargar PNG",
            downloadSVG: "Descargar SVG",
            downloadXLS: "Descargar XLS",
            downloadCSV: "Descargar CSV",
            drillUpText: "◁ Volver a categorias",
            viewData: "Ver tabla",
            openInCloud: "Ver gráfico en la nube"
        }
    });
}



function config_datatable()
{
    let config = {
    language: {
        "decimal": ".",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }

  }

  return config;
}
//funcion que caraga los datos en el dataTable
function loadDataTable( selector, url , columns )
{
  selector.DataTable().destroy();

  let config = config_datatable();

  config.ajax =  {
    url     : url,
    type    : 'GET',
    dataSrc : function (json) {
      
      return json.data;
    },
    error: function(xhr, textStatus, thrownError)
    {
      //loadDataTable( selector, url , columns);
    }
  }
  config.rowId = 'id';
  config.columns = columns; 
  
  return selector.DataTable( config );
}

function sortTable(n,type) 
{
    
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
 
  table = document.getElementById("table-sortable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
 
  /*Make a loop that will continue until no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare, one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place, based on the direction, asc or desc:*/
      if (dir == "asc") {
        if ((type=="str" && x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) || (type=="int" && parseFloat(x.innerHTML) > parseFloat(y.innerHTML))) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if ((type=="str" && x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) || (type=="int" && parseFloat(x.innerHTML) < parseFloat(y.innerHTML))) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /*If no switching has been done AND the direction is "asc", set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}