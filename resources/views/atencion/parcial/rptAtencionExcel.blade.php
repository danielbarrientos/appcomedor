<br>
<style type="text/css">

.cell {
    background-color: #346AA6;
    color: #FFFFFF;
    font-size: 11;
    
}
th{
	font-size: 11;
}
.titulo{
	text-align: center;
}

tr td {
    background-color: #ffffff;
    font-size: 10;
}
tr > td {
    border: 1px solid #000000;
    
}
</style>
<tr ><th class="titulo" colspan="7" > CONTROL DE ATENCIÓN  {{$escuela->nombre}} {{Session::get('nombre_semestre')}}</th></tr>

<br>
@foreach ($periodosNoNulos as $index => $periodo)

	<table >
		@php
				$columnas=count(($conjuntoFechas[$index]))+3;
				$contador=1;
		@endphp
		
		<thead  >
			<tr >
				<th class="titulo"  colspan="{{$columnas}}"> PERIODO DE ATENCIÓN Nº {{$index+1}} 	</th>
			</tr>

			<tr class="cell">
				<th>#</th> 
				<th>DNI</th> 
				<th>APELLIDOS Y NOMBRES</th>
				<th>VENTA</th>
				@foreach ($conjuntoFechas[$index] as $fecha)
					<th width="5" class="m-0 p-0" ><p  class="texto-vertical-1 m-0 p-1 ">{{date("d/m", strtotime($fecha->dia))}}</p></th> 
				@endforeach
			</tr>
		</thead>
		<tbody>
				
				
				@foreach ($conjuntoComensales[$index] as $comensal)

				<tr>
					<td>{{$contador++}}</td>
					<td >{{$comensal['dni_estudiante']}}</td>
					<td width="40">{{$comensal['apellidos']}} {{$comensal['nombres']}}</td>
					<td> {{($comensal['tipo_venta']==1)? 'Regular':'Libre' }} </td>
					@foreach ($conjuntoFechas[$index] as $fecha)
						<td width="7">{{$comensal[$fecha->dia]}}</td> 
					@endforeach
				</tr>	
				@endforeach
				
			
		</tbody>
	</table>

@endforeach