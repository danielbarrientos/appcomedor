  <div class="card-box">
        <h4 class="m-t-0 header-title">Resumen de ventas</h4>
        <!--p class="text-muted font-13 m-b-10">
            Your awesome text goes here.
        </p-->
        <div class="table-responsive">
            <table class="table table-sm">
          <thead>
            <tr>
              <th>EAP</th>
              <th> Desayuno</th>
              <th> Almuerzo</th>
              <th> Cena</th>
            </tr>
          </thead>
          <tbody>

           @foreach ($resumenVentas as $escuela)
            
            
              
                <tr>
                <td>{{$escuela->nombre}}</td>
                 <td><strong>{{$escuela->cupos_desayuno_count}}</strong>/{{total_cupos(1,$escuela->id_escuela,session()->get('id_semestre'))}}</td>
                 <td><strong>{{$escuela->cupos_almuerzo_count}}</strong>/{{total_cupos(2,$escuela->id_escuela,session()->get('id_semestre'))}}</td>
                 <td><strong>{{$escuela->cupos_cena_count}}</strong>/{{total_cupos(3,$escuela->id_escuela,session()->get('id_semestre'))}}</td>
               </tr>
              
             
           @endforeach
          </tbody>
        </table>
        </div>
      </div>