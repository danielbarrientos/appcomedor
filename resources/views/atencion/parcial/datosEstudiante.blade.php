<div class="row ">
    <div class="col-md-4">
     <img src="{{asset('archivos/fotos/'.$estudiante->nombre_foto)}}"  class="img-thumbnail">
   </div> 
   <div class="col-md-8">
       <table class=" table-sm table">
        <tbody>
          <tr>
            <td width="100px;"><strong>Dni:</strong></td>
            <td>{{ @$estudiante->dni_estudiante}}</td>
          </tr>
           <tr>
            <td><strong>Código:</strong> </td>
            <td>{{ @$estudiante->codigo_universitario}}</td>
          </tr>
           <tr>
            <td><strong>Apellidos y Nombres:</strong></td>
            <td>{{ @$estudiante->apellidos}} {{@$estudiante->nombres}}</td>
          </tr>
          
           <tr>
            <td><strong>EAP:</strong></td>
            <td>{{@$estudiante->escuela->nombre}}</td>
          </tr>
        </tbody>
        
      </table>
      <div class="row">
        <div class="col-md-12">
          <form action="{{url('atencion/'.$ruta)}} " method="GET" id="formDatos">
            <input type="hidden" name="confirmado" value="1">
             <input type="hidden" name="verificar" value="1">
            <input type="hidden" name="parametro" value="{{$estudiante->dni_estudiante}}">

            <button type="submit" class="btn btn-block btn-primary btn-square">Confirmar Indentidad</button>
             <a href="{{url('registrarAtencionRegular')}}" class="btn btn-block btn-square btn-secondary ">Cancelar</a>
          </form>
        </div>
      </div>
   </div>
   
  </div>