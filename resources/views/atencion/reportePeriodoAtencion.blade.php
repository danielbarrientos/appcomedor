@extends('layouts.coreui')
	@section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Reporte de asistencia</li>
  	@endsection
 @section('content')
 <div class="row">
	<div class="col-lg-12 ">
		<div class="card">
			<div class=" card-header"><i class="fa fa-list"></i> Reporte de asistencia</div>			
            	<div class="card-body">
            		<form method="GET" action="{{url('atencion/rptAtencionPdf')}}"  id="formulario">
            			
	            		
	        			<div class="row">
	        				<div class="col-md-3">
	        					<div class="form-group">
			 						<label>Escuela</label>
			 						
			 						<select class="form-control form-control-sm " name="escuela" id="escuela" required="required">
			 							<option value="">Seleccione</option>	 
			 							@foreach ($escuelas as $escuela)
			 								<option value="{{$escuela->id_escuela}}" {{ (old('escuela',@$id_escuela) == $escuela->id_escuela) ? 'selected':'' }}>{{$escuela->nombre}}</option>	
			 							@endforeach
			 						</select>
			 						<em class="text-danger">{{ @$errors->first('escuela') }}</em>
			 					</div>	
	        				</div>	
	        				
			 				<div class="col-md-4">
			 					<div class="form-group">
			 						<label>Periodo</label>
			 						<select class="form-control  form-control-sm" name="periodo" id="periodo" >
			 							<option value="0">Todos los periodos</option>	 
			 							@foreach ($periodos as $periodo)

			 								<option value="{{$periodo->id_periodoatencion}}" {{ (old('periodo',@$id_periodo) == $periodo->id_periodoatencion) ? 'selected':'' }}>{{$periodo->fecha_inicio->formatLocalized('%A %d %B %Y')}} al {{$periodo->fecha_fin->formatLocalized('%A %d %B %Y')}}</option>	
			 							@endforeach
			 						</select>
			 						<em class="text-danger">{{ @$errors->first('periodo') }}</em>
			 					</div>
			 				</div>	

			            	<div class="col-md-3">
			            		<div class="form-group">
			             			<label>Servicio</label>
				 					<select class="form-control  form-control-sm" name="servicio" id="servicio" required="required">
				 						<option value="">Seleccione</option>
				 						<option value="1" {{ (old('servicio',@$id_servicio) == '1') ? 'selected':'' }}>Desayuno</option>
				 						<option value="2" {{ (old('servicio',@$id_servicio) == '2') ? 'selected':'' }}>Almuerzo</option>
				 						<option value="3" {{ (old('servicio',@$id_servicio) == '3') ? 'selected':'' }}>Cena</option>
				 					</select>
				 					<em class="text-danger">{{ @$errors->first('servicio') }}</em>	
			             		</div>
			            	</div>
			            	

			              </div>	
			 					<!--button class="btn btn-primary btn-sm  "> Ver Reporte </button-->
			 					<button  class="btn btn-light btn-sm  "><i class="fa fa-file-pdf-o fa-lg text-danger"></i>  Exportar PDF</button>

			 					<button  class="btn btn-light btn-sm " target="_blank" formaction="{{url('atencion/rptAtencionExcel')}}"><i class="fa fa-file-excel-o fa-lg text-success "></i>  Exportar Excel</button>			 		
	
        			</form>
        			<br>
        			@if (@$comensales)
        				        			
        			<div class="row">
        				<div class="col-sm-12">
        					<div class="table-responsive">
	        					<table class="table-sm table-hover table-bordered">
	        						<thead >
	        							<tr>
	        								<th>#</th> 
	        								<th>DNI</th> 
	        								<th>APELLIDOS Y NOMBRES</th>
	        								<th>VENTA</th>
	        								@foreach ($fechas as $fecha)
	        									<th width="5" class="m-0 p-0" ><p  class="texto-vertical-1 m-0 p-1 ">{{date("d/m", strtotime($fecha->dia))}}</p></th> 
	        								@endforeach
	        							</tr>
	        						</thead>
	        						<tbody>
	        								@php
	        									$i=1 ;
	        								@endphp
	        								@foreach ($comensales as $comensal)

	        								<tr>
	        									<td>{{$i++}}</td>
	        									<td >{{$comensal['dni_estudiante']}}</td>
	        									<td>{{$comensal['apellidos']}} {{$comensal['nombres']}}</td>
	        									<td> {{($comensal['tipo_venta']==1)? 'Regular':'Libre' }} </td>
	        									@foreach ($fechas as $fecha)
	        											
	        										<td >{{$comensal[$fecha->dia]}}</td> 
	        									@endforeach
	        								</tr>	
	        								@endforeach
	        								
	        							
	        						</tbody>
	        					</table>
        					</div>
        				</div>
        			</div>
        			
        			@endif
            	</div>    
		</div>
	</div>			
							
</div>


@endsection

@section('scripts')
	
@parent

@endsection 