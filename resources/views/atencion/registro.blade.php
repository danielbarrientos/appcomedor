@extends('layouts.simpleBack')
@section('breadcrumb')

<li class="breadcrumb-item">
  <a href="{{url('panel')}}">Inicio</a>
</li>

<li class="breadcrumb-item active">Atención</li>
@endsection
@section('content')

<div class="row mt-2 ">
  <div class="col-sm-12  col-lg-8 offset-lg-2 ">
    <div class="card">

      <div class=" card-header">
        <i class="fa fa-list"></i> Atención {{@$servicio->descripcion}} ({{ Session::get('nombre_semestre') }})
      </div>
      <br>
      <div class="">
        @if (@$periodo->estado==3 && !is_null($servicio))
          <form method="GET" action="{{url('/atencion/'.$ruta)}}">
            <div class="row m-2">

              <div class="col-sm-6">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" name="verificar" value="1" @if(old('verificar',@$verificar)=="1" )  checked
                  @endif class="custom-control-input" id="verificar">
                  <label class="custom-control-label" for="verificar">Verificar identidad</label>
                </div>
              </div>
              <div class="col-sm-6 ">
                <div class="input-group float-right ">
                  <input type="search" name="parametro" class="form-control form-control-sm " value="" placeholder="Dni, Cód. universitario o Cód. rfid  " maxlength="11" minlength="6" required="required" autofocus="autofocus">
                  <div class="input-group-append">
                    <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button>
                  </div>
                </div>
              </div>
            </div>
            <!--row-->
          </form>
          <div class="row m-2">
            <div class="col-sm-12">
              <ul class="nav nav-pills nav-justified">
                <li class="nav-item ">
                  <a class="nav-link  @if(Route::currentRouteName()=='atencionRegular') active @endif  " class="bg-" href="{{route('atencionRegular')}}">Atención Regular</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link @if(Route::currentRouteName()=='atencionLibre') active @endif" href="{{route('atencionLibre')}}">Atención Libre</a>
                </li>
              </ul>
              @if (@$estudiante)
              <div class="card ">

                <div class="card-body">
                  @include('atencion.parcial.datosEstudiante')
                </div>
              </div>

              @endif
              <div class="mt-1">
                @if (Session::has('msj-warning'))

                <div class="alert alert-warning" role="alert">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <h4 class="alert-heading"> ESTUDIANTE NO ENCONTRADO</h4>
                  <p> {!!Session::get('msj-warning')!!}</p>
                </div>
                @endif
                @if (Session::has('msj-info'))
                <div class="alert alert-info" role="alert">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <h4 class="alert-heading"> Mensaje</h4>
                  <p> {!!Session::get('msj-info')!!}</p>
                </div>
                @endif
                @if (Session::has('msj-success'))
                <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <h4 class="alert-heading text-center"> REGISTRO CORRECTO</h4>
                  <hr>
                  <p> {!!Session::get('msj-success')!!}</p>
                </div>
                @endif
                @if (Session::has('msj-error'))
                <div class="alert alert-danger " role="alert">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <h5 class="alert-heading text-center">REGISTRO DUPLICADO</h5>
                  <hr>
                  <p> {!!Session::get('msj-error')!!}</p>
                </div>
                @endif
              </div>
            </div>
          </div>
      </div>
      <!--card-body-->
      @else
      <div class="card-body">
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> No hay periodos en proceso de atención o la hora actual no pertenece a ningún horario de atención.
          <ul>
            <li><strong>Desayuno</strong> : 07:00 am. – 09:00 am.</li>
            <li><strong>Almuerzo </strong> : 12:00 pm. – 02:00 pm.</li>
            <li><strong>Cena </strong> : 05:30 pm. – 07:30 pm.</li>
          </ul>
        </div>
      </div>
      @endif
    </div>
    <!--car box-->
  </div>
  <!--col-->

</div>

@endsection
@php

@endphp
@section('scripts')

<script type="text/javascript">
  function recargarPagina() {
    window.location.href = '{{ url()->full() }}';

  }

  window.onload = function() {
    document.onkeydown = teclas;
  }

  function teclas(event) 
  {
    var codigo = event.keyCode;
    if (codigo == 32)     
      $('#formDatos').submit();
    
  }
</script>

@parent

@endsection
