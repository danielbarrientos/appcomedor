<script type="text/javascript">
	@if (Session::has('msg-info'))
 	toastr.info("{!! Session::get('msg-info') !!}","",{
 		"timeOut":10000,
 		"progressBar": true,
 		"showMethod": "fadeIn",
			"hideMethod": "fadeOut"

 	})
 	@endif	
 	@if (Session::has('msg-success'))
 	toastr.success("{!! Session::get('msg-success') !!}","",{
 		"timeOut":10000,
 		"progressBar": true,
 		"showMethod": "fadeIn",
			"hideMethod": "fadeOut"

 	})
 	@endif	
 	@if (Session::has('msg-warning'))
 	toastr.warning("{!! Session::get('msg-warning') !!}","",{
 		"timeOut":10000,
 		"progressBar": true,
 		"showMethod": "fadeIn",
			"hideMethod": "fadeOut"

 	})
 	@endif	
 	@if (Session::has('msg-error'))
 	toastr.error("{!! Session::get('msg-error') !!}","",{
 		"timeOut":10000,
 		"progressBar": true,
 		"showMethod": "fadeIn",
			"hideMethod": "fadeOut"

 	})
 	@endif	
</script>
