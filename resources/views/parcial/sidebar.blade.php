<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{url('panel')}}">
          <i class="nav-icon icon-speedometer"></i> Inicio
          <span class="badge badge-primary">NEW</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('ficha')}}">
          <i class="fa fa-file-text  nav-icon"></i> Ficha socioeconomica
        </a>
      </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('estudiante')}}">
              <i class="icon-people icons  nav-icon"></i> Estudiantes
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('beneficiario')}}">
                <i class="icon-people icons  nav-icon"></i> Beneficiarios
            </a>
        </li>
  
          @if (Session::get('id_rol') != 3)
              
          
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon fa fa-institution fa-lg "></i> Administración C.U</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="{{url('semestre')}}">
               <i class="fa fa-circle-o nav-icon"></i> Semestre </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{url('cupoprogramado')}}">
               <i class="fa fa-circle-o nav-icon"></i> Cupos por EAP</a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href="{{url('periodoAtencion')}}">
               <i class="fa fa-circle-o nav-icon"></i> Peridos de atención</a>
          </li>

        </ul>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon  fa fa-user-circle-o fa-lg"></i> Estudiantes</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="{{url('estudiante')}}">
               <i class="fa fa-circle-o nav-icon"></i> Registro individual </a>
          </li>
         <li class="nav-item">
            <a class="nav-link" href="{{url('estudiante/registroMasivo')}}">
               <i class="fa fa-circle-o nav-icon"></i> Registro masivo </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('beneficiario')}}">
               <i class="fa fa-circle-o nav-icon"></i> Beneficiarios</a>
          </li>

        </ul>
      </li>
        <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon fa fa-money fa-lg"></i> Venta </a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="{{url('ventaCupo/registrarVentaRegular')}}" target="_blank">
               <i class="fa fa-circle-o nav-icon"></i> Registrar venta</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{url('ventaCupo/reporteVenta')}}">
               <i class="fa fa-circle-o nav-icon"></i> Reporte de ventas</a>
          </li>

        </ul>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon fa fa-cutlery"></i> Atención </a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a href='javascript:void(0);' onclick="window.open('{{url('atencion/registrarAtencionRegular')}}', '_blank', 'width=800,height=600,scrollbars=yes,status=yes,resizable=yes,screenx=300,screeny=0');"  class="nav-link @if(Route::currentRouteName()=='atencionLibre') active @endif "> <i class="fa fa-circle-o nav-icon  "></i>Registrar atención</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{url('atencion/reporteAtencion')}}">
               <i class="fa fa-circle-o nav-icon"></i> Reporte de atencion</a>
          </li>

        </ul>
      </li>

      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon fa fa-user"></i> Usuarios</a>
        <ul class="nav-dropdown-items">
          @if (permiso('usuario.index'))
            <li class="nav-item">
            <a class="nav-link" href="{{url('usuario')}}">
               <i class="fa fa-circle-o nav-icon"></i> Registro de usuarios
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" href="{{url('rol')}}">
               <i class="fa fa-circle-o nav-icon"></i> Roles de usuario
            </a>
          </li>
        </ul>
      </li>
      @endif


      <li class="divider"></li>
      <li class="nav-title">Extras</li>

    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>