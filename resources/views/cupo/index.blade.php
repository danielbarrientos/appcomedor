@extends('layouts.coreui')
  @section('breadcrumb')

    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Cupos Programados</li>
  @endsection
 @section('content')
 
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
         <i class="fa fa-align-justify"></i>  Cupos por Escuela Académico Profesional
        </div>
        <div class="card-body">
            @if (@$escuelas)
               <div class="table-responsive">
            <table class=" table table-sm table-striped table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Escuela Académico Profesional</th>
                  <th>Desayuno</th>
                  <th>Almuerzo</th>
                  <th>Cena</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($escuelas as $escuela)
                  <tr>
                    <td>{{$escuela->id_escuela}}</td>
                    <td>{{$escuela->nombre}}</td>
                    <td>
                      @if ($escuela->cupo_desayuno_count==1)
                        @php
                          $cupo=find_cupo(1,$escuela->id_escuela);
                        @endphp
                         <button type="button"  OnClick="editarCupo('{{ $escuela->nombre}}','Desayuno','{{  $cupo->id_cupoprogramado }}','{{ $cupo->cantidad }}');" data-toggle='modal' data-target='#modalCrear'  data-toggle="tooltip" data-placement="right" title=" Editar"  class="btn btn-link badge badge-success "><i class="fa fa-pencil "></i> {{$cupo->cantidad}}</a>
                      @elseif($escuela->cupo_desayuno_count==0)
                          <a  href="#" data-toggle='modal' data-target='#modalCrear' class=" badge " OnClick="crearCupo('{{$escuela->id_escuela}}','{{ $escuela->nombre}}','1','Desayuno');" ><i class="fa fa-plus"></i> Agregar</a>
                      @endif
                    </td>
                    <td>
                       @if ($escuela->cupo_almuerzo_count==1)
                         @php
                          $cupo=find_cupo(2,$escuela->id_escuela);
                        @endphp
                          <button type="button"  OnClick="editarCupo('{{ $escuela->nombre}}','Almuerzo','{{  $cupo->id_cupoprogramado }}','{{ $cupo->cantidad }}');" data-toggle='modal' data-target='#modalCrear'  data-toggle="tooltip" data-placement="right" title=" Editar"  class=" badge badge-primary btn btn-link"><i class="fa fa-pencil"></i> {{$cupo->cantidad}}</button>
                      @elseif($escuela->cupo_almuerzo_count==0)
                       <a  href="#" data-toggle='modal' data-target='#modalCrear' class=" badge " OnClick="crearCupo('{{$escuela->id_escuela}}','{{ $escuela->nombre}}','2','Almuerzo');" ><i class="fa fa-plus"></i> Agregar</a>
                      @endif
                    </td>
                    <td>
                       @if ($escuela->cupo_cena_count==1)
                         @php
                          $cupo=find_cupo(3,$escuela->id_escuela);
                        @endphp
                          <button type="button"  OnClick="editarCupo('{{ $escuela->nombre}}','Cena','{{  $cupo->id_cupoprogramado }}','{{ $cupo->cantidad }}');" data-toggle='modal' data-target='#modalCrear'  data-toggle="tooltip" data-placement="right" title=" Editar"  class="badge badge-dark btn btn-link"><i class="fa fa-pencil"></i> {{$cupo->cantidad}}</button>
                      @elseif($escuela->cupo_cena_count==0)
                       <a  href="#" data-toggle='modal' data-target='#modalCrear' class=" badge " OnClick="crearCupo('{{$escuela->id_escuela}}','{{ $escuela->nombre}}','3','Cena');" ><i class="fa fa-plus"></i> Agregar</a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
            @else
               <div class="alert alert-info">
                  <strong>No se ha seleccionado ningún semestre</strong> No se ha encontrado ningún semestre, debe crear un nuevo semestre
              </div>
            @endif

        </div>
      </div>
    </div>
  </div>




       <div class="modal fade formulario" id="modalCrear" data-controls-modal="modalCrear" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
      <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title"> <i class="fa fa-institution  "></i> Cupos por Escuela</h5>
                <button type="button" class="close" data-dismiss="modal"  aria-label="close"><span aria-hidden="true">x</span></button>
            </div>
               @if ($semestre->estado=='1')
                <form  class="form-horizontal formulario" method="POST" action="{{ url('') }}" id="formulario">
                  <div class="modal-body">
                    <p>Ingrese la cantidad de cupos que desea asignar a la EAP</p>

                        <input type="hidden" name="id_escuela" id="id_escuela">
                        <input type="hidden" name="id_servicio" id="id_servicio">
                        <input type="hidden" name="id_cupoprogramado" id="id_cupoprogramado">
                        <input type="hidden" name="ruta" id="ruta">

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Escuela</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control input-sm" value="" disabled="disabled" name="nombre_escuela" id="nombre_escuela">
                                </div>
                            </div>
                           <div class="form-group row">
                                <label class="col-md-2 form-control-label">Servicio</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control input-sm" value="" disabled="disabled" name="nombre_servicio" id="nombre_servicio">
                                </div>
                            </div>


                        <div class="form-group row">
                                <label class="col-md-2 form-control-label">Cantidad</label>
                                <div class="col-md-5">
                                    <input type="number" min="1" class="form-control input-sm" name="cantidad"  placeholder="Ingrese  cantidad " id="cantidad " autofocus required="required">
                                </div>
                            </div>


                   </div>
                  <div class="modal-footer">
                       <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                          <button  class="btn btn-primary btn-sm" id="btnGuardar">Guardar</button>

                  </div>
                </form>
                 @else
                  <div class="alert alert-info">
                    <strong>
                      <i class="fa fa-info-circle fa-lg"></i>
                       El semestre no se encuentra Activo
                    </strong>
                    <p>No se puede realizar ningún cambio</p>
                  </div>
                @endif
                <div id="mensajes" class="m-t-5"></div>
            </div>
        </div>
    </div><!-- /.modal -->



@endsection

@section('scripts')
	<script type="text/javascript">
    function recargarPagina(){
      window.location.href='{{ url()->full() }}';

      }
      function crearCupo(id_escuela,nombre_escuela,id_servicio,nombre_servicio){

        $("#id_escuela").val(id_escuela);
        $("#id_servicio").val(id_servicio);
        $("#nombre_escuela").val(nombre_escuela);
        $("#nombre_servicio").val(nombre_servicio);
        $("[type=number]").val('');
         $('#ruta').val("{{ url('cupoprogramado/insertar') }}");
      }
 function editarCupo(nombre_escuela,nombre_servicio,id_cupoprogramado,cantidad,){
    $("#mensajes").html('');
    $("#nombre_escuela").val(nombre_escuela);
    $("#nombre_servicio").val(nombre_servicio);
    $("[type=number]").val(cantidad);
    $("#id_cupoprogramado").val(id_cupoprogramado);
    $('#ruta').val('{{ url('')}}/cupoprogramado/actualizar');

 }

 function pulsar(e) { //funcion para evitar tecla enter
  tecla = (document.all) ? e.keyCode :e.which;
  return (tecla!=13);
}



  $(document).on("submit","#formulario", function(e){
    $('#btnGuardar').attr("disabled", false);//desabilitamos el boton
    e.preventDefault();

     var datos= new FormData($("#formulario")[0]);//esto es para capturar todos los input del formulario

      var route=$("#ruta").val();
    $.ajax({
      url:route,
      headers:{'X-CSRF-TOKEN': "{{csrf_token()}}" },// envio del token
      type:'POST',
      datatype: 'json',
      data: datos,//nombrq q va a recibirl el controllador : nombre de la variable
      //cache: false,//ESTABLECER  FALSE PARA ENVIAR ARCHIVOS VIA AJAX
      contentType: false,//ESTABLECER EN FALSE PARA ENVIARDATOS FORMDATA
      processData: false,
      success: function(respuesta){
        if (respuesta.parametro=='edicion') {
          //  $('#modalCrear').modal('toggle');

        }

    //recargarPagina();
        //$("#modalCrearAlternativa").modal('toggle');
        msjSuccess(respuesta.mensaje);
        recargarPagina();
        //window.setInterval("recargarPagina()",1500);
        $('#btnGuardar').attr("disabled", true);//desabilitamos el boton

      },
      error: function(xhr, textStatus, thrownError){
            var listaErrores='';
                        $.each($.parseJSON(xhr.responseText), function (ind, elem) {
                   listaErrores +="<li>"+ elem+"</li>";
       });
          msjError(listaErrores);
      }

    });
  });

  function msjSuccess(mensaje){
    toastr.success(mensaje,"",{
    "timeOut":10000,
    "progressBar": true,
    "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

  });

}
 function msjError(lista){
  $("#mensajes").html("<div class=' alert alert-danger alert-dismissible ' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='close'><span aria-hidden='true'>&times;</span></button><strong>Mesajes de validacion !!</strong>"+lista+"</div>");
}


</script>

@parent

@endsection
