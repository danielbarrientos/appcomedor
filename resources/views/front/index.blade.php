@extends('layouts.front')

@section('content')

<div class="row ">
	<div @if (Session::has('dni_estudiante')) class="col-md-12"
	@else class="col-md-9"
	@endif >
	<div class="card-box">

		<h2 class="text text-center">COMEDOR UNIVERSITARIO -UNAMBA </h2>
		<br>
		<div class="row">
			<div class="col-md-6 text-center">

				<img src="{{asset('/img/front/platos.png')}}">
				<h3>Servicio</h3>
				<p>Tiene como finalidad brindar el servicio del comedor universitario con una alimentación sana balanceada de calidad óptima y con bajo costo a los estudiantes de escasos recursos económicos y aceptable rendimiento académico.</p>
			</div>
			<div class="col-md-6 text-center">

				<img src="{{asset('/img/front/reloj.png')}}">
				<h3>Horarios </h3>
				<p>Los horarios de atención del comedor son los siguientes:</p>
				<p>Desayuno : 07:00 am. – 09:00 am. <br>
					Almuerzo : 12:00 pm. – 02:00 pm. <br>
				</p>

			</div>

		</div>

	</div>
	<!--car box-->
</div>
<!--col-->
@if (!Session::has('dni_estudiante'))
<div class="col-md-3">
	<div class="card-box">

		<h4 class="m-t-0 header-title text-center">Inicio de sesión</h4>

		<p class="text-muted font-13 m-b-10">
			IdentifÍquese como estudiante de la UNAMBA
		</p>
		<form action="{{url('/iniciarSession')}}" method="POST">
			{{csrf_field()}}
			<div class="form-group">
				<label>Código de estudiante:</label>
				<input type="text" name="codigo" class="form-control input-sm " placeholder="Ingrese su código de estudiante" maxlength="6" required="required" value="{{old('codigo')}}">
				<em class="text-danger"> {{ $errors->first('codigo')}}</em>
			</div>
			<div class="form-group">
				<label>Documento de identidad.</label>
				<input type="text" name="dni" class="form-control input-sm" placeholder="Ingrese su Nº de DNI" maxlength="8" required="required" value="{{old('dni')}}">
				<em class="text-danger">{{ $errors->first('dni')}}</em>
			</div>
			<button type="submit" class="btn btn-sm btn-primary btn-block"><i class="icon-login fa-lg"></i> Ingresar</button>
		</form>
		<hr>
		<h4>Inicia sessión para:</h4>
		<ul>
			<li>Registrar o actualizar su ficha Socioeconómica.</li>
			<li>Consultar su historial de compras y atención.</li>
		</ul>
	</div>

</div>

@endif

</div>


@endsection

@section('scripts')

@parent

@endsection
