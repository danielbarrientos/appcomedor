@section('scripts')
    <link href="{{asset('css/menu-step.css')}}" rel='stylesheet' type='text/css' >
@parent
@endsection
@extends('layouts.front')
	
 @section('content')
    <div class="row">
        <div class="col-md-push-1 col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <h4>FICHA SOCIOECONOMICA</h4>
                </div>
                <div class="panel-body ">
                
                    <div id="menuBackground">
                        <div id="menuContainer">
                            <ul id="menu">
                                <li class="active" id="step1"><div class="numberStep">1</div><div class="texto_step">Identificación del estudiante</div></li>
                                <li id="step2"><div class="numberStep">2</div><div class="texto_step">Características generales</div></li>
                                <li id="step3"><div class="numberStep">3</div><div class="texto_step">Características de vivienda</div></li>
                                <li id="step4"><div class="numberStep">4</div><div class="texto_step">Datos deL hogar</div></li>
                                <li id="step5"><div class="numberStep">5</div><div class="texto_step">Características académicas</div></li>
                            </ul>     
                        </div>
                    </div>
                    <div class="row panel-body">
                        <div class="col-m-12">
                            <!--ul class="nav nav-tabs" role="tablist" >
                                <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">TAB1</a></li>
                                <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">TAB2</a></li>
                                <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">TAB3</a></li>
                                <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">TAB4</a></li>
                                <li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">TAB5</a></li>
                            </ul-->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab1">
                                    @include('front.parcial.ficha_tab1')    
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab2">
                                    @include('front.parcial.ficha_tab2')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab3">
                                    @include('front.parcial.ficha_tab3')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab4">
                                    @include('front.parcial.ficha_tab4')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab5">
                                    @include('front.parcial.ficha_tab5')
                                   
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
	

@endsection

@section('scripts')
    <script src="{{asset('plugins/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('js/front/index_jscript.js?111')}}"></script>
    <script src="{{asset('js/front/index_jquery.js?111')}}"></script>
@parent

@endsection
