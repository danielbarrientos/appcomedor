<h4 class="text-center">DATOS DEL HOGAR</h4><hr>
    <form action="{{url('fichaEstudiante/tab4')}}" method="POST" id="form_tab4" class="especial" >
    <fieldset>
        <legend>COMPOSICION FAMILIAR</legend>
        <small>Hermanos que actualmente dependen de los padres de usted .Incluyase si es el caso</small>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <button class="btn btn-xs btn-primary" type="button" id="btn_fila_composicion"> <i class="fa fa-plus"></i> Agregar familiar </button>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>Parentesco</th>
                        <th>Nombres y Apellidos</th>
                        <th>Edad</th>
                        <th>Dni <small>(opcional)</small></th>
                        <th>Estado civ.</th>
                        <th>Centro trabajo/Estudio <small>(opcionall)</small></th>
                        <th>Grado Intrucción</th>
                        <th>Eliminar</th>        
                    </tr>
                </thead>
                <tbody id="list_composicion">
                    @if (! empty($composicion_familiar))
                    @foreach ($composicion_familiar as $item)
                    <tr>
                        <td>
                            <select class="from-control input-sm parentesco">
                                <option  value="Padre" {{(@$item->parentesco=='Padre')?'selected':''}}>Padre</option>
                                <option  value="Madre" {{(@$item->parentesco=='Madre')?'selected':''}}>Madre</option>
                                <option  value="Hermano" {{(@$item->parentesco=='Hermano')?'selected':''}}>Hermano</option>
                                <option  value="Yo" {{(@$item->parentesco=='Yo')?'selected':''}}>Yo</option>
                                <option  value="Otros" {{(@$item->parentesco=='Otros')?'selected':''}}>Otros</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control input-sm nombre"  placeholder="Ingrese nombre completo" value="{{old('algo',@$item->nombre)}}" required>
                        </td>
                        <td width="80px;">
                            <input type="number" min="0" class="form-control input-sm edad"  placeholder="Ingrese edad" value="{{old('algo',@$item->edad)}}" required>
                        </td>
                        <td width="120px;">
                            <input type="number" min="0" class="form-control input-sm dni" placeholder="Ingrese dni "  value="{{old('algo',@$item->dni)}}" >
                        </td>
                        <td>
                            <select name="" id="" class="form-control input-sm estado_civil">
                                <option value=""></option>
                                <option value="Soltero" {{(@$item->estado_civil=='Soltero')?'selected':''}}>Soltero</option>
                                <option value="Casado" {{(@$item->estado_civil=='Casado')?'selected':''}}>Casado</option>
                                <option value="Conviviente" {{(@$item->estado_civil=='Conviviente')?'selected':''}}>Coniviente</option>
                                <option value="Divorciado" {{(@$item->estado_civil=='Divorciado')?'selected':''}}>Divorciado</option>
                                <option value="Viudo" {{(@$item->estado_civil=='Viudo')?'selected':''}}>Viudo</option>
                                <option value="Fallecido" {{(@$item->estado_civil=='Fallecido')?'selected':''}}>Fallecido</option>
                            </select>
                        </td>
                        <td>
                            <input type="text"  class="form-control input-sm centro_trabajo" placeholder="Ingrese centro de Trabajo/Estudio" value="{{old('algo',@$item->centro_trabajo)}}" >
                        </td>
                        <td>
                            <select class="form-control input-sm grado_instruccion">
                                <option value="Ninguno"  {{(@$item->grado_instruccion=='Ninguno')?'selected':''}}>Ninguno</option>
                                <option value="Superior universitario" {{(@$item->grado_instruccion=='Superior universitario')?'selected':''}}>Superior universitario</option>
                                <option value="Superior técnico" {{(@$item->grado_instruccion=='Superior técnico')?'selected':''}}>Superior técnico</option>
                                <option value="Secundaria" {{(@$item->grado_instruccion=='Secundaria')?'selected':''}}>Secundaria</option>
                                <option value="Primaria" {{(@$item->grado_instruccion=='Primaria')?'selected':''}}>Primaria</option>
                            </select>
                        </td>
                        <td>
                                <button class="btn btn-xs btn-danger" type="button"><i class="fa fa-trash fa-lg" title="Eliminar"></i></button>
                        </td>
                    </tr>
                @endforeach      
                @endif
                        
                </tbody>
            </table>  
        </div>
        <div class="alert alert-danger alert-dismissible" id="alert_composicion" >
            
            <p id="msg_composicion"></p>
        </div>
    </fieldset>
    <fieldset>
        <legend>NUCLEO FAMILIAR DEL ESTUDIANTE</legend>
        <small>LLenar si es casado,Conviviente,con hijos u otros familiares que dependen de usted.</small>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <button class="btn btn-xs btn-primary" type="button" id="btn_fila_nucleo"> <i class="fa fa-plus"></i> Agregar familiar </button>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>Nombres y Apellidos</th>
                        <th>Edad</th>
                        <th>Dni <small>(opcional)</small></th>
                        <th>Estado civ.</th>
                        <th>Centro trabajo/Estudio <small>(opcional)</small></th>
                        <th>Grado Intrucción</th>
                        <th>Eliminar</th>        
                    </tr>
                </thead>
                <tbody id="list_nucleo">
                    @if (! empty($nucleo_familiar))
                    @foreach ($nucleo_familiar as $item)
                    <tr>
                        <td>
                            <input type="text" class="form-control input-sm nombre" value="{{old('algo',@$item->nombre)}}" required>
                        </td>
                        <td width="80px;">
                            <input type="number" min="0" class="form-control input-sm edad" value="{{old('algo',@$item->edad)}}" required>
                        </td>
                        <td width="120px;">
                            <input type="number" min="0" class="form-control input-sm dni" value="{{old('algo',@$item->dni)}}" >
                        </td>
                        <td>
                            <select name="" id="" class="form-control input-sm estado_civil">
                                <option value="Soltero" {{(@$item->estado_civil=='Soltero')?'selected':''}}>Soltero</option>
                                <option value="Casado" {{(@$item->estado_civil=='Casado')?'selected':''}}>Casado</option>
                                <option value="Conviviente" {{(@$item->estado_civil=='Conviviente')?'selected':''}}>Coniviente</option>
                                <option value="Divorciado" {{(@$item->estado_civil=='Divorciado')?'selected':''}}>Divorciado</option>
                                <option value="Viudo" {{(@$item->estado_civil=='Viudo')?'selected':''}}>Viudo</option>
                                <option value="Fallecido" {{(@$item->estado_civil=='Fallecido')?'selected':''}}>Fallecido</option>
                            </select>
                            
                        </td>
                        <td>
                            <input type="text"  class="form-control input-sm centro_trabajo" value="{{old('algo',@$item->centro_trabajo)}}" >
                        </td>
                        <td>
                            <select class="form-control input-sm grado_instruccion">
                                <option value="Ninguno"  {{(@$item->grado_instruccion=='Ninguno')?'selected':''}}>Ninguno</option>
                                <option value="Superior universitario" {{(@$item->grado_instruccion=='Superior universitario')?'selected':''}}>Superior universitario</option>
                                <option value="Superior técnico" {{(@$item->grado_instruccion=='Superior técnico')?'selected':''}}>Superior técnico</option>
                                <option value="Secundaria" {{(@$item->grado_instruccion=='Secundaria')?'selected':''}}>Secundaria</option>
                                <option value="Primaria" {{(@$item->grado_instruccion=='Primaria')?'selected':''}}>Primaria</option>
                            </select>
                        </td>
                        <td>
                            <button class="btn btn-xs btn-danger" type="button"><i class="fa fa-trash fa-lg" title="Eliminar"></i></button>
                        </td>
                    </tr>
                @endforeach      
                @endif
                                             
                </tbody>
            </table>  
        </div>
        <div class="alert alert-danger alert-dismissible" id="alert_nucleo" >
           
            <p id="msg_nucleo"></p>
        </div>
    </fieldset>
    <fieldset>
        <legend>SITUACION FAMILIAR</legend>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="email">Situacion de padres:</label>
                    <select class="form-control input-sm" name="situacion_padres" id="situacion_padres" >
                        <option value="Ninguna" selected >Ninguna</option>
                        <option value="Padres viven juntos">Padres viven juntos</option>
                        <option value="Padres separados/divorciados">Padres separados/divorciados</option>
                        <option value="Padre/Madre privados de su libertad">Padre/Madre privados de su libertad</option>
                        <option value="Padre y Madre solteros">Padre y Madre solteros</option>
                        <option value="Padres ancianos">Padres ancianos</option>
                    </select>
                    <em id="situacion_padres-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Orfandad:</label>
                    <select class="form-control input-sm" name="situacion_orfandad" id="situacion_orfandad" >
                        <option value="Ninguna" selected >Ninguna</option>
                        <option value="Huerfano de padre">Huerfano de padre</option>
                        <option value="Huerfano de madre">Huerfano de madre</option>
                        <option value="Huerfano de ambos padres">Huerfano de ambos padres</option>

                    </select>
                    <em id="situacion_orfandad-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd"><small>Victimas de violencia política (terrorismo)</small></label>
                    <select class="form-control input-sm" name="victima_terrorismo" id="victima_terrorismo" >
                        <option value="No">No</option>
                        <option value="Si">Si</option>
                    </select>
                    <em id="victima_terrorismo-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Resolución Nº</label>
                    <input type="text" class="form-control input-sm" placeholder="ingrese su nro. de resolucion" id="resolucion_terrorismo">
                </div>
            </div>
        </div>        
    </fieldset>
    <fieldset>
        <legend>ALIMENTACION DEL ESTUDIANTE <small>(Lugar donde toma sus alimentos)</small></legend>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Desayuno:</label>
                    <select class="form-control input-sm" name="desayuno" id="desayuno" required >
                        <option value="" selected disabled><-- Seleccione --></option>
                        <option value="Hogar-Casa">Hogar-Casa</option>
                        <option value="Restaurant">Restaurant</option>
                        <option value="Casa de parientes">Casa de parientes</option>
                        <option value="Comedor universitario">Comedor universitario</option>
                        <option value="Prepara usted">Prepara usted</option>
                        <option value="Comedor popular">Comedor popular</option>
                    </select>
                    <em id="desayuno-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Almuerzo:</label>
                    <select class="form-control input-sm" name="almuerzo" id="almuerzo" required >
                        <option value="" selected disabled><-- Seleccione --></option>
                        <option value="Hogar-Casa">Hogar-Casa</option>
                        <option value="Restaurant">Restaurant</option>
                        <option value="Casa de parientes">Casa de parientes</option>
                        <option value="Comedor universitario">Comedor universitario</option>
                        <option value="Prepara usted">Prepara usted</option>
                        <option value="Comedor popular">Comedor popular</option>
                    </select>
                    <em id="almuerzo-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Cena:</label>
                    <select class="form-control input-sm" name="cena" id="cena" required >
                        <option value="" selected disabled><-- Seleccione --></option>
                        <option value="Hogar-Casa">Hogar-Casa</option>
                        <option value="Restaurant">Restaurant</option>
                        <option value="Casa de parientes">Casa de parientes</option>
                        <option value="Comedor universitario">Comedor universitario</option>
                        <option value="Prepara usted">Prepara usted</option>
                        <option value="Comedor popular">Comedor popular</option>
                    </select>
                    <em id="cena-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div>        
    </fieldset>
    <fieldset>
        <legend>SITUACION ECONÓMICA FAMILIAR</legend>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group" id="grupo_dependencia">
                    <label for="email">Dependencia económica:</label><br>
                        <label for="dependencia1" style="font-weight:400"><input type="radio" name="dependencia_economica" id="dependencia1" value="Solo del padre" required> Solo del padre</label><br>
                        <label for="dependencia2" style="font-weight:400"><input type="radio" name="dependencia_economica" id="dependencia2" value="Solo de la madre"> Solo de la madre</label><br>
                        <label for="dependencia3" style="font-weight:400"><input type="radio" name="dependencia_economica" id="dependencia3" value="De ambos padres"> De ambos padres</label><br>
                        <label for="dependencia4" style="font-weight:400"><input type="radio" name="dependencia_economica" id="dependencia4" value="De usted mismo"> De usted mismo</label><br>
                        <label for="dependencia5" style="font-weight:400"><input type="radio" name="dependencia_economica" id="dependencia5" value="De otro familiar"> De otro familiar</label><br>
                        <em id="dependencia_economica-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group" id="grupo_monto">
                    <label for="email">Monto que persive (Mes S/.):</label><br>
                        
                        <label for="monto1" style="font-weight:400"><input type="radio" name="ingreso_familiar" id="monto1" value="Menos de 500" required> Menos de 500</label><br>
                        <label for="monto1" style="font-weight:400"><input type="radio" name="ingreso_familiar" id="monto1" value="De 500 a 800" required> De 500 a 800</label><br>
                        <label for="monto2" style="font-weight:400"><input type="radio" name="ingreso_familiar" id="monto2" value="De 900 a 1100"> De 900 a 1100</label><br>
                        <label for="monto3" style="font-weight:400"><input type="radio" name="ingreso_familiar" id="monto3" value="De 1200 a 1500"> De 1200 a 1500</label><br>
                        <label for="monto4" style="font-weight:400"><input type="radio" name="ingreso_familiar" id="monto4" value="De 1600 a 2000"> De 1600 a 2000</label><br>
                        <label for="monto5" style="font-weight:400"><input type="radio" name="ingreso_familiar" id="monto5" value="Más de 2000"> Más de 2000</label><br>
                        <em id="ingreso_familiar-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-6">
                <label for="">Situacion económica del estudiante <small>(señale sus gastos mensual)</small></label>
               <table class="">
                   <thead>
                       <tr>
                           <th width="10%;"> Nº</th>
                           <th width="60%;"> Concepto</th>
                           <th>Monto S/.</th>
                       </tr>
                   </thead>
                   <tbody id="list_gastos">
                        <tr>
                            <td class="nro">1</td>
                            <td class="concepto">Alimentación</td>
                            <td><input type="number" step="any" min="0.00" class="form-control input-sm" required></td>
                        </tr>
                       <tr>
                           <td class="nro">2</td>
                           <td class="concepto">Vivienda</td>
                           <td><input type="number" step="any" min="0.00" class="form-control input-sm"></td>
                       </tr>
                       <tr>
                            <td class="nro">3</td>
                            <td class="concepto">Material de estudios</td>
                            <td><input type="number" step="any" min="0.00" class="form-control input-sm" required></td>
                        </tr>
                        <tr>
                            <td class="nro">4</td>
                            <td class="concepto">Material de aseo</td>
                            <td><input type="number" step="any" min="0.00" class="form-control input-sm" required></td>
                        </tr>
                        <tr>
                            <td class="nro">5</td>
                            <td class="concepto">Pasajes</td>
                            <td><input type="number" step="any" min="0.00" class="form-control input-sm" required></td>
                        </tr>
                        <tr>
                            <td class="nro">6</td>
                            <td class="concepto">Otros gastos</td>
                            <td><input type="number" step="any" min="0.00" class="form-control input-sm" required></td>
                        </tr>
                   </tbody>
               </table>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>SITUACION DE SALUD DEL ESTUDIANTE Y DE LA FAMILIA</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email"><small>¿Actualmente se encuentra afiliado a algún tipo de seguro de salud ?</small></label>
                    <select class="form-control input-sm"  id="seguro_salud" required >
                        <option value="" selected disabled><-- Seleccione --></option>
                        <option value="Seguro integral de salud(SIS/AUS)">Seguro integral de salud(SIS/AUS)</option>
                        <option value="Seguro privado de salud">Seguro privado de salud</option>
                        <option value="EsSalud">EsSalud</option>
                        <option value="Ninguno">Ninguno</option>
                        <option value="Otros">Otros</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Especifique:</label>
                    <input type="text" class="form-control input-sm" id="especificacion_salud" placeholder=" en caso de otros (especifique)">
                </div>
            </div>
        </div>
    </fieldset>
    <div class="row">
        <div class="col-md-6">
            <fieldset>
                <legend>¿USTED O ALGÚN MIENBRO DE SU FAMILIA PRESENTA ALGUNA ENFERMEDAD?</legend>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th width="40%;"> Miembro de familia</th>
                            <th> Enfermedad</th>
                        </tr>
                    </thead>
                    <tbody id="list_salud_familiar">
                        <tr>
                            <td class="miembro">Estudiante</td>
                            <td>
                                <select class="form-control input-sm">
                                    <option value="">Ninguna</option>
                                    <option value="E.T.S. (Enfermedades de transmisíon sexual)">E.T.S. (Enfermedades de transmisíon sexual)</option>
                                    <option value="Congénitas (de nacimiento)">Congénitas(de nacimiento)</option>
                                    <option value="Oncológica (cáncer)">Oncológica (cáncer)</option>
                                    <option value="Crónicas (Hipertensión,Diabetes)">Crónicas (Hipertensión,Diabetes)</option>
                                    <option value="Convulsiones (Epilepsia)">Convulsiones (Epilepsia)</option>
                                    <option value="Alergia a alimentos/medicamentos">Alergia a alimentos/medicamentos</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="miembro">Padre</td>
                            <td>
                                <select class="form-control input-sm">
                                    <option value="">Ninguna</option>
                                    <option value="E.T.S. (Enfermedades de transmisíon sexual)">E.T.S. (Enfermedades de transmisíon sexual)</option>
                                    <option value="Congénitas (de nacimiento)">Congénitas(de nacimiento)</option>
                                    <option value="Oncológica (cáncer)">Oncológica (cáncer)</option>
                                    <option value="Crónicas (Hipertensión,Diabetes)">Crónicas (Hipertensión,Diabetes)</option>
                                    <option value="Convulsiones (Epilepsia)">Convulsiones (Epilepsia)</option>
                                    <option value="Alergia a alimentos/medicamentos">Alergia a alimentos/medicamentos</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="miembro">Madre</td>
                            <td>
                                <select class="form-control input-sm">
                                    <option value="">Ninguna</option>
                                    <option value="E.T.S. (Enfermedades de transmisíon sexual)">E.T.S. (Enfermedades de transmisíon sexual)</option>
                                    <option value="Congénitas (de nacimiento)">Congénitas(de nacimiento)</option>
                                    <option value="Oncológica (cáncer)">Oncológica (cáncer)</option>
                                    <option value="Crónicas (Hipertensión,Diabetes)">Crónicas (Hipertensión,Diabetes)</option>
                                    <option value="Convulsiones (Epilepsia)">Convulsiones (Epilepsia)</option>
                                    <option value="Alergia a alimentos/medicamentos">Alergia a alimentos/medicamentos</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="miembro">Hermano(a)</td>
                            <td>
                                <select class="form-control input-sm">
                                    <option value="">Ninguna</option>
                                    <option value="E.T.S. (Enfermedades de transmisíon sexual)">E.T.S. (Enfermedades de transmisíon sexual)</option>
                                    <option value="Congénitas (de nacimiento)">Congénitas(de nacimiento)</option>
                                    <option value="Oncológica (cáncer)">Oncológica (cáncer)</option>
                                    <option value="Crónicas (Hipertensión,Diabetes)">Crónicas (Hipertensión,Diabetes)</option>
                                    <option value="Convulsiones (Epilepsia)">Convulsiones (Epilepsia)</option>
                                    <option value="Alergia a alimentos/medicamentos">Alergia a alimentos/medicamentos</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="miembro">Hijo(a)</td>
                            <td>
                                <select class="form-control input-sm">
                                    <option value="">Ninguna</option>
                                    <option value="E.T.S. (Enfermedades de transmisíon sexual)">E.T.S. (Enfermedades de transmisíon sexual)</option>
                                    <option value="Congénitas (de nacimiento)">Congénitas(de nacimiento)</option>
                                    <option value="Oncológica (cáncer)">Oncológica (cáncer)</option>
                                    <option value="Crónicas (Hipertensión,Diabetes)">Crónicas (Hipertensión,Diabetes)</option>
                                    <option value="Convulsiones (Epilepsia)">Convulsiones (Epilepsia)</option>
                                    <option value="Alergia a alimentos/medicamentos">Alergia a alimentos/medicamentos</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="miembro">Esposo(a)</td>
                            <td>
                                <select class="form-control input-sm">
                                    <option value="">Ninguna</option>
                                    <option value="E.T.S. (Enfermedades de transmisíon sexual)">E.T.S. (Enfermedades de transmisíon sexual)</option>
                                    <option value="Congénitas (de nacimiento)">Congénitas(de nacimiento)</option>
                                    <option value="Oncológica (cáncer)">Oncológica (cáncer)</option>
                                    <option value="Crónicas (Hipertensión,Diabetes)">Crónicas (Hipertensión,Diabetes)</option>
                                    <option value="Convulsiones (Epilepsia)">Convulsiones (Epilepsia)</option>
                                    <option value="Alergia a alimentos/medicamentos">Alergia a alimentos/medicamentos</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>       
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <small>Usted o un familiar directo (Padres,Hermanos,Tios) presenta alguna discapacidad?: especifique quien y que discapacidad</small>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Discapacidad:</label>
                            <select class="form-control input-sm" name="discapacidad" id="discapacidad">
                                <option value="Ninguna"selected>Ninguna</option>
                                <option value="Física">Física</option>
                                <option value="Mental">Mental</option>
                                <option value="Sensorial">Sensorial</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Especifique:</label>
                            <input type="text" class="form-control input-sm" id="especificacion_discapacidad" placeholder="Ejm: Padre,discapacidad">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset id="field_conadis">
                   
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="email">Cuenta con carné CONADIS:</label>
                                <select class="form-control input-sm" name="carne_conadis" id="carne_conadis">
                                    <option value="No"selected>No</option>
                                    <option value="Si">Si</option>
                                </select>
                                <em id="carne_conadis-error" class="error invalid-feedback"></em>
                            </div>
                        </div>
                        <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Nº:</label>
                                    <input type="text" class="form-control input-sm" id="nro_conadis">
                                    <em id="nro_conadis-error" class="error invalid-feedback"></em>
                                </div>
                            </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Resolucion Nº:</label>
                                <input type="text" class="form-control input-sm" id="resolucion_conadis">
                                <em id="nro_conadis-error" class="error invalid-feedback"></em>
                            </div>
                        </div>
                    </div>
            </fieldset>

        </div>
    </div>
    <fieldset id="embarazo">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">¿Actualmente estas gestando?:</label>
                        <select class="form-control input-sm"  id="embarazo">
                            <option value="No" selected >No</option>
                            <option value="Si">Si</option>
                        </select>
                        <em id="embarazo-error" class="error invalid-feedback"></em>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Meses de gestacion:</label>
                        <select class="form-control input-sm"  id="meses_embarazo">
                            <option value="" selected ></option>
                            <option value="1" >1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                        <em id="meses_embarazo_tipo-error" class="error invalid-feedback"></em>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <button type="button" id="return_tab3" class="btn btn-default"><i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true"></i> Volver</button>    
    <div class="pull-right">
            <button class="btn btn-primary"> Guardar y siguiente <i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></button>
    </div>
   
</form>
