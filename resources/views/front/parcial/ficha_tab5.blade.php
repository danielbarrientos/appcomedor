<h4 class="text-center">CARACTERÍSTICAS ACADÉMICAS</h4><hr>
<form action="{{url('fichaEstudiante/tab5')}}" method="POST" id="form_tab5" class="general" >  
    <fieldset>
      
        <legend>ANTECEDENTES ACADÉMICOS</legend> 
       
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="email">Escuela académico profesional:</label>
                    <input type="text" id="escuela" class="form-control input-sm" disabled>
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Ciclo de estudios (semestre):</label>
                    <select class="form-control input-sm" name="semestre" id="semestre" required>
                        <option value="">Seleccione</option>
                        <option value="Primero">Primero</option>
                        <option value="Segundo">Segundo</option>
                        <option value="Tercero">Tercero</option>
                        <option value="Cuarto">Cuarto</option>
                        <option value="Quinto">Quinto</option>
                        <option value="Sexto">Sexto</option>
                        <option value="Septimo">Septimo</option>
                        <option value="Octavo">Octavo</option>
                        <option value="Noveno">Noveno</option>
                        <option value="Décimo">Décimo</option>
                    </select>
                    <em id="semestre-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div>
        <div class="row">
                    <p><small><b>Nota: Dejar en blanco los campos referentes a promedios y creditos aprobados en caso de ser ingresante</b></small></p> 
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email"><small>Prom. ponderado penúltimo semestre</small></label>
                    <input type="number"  min="0.00" max="20.00" step="any" name="penultimo_promedio" id="penultimo_promedio" class="form-control input-sm" placeholder="Ingrese promedio" >
                    <em id="penultimo_promedio-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for=""><small>Prom. ponderado último semestre</small></label>
                    <input type="number" min="0.00" max="20.00" step="any"   name="ultimo_promedio" id="ultimo_promedio" class="form-control input-sm" placeholder="Ingrese promedio" >
                    <em id="ultimo_promedio-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for=""><small>Créditos aprobados último semestre:</small></label>
                    <input type="number" min="0" max="220" pattern="^[0-220]+" name="total_creditos" id="total_creditos" class="form-control input-sm" placeholder="Ingrese créditos aprobados del último semestre" >
                    <em id="total_creditos-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Modalidad de ingreso a la universidad:</label>
                    <select class="form-control input-sm" name="modalidad_ingreso" id="modalidad_ingreso" required>
                        <option value="">Seleccione</option>
                        <option value="Exámen ordinario">Exámen ordinario</option>
                        <option value="Exámen extraordinario">Exámen extraordinario</option>
                        <option value="Cpu">Cpu</option>
                        <option value="Deportistas calificados">Deportistas calificados</option>
                        <option value="Primeros puestos">Primeros puestos</option>
                        <option value="Víctimas del terrorismo">Víctimas del terrorismo</option>
                        <option value="Personas con discapacidad">Personas con discapacidad</option>
                        <option value="Otros">Otros</option>
                    </select>
                    <em id="modalidad_ingreso-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div> 
    </fieldset>
    <button type="button" id="return_tab4" class="btn btn-default"><i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true"></i> Volver</button>    
        <div class="pull-right">
                <button class="btn btn-primary"> GUARDAR Y FINALIZAR <i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></button>
        </div> 
</form>
<div class="row" id="pdf_ficha">
    <div class="col-md-12 text-center">
        <fieldset>
            <legend>DESCARGAR FICHA (OPCIONAL)</legend>
            <small> Su ficha ha sido llenada correctamente, <b>No</b> es necesario presentar la ficha en formato impreso. </small><br>
            <small>No olvide cerrar sesíon.</small><br>
            <a href="{{url('fichaEstudiante/pdf')}}" target="_blank" class="btn btn-lg btn-default" ><i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Ver ficha </a> 
        </fieldset>
                
    </div>    
</div>
    
    