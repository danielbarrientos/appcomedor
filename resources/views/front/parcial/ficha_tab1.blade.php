<h4 class="text-center">IDENTIFICACIÓN DEL ESTUDIANTE</h4><hr>
    <form action="{{url('/fichaEstudiante/tab1')}}" method="POST" id="form_tab1" class="general">
    <fieldset>
        <legend>DATOS PERSONALES</legend>
        <div class="row">
                <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Apellidos:</label>
                                    <input type="text" class="form-control input-sm" name="apellidos" id="apellidos" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Nombres:</label>
                                    <input type="text" class="form-control input-sm" name="nombres" id="nombres" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Dni:</label>
                                    <input type="text" class="form-control input-sm" name="dni" id="dni" disabled>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="pwd">Nº Celular:</label>
                                    <input type="number"  class="form-control input-sm" minlength="9" name="nro_celular" maxlength="9" id="nro_celular" placeholder="Nro celular" required>
                                    <em id="nro_celular-error" class="error invalid-feedback"></em> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="pwd">Correo electrónico:</label>
                                    <input type="email" class="form-control input-sm" name="email" id="email" placeholder=" email (correo electrónico)" required>
                                    <em id="email-error" class="error invalid-feedback"></em>
                                </div>
                            </div>
                        </div>
                </div>
            </div>  
            
    </fieldset>
    <fieldset>
        <legend>EN CASO DE EMERGENCIA LLAMAR A:</legend>
            
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label for="email">Apellidos y Nombres:</label>
                        <input type="text" class="form-control input-sm" maxlength="50" name="nombre_contacto" id="nombre_contacto" placeholder="Nombre apellido de contacto" required>
                        <em id="nombre_contacto-error" class="error invalid-feedback"></em>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pwd">Teléfono:</label>
                        <input type="number" class="form-control input-sm"  maxlength="9" name="telefono_contacto" id="telefono_contacto" placeholder="  teléfono de contacto" required>
                        <em id="telefono_contacto-error" class="error invalid-feedback"></em>
                    </div>
                </div>
            </div>  
                   
    </fieldset>
    <fieldset>
        <legend>UBICACIÓN DEL DOMICILIO EN ABANCAY:</legend>
        
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Distrito:</label>
                        <select class="form-control input-sm" name="distrito_vivienda" id="distrito_vivienda" required>
                        </select>
                        <em id="distrito_vivienda-error" class="error invalid-feedback"></em>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Poblado /Anexo:</label>
                        <input type="text" class="form-control input-sm" name="anexo_vivienda" id="anexo_vivienda" placeholder="Poblado anexo" required>
                        <em id="anexo_vivienda-error" class="error invalid-feedback"></em>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Dirección:</label>
                        <input type="text" class="form-control input-sm" name="direccion_vivienda" id="direccion_vivienda" placeholder=" Dirección" required>
                        <em id="direccion_vivienda-error" class="error invalid-feedback"></em>
                    </div>
                </div>
            </div>       
    </fieldset>
       
        <div class="pull-right">
                <button class="btn btn-primary"> Guardar y siguiente <i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></button>
        </div>
    </form>