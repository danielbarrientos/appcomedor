<h4 class="text-center">CARACTERÍSTICAS DE VIVIENDA</h4><hr>
    <form action="{{url('fichaEstudiante/tab3')}}" method="POST" id="form_tab3"  class="general">
    <fieldset>
        <legend>LA VIVIENDA EN ABANCAY, ACTUALMENTE ES:</legend>
        
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="email">Tenencia:</label>
                        <select class="form-control input-sm" name="vivienda_tenencia" id="vivienda_tenencia" required>
                            <option value="" selected disabled><- Seleccione -></option>
                            <option value="Propia">Propia</option>
                            <option value="Alquilada">Alquilada</option>
                            <option value="Familiares">Familiares</option>
                            <option value="Otros">Otros</option>
                        </select>
                        <em id="vivienda_tenencia-error" class="error invalid-feedback"></em>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pwd">Material:</label>
                        <select class="form-control input-sm" name="vivienda_material" id="vivienda_material" required>
                            <option value="" selected disabled><- Seleccione -></option>
                            <option value="Noble">Noble</option>
                            <option value="Adobe">Adobe</option>
                            <option value="Madera">Madera</option>
                            <option value="Otros">Otros</option>
                        </select>
                        <em id="vivienda_material-error" class="error invalid-feedback"></em>
                    </div>
                </div>
            
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="email">Nº de habitaciones:</label>
                        <select class="form-control input-sm" name="nro_habitaciones" id="nro_habitaciones" required>
                            <option value="" selected disabled><- Seleccione -></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4 o más">4 o más</option>
                        </select>
                        <em id="nro_habitaciones-error" class="error invalid-feedback"></em>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pwd">Tipo:</label>
                        <select class="form-control input-sm" name="vivienda_tipo" id="vivienda_tipo" required>
                            <option value="" selected disabled><- Seleccione -></option>
                            <option value="Casa independiente">Casa independiente</option>
                            <option value="Departamento">Departamento</option>
                            <option value="Quinta">Quinta</option>
                            <option value="Cuarto solo">Cuarto solo</option>
                        </select>
                        <em id="vivienda_tipo-error" class="error invalid-feedback"></em>
                    </div>
                </div>
               
            </div>  
    </fieldset>
    <fieldset>
        <legend>SERVICIOS CON QUE CUENTA LA VIVIENDA:</legend>
        
            <div class="row" id="content_servicios">
                <div class="col-md-2">
                    <label for="Agua" style="font-weight:400"><input  type="checkbox"   value="Agua" id="Agua" name="servicios_vivienda[]"> Agua</label>
                </div>
                <div class="col-md-2">
                    <label for="Desague" style="font-weight:400"><input  type="checkbox"   value="Desague" id="Desague" name="servicios_vivienda[]"> Desague</label>
                </div>
                <div class="col-md-2">
                    <label for="Luz_electrica" style="font-weight:400"><input  type="checkbox"   value="Luz eléctrica" id="Luz_electrica" name="servicios_vivienda[]"> Luz eléctrica</label>
                </div>
                <div class="col-md-3">
                    <label for="Alumbrado_publico" style="font-weight:400"><input  type="checkbox"   value="Alumbrado público" id="Alumbrado_publico" name="servicios_vivienda[]"> Alumbrado público</label>
                </div>
                <div class="col-md-2">
                    <label for="Telefono_fijo" style="font-weight:400"><input  type="checkbox"   value="Teléfono fijo" id="Telefono_fijo" name="servicios_vivienda[]"> Teléfono fijo</label>
                </div>
                <div class="col-md-2">
                    <label for="Tv_cable" style="font-weight:400"><input  type="checkbox"   value="Tv cable" id="Tv_cable" name="servicios_vivienda[]"> Tv cable</label>
                </div>
                <div class="col-md-2">
                    <label for="Internet" style="font-weight:400"><input  type="checkbox"   value="Internet" id="Internet" name="servicios_vivienda[]"> Internet</label>
                </div>
            </div>          
            
    </fieldset>
    <fieldset>
        <legend>USTED POSEE:</legend>
        <div class="row" id="content_posesiones">
                <div class="col-md-2">
                    <label for="tv_color_plasma"  style="font-weight:400"><input type="checkbox"   value="Tv a color LCD plasma" name="posesiones[]" id="tv_color_plasma" > Tv a color LCD plasma</label>
                    <label for="cocina_electrica"  style="font-weight:400"><input type="checkbox"   value="Cocina eléctrica" name="posesiones[]" id="cocina_electrica" > Cocina eléctrica</label>
                    <label for="silla_mesa"  style="font-weight:400"><input type="checkbox"   value="Silla/Mesa" name="posesiones[]" id="silla_mesa" > Silla/Mesa</label>
                </div>
                <div class="col-md-2">
                    <label for="tv_blanco_negro"  style="font-weight:400"><input type="checkbox"   value="Tv blanco y negro" name="posesiones[]" id="tv_blanco_negro" > Tv blanco y negro</label>
                    <label for="mini_componente"  style="font-weight:400"><input type="checkbox"   value="Mini componente" name="posesiones[]" id="mini_componente" > Mini componente</label>
                    <label for="celular"  style="font-weight:400"><input type="checkbox"   value="Celular" name="posesiones[]" id="celular" > Celular</label>
                </div>
                <div class="col-md-2">
                    <label for="plancha_electrica"  style="font-weight:400"><input type="checkbox"   value="Plancha eléctrica" name="posesiones[]" id="plancha_electrica" > Plancha eléctrica</label>
                    <label for="horno_microondas"  style="font-weight:400"><input type="checkbox"   value="Horno microondas" name="posesiones[]" id="horno_microondas" > Horno microondas</label>
                    <label for="cocina_gas"  style="font-weight:400"><input type="checkbox"   value="Cocina gas" name="posesiones[]" id="cocina_gas" > Cocina gas</label>
                </div>
                <div class="col-md-2">
                    <label for="aspiradora"  style="font-weight:400"><input type="checkbox"   value="Aspiradora" name="posesiones[]" id="aspiradora" > Aspiradora</label>
                    <label for="computadora"  style="font-weight:400"><input type="checkbox"   value="Computadora" name="posesiones[]" id="computadora" > Computadora</label>
                    <label for="ipod"  style="font-weight:400"><input type="checkbox"   value="Ipod" name="posesiones[]" id="ipod" > Ipod</label>
                </div>
                <div class="col-md-2">
                    <label for="refrigeradora"  style="font-weight:400"><input type="checkbox"   value="Refrigeradora" name="posesiones[]" id="refrigeradora" > Refrigeradora</label>
                    <label for="impresora"  style="font-weight:400"><input type="checkbox"   value="Impresora" name="posesiones[]" id="impresora" > Impresora</label>
                    <label for="cama"  style="font-weight:400"><input type="checkbox"   value="Cama" name="posesiones[]" id="cama"> Cama</label>
                </div>
            </div>
    </fieldset>
    <button type="button" id="return_tab2" class="btn btn-default"><i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true"></i> Volver</button>    
    <div class="pull-right">
            <button class="btn btn-primary"> Guardar y siguiente <i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></button>
    </div>
</form>