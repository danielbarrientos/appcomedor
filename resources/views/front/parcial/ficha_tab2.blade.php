<h4 class="text-center">CARACTERÍSTICAS GENERALES</h4><hr>
    <form action="{{url('fichaEstudiante/tab2')}}" method="POST" id="form_tab2"  class="general">
    <fieldset>
        <legend></legend>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Sexo:</label>
                    <select class="form-control input-sm" name="sexo" id="sexo" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        <option value="M">Masculino</option>
                        <option value="F">Femenino</option>
                    </select>
                    <em id="sexo-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pwd">Fecha de nacimiento:</label>
                    <input type="date" class="form-control input-sm" name="fecha_nacimiento" id="fecha_nacimiento" required>
                    <em id="fecha_nacimiento-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pwd">Estado civil:</label>
                    <select class="form-control input-sm" name="estado_civil" id="estado_civil" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        <option value="Soltero">Soltero</option>
                        <option value="Casado">Casado</option>
                        <option value="Conviviente">Conviviente</option>
                        <option value="Divorciado">Divorciado</option>
                        <option value="Separado">Separado</option>
                        <option value="Viudo">Viudo</option>
                    </select>
                    <em id="estado_civil-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>LUGAR DE NACIMIENTO:</legend>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="email">Departamento:</label>
                    <select class="form-control input-sm"  style="width: 100%;" name="departamento_nacimiento" id="departamento_nacimiento" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        @foreach ($departamentos as $departamento)
                            <option value="{{$departamento->id_departamento}}">{{$departamento->nombre}}</option>
                        @endforeach
                    </select>
                    <em id="departamento_nacimiento-error" class="error invalid-feedback"></em>
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Provincia:</label>
                    <select class="form-control input-sm" style="width: 100%;" name="provincia_nacimiento" id="provincia_nacimiento" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        
                    </select>
                    <em id="provincia_nacimiento-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Distrito:</label>
                    <select class="form-control input-sm" style="width: 100%;" name="distrito_nacimiento" id="distrito_nacimiento" required>
                            <option value="" selected disabled><- Seleccione -></option>  
                    </select>
                    <em id="distrito_nacimiento-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Comunidad/Anexo:</label>
                    <input type="text" class="form-control input-sm " name="comunidad_nacimiento" id="comunidad_nacimiento" placeholder="Comunidad/Anexo de nacimiento">
                    <em id="comunidad_nacimiento-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>LUGAR DE PROCEDENCIA: <small>(Indicar el lugar de donde proviene antes de ingresar a la UNAMBA)</small></legend>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="email">Departamento:</label>
                    <select class="form-control input-sm" style="width: 100%;" name="departamento_procedencia" id="departamento_procedencia" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        @foreach ($departamentos as $departamento)
                            <option value="{{$departamento->id_departamento}}">{{$departamento->nombre}}</option>
                        @endforeach
                    </select>
                    <em id="departamento_procedencia-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Provincia:</label>
                    <select class="form-control input-sm" style="width: 100%;" name="provincia_procedencia" id="provincia_procedencia" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        
                    </select>
                    <em id="provincia_procedencia-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Distrito:</label>
                    <select class="form-control input-sm" style="width: 100%;" name="distrito_procedencia" id="distrito_procedencia" required>
                            <option value="" selected disabled><- Seleccione -></option>    
                    </select>
                    <em id="distrito_procedencia-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="pwd">Comunidad/Anexo:</label>
                    <input type="text" class="form-control input-sm " name="comunidad_procedencia" id="comunidad_procedencia" placeholder="Comunidad/Anexo de procedencia">
                    <em id="comunidad_procedencia-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend></legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Idioma o lengua con el que aprendio a hablar:</label>
                    <select class="form-control input-sm" name="primer_idioma" id="primer_idioma" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        <option value="Castellano">Castellano</option>
                        <option value="Quechua">Quechua</option>
                        <option value="Aymara">Aymara</option>
                        <option value="Otro">Otro</option>
                    </select>
                    <em id="primer_idioma-error" class="error invalid-feedback"></em>    
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pwd">Limitacion Física:</label>
                    <select class="form-control input-sm" name="limitacion_fisica" id="limitacion_fisica">
                        <option value="" selected disabled><- Seleccione -></option>
                        <option value="Para ver">Para ver</option>
                        <option value="Para oír">Para oír</option>
                        <option value="Para hablar">Para hablar</option>
                        <option value="Para usar extremidades">Para usar extremidades</option>
                        <option value="Otra Dificultad">Otra dificultad</option>
                        <option value="Ninguna">Ninguna</option>
                    </select>
                    <em id="limitacion_fisica-error" class="error invalid-feedback"></em>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pwd">Con quien vive usted:</label>
                    <select class="form-control input-sm" name="vive_con" id="vive_con" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        <option value="Padres">Padres</option>
                        <option value="Tios">Tios</option>
                        <option value="Abuelos">Abuelos</option>
                        <option value="Hermanos">Hermanos</option>
                        <option value="Independiente">Independiente</option>
                    </select>
                    <em id="vive_con-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pwd"> <small>Tiempo de desplazamiento:(De su domicilio a la Universidad horas:minutos)</small></label>
                    <div class="row">
                        <div class="col-md-5">
                            <input type="number" min="0" max="23" class="form-control input-sm" placeholder="horas"  name="tiempo_horas" id="tiempo_horas" required>
                        </div>
                        <div class="col-md-5">
                            <input type="number" min="2" max="59" class="form-control input-sm" placeholder="minutos" name="tiempo_minutos"  id="tiempo_minutos" required>
                        </div>
                    </div>
                       
                    <em id="tiempo_horas-error" class="error invalid-feedback"></em>
                    <em id="tiempo_minutos-error" class="error invalid-feedback"></em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pwd">Medio de transporte:<small>(Que usa con más frecuencia para ir a la Universidad)</small></label>
                    <select class="form-control input-sm" name="medio_transporte" id="medio_transporte" required>
                        <option value="" selected disabled><- Seleccione -></option>
                        <option value="Vehículo propio">Vehículo propio</option>
                        <option value="Vehículo de amigos">Vehículo de amigos</option>
                        <option value="Transporte público">Transporte público</option>
                        <option value="Transporte de la universidad">Transporte de la universidad</option>
                        <option value="Bicicleta">Bicicleta</option>
                        <option value="A pie">A pie</option>
                        <option value="Motocicleta">Motocicleta</option>
                        <option value="Otros">Otros</option>
                    </select>
                    <em id="medio_transporte-error" class="error invalid-feedback"></em>
                </div>
            </div>
        </div>
    </fieldset>
    <button type="button" id="return_tab1" class="btn btn-default"><i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true"></i> Volver</button>    
    <div class="pull-right">
            <button class="btn btn-primary"> Guardar y siguiente <i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></button>
    </div>
    </form>