
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .titulo{
            text-align: center;
            font-size: 12px;
        } 
        .seccion{
            font-size: 11px;
            font-weight: bold;
        }
        .table {
            width: 100%;
            border-collapse: collapse;
            padding-left: 10px; 
            padding-top: 0px;
            border-top: 0px;
         }
        .table td {
            
            text-align: left;
            vertical-align: top;
            border: 0.3px solid #000;
            border-collapse: collapse;
            font-size: 10px;
            padding: 0.2em;
            
         } 
         .bg-gray{
             background-color: rgba(236, 240, 238, 0.651);
         }
        .numeracion{
            font-size: 10px;
            font-weight: bold;
            padding-left: 10px;
            padding-bottom: 0px;
            border-bottom: 0px;
            margin-bottom: 2px;
        }
        .respuesta{
            font-weight: normal;
            font-size: 10px;
        }
       
    </style>
</head>
<body>
   
    <table>
        <tr>
            <td width="210px;">
                <img src="{{asset('img/brand/unamba.png')}}" alt="logo_unamba" style="max-width: 120px; max-height: 100px" >
            </td>
            <td class="titulo"> UNIVERSIDAD NACIONAL MICAELA BASTIDAS DE APURÍMAC <br> DIRECCIÓN DE BIENESTAR UNIVERSITARIO</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="titulo">FICHA SOCIOECONOMICA</td>
        </tr>
    </table>
    <p class="seccion">I. IDENTIFICACIÓN DEL ESTUDIANTE</p>
    <p class="numeracion">1. DATOS PERSONALES</p>
    <table class="table" style="margin-bottom: 5px;">
        <tr>
            <td style="width: 120px; " class="bg-gray">APELLIDOS: </td>
            <td style="letter-spacing: 1px;">{{$estudiante->apellidos}}</td>
        </tr>
        <tr>
            <td  class="bg-gray">NOMBRES: </td>
            <td style="letter-spacing: 1px;">{{$estudiante->nombres}}</td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <td style=" text-align: center;" class="bg-gray">DNI Nº</td>
            <td style=" text-align: center;" class="bg-gray">Nº CELULAR</td>
            <td style=" text-align: center;" class="bg-gray">CORREO ELECTRÓNICO</td>
        </tr>  
        <tr>
            <td style="text-align: center;">{{$estudiante->dni}}</td>
            <td style="text-align: center;">{{$estudiante->nro_celular}}</td>
            <td style="text-align: center;">{{$estudiante->email}}</td>
        </tr>
    </table>
   <table class="table" style="margin-top: 5px; padding-left: 203px;">
        <tr>
            <td style="width: 70%; text-align: center;" class="bg-gray">APELLIDOS Y NOMBRES</td>
            <td style="width: 30%; text-align: center;" class="bg-gray">TELÉFONO</td>
        </tr>
   </table>
  
   <table class="table">
        <tr>
            <td style="width: 27%;" class="bg-gray">EN CASO DE EMERGENCIA LLAMAR A:</td>
            <td style="width: 51%; text-align: center;">{{$ficha['contacto']->nombre_contacto}}</td>
            <td style="text-align: center;">{{$ficha['contacto']->telefono_contacto}}</td>
        </tr>
   </table>
   <p class="numeracion">2. UBICACIÓN DEL DOMICILIO EN ABANCAY</p>
   <table class="table" >
        <tr>
            <td style=" text-align: center;" class="bg-gray">DISTRITO</td>
            <td style=" text-align: center;" class="bg-gray">POBLADO ANEXO URB. AAHH.</td>
            <td style=" text-align: center;" class="bg-gray">DIRECCIÓN</td>
        </tr>  
        <tr>
            <td style="text-align: center;">{{buscar_ubigeo('distrito',$ficha['ubicacion_domicilio']->id_distrito)}}</td>
            <td style="text-align: center;">{{$ficha['ubicacion_domicilio']->anexo_vivienda}}</td>
            <td style="text-align: center;">{{$ficha['ubicacion_domicilio']->direccion_vivienda}}</td>
        </tr>
    </table>

    <p class="seccion">II. CARACTERÍSTICAS GENERALES DEL ESTUDIANTE</p>
    <span class="numeracion" style="margin-right: 50px;"> 3. SEXO: <span class="respuesta">{{$estudiante->sexo}}</span> </span>
    <span class="numeracion" style="margin-left: 100px;"> 4. FECHA DE NACIMIENTO:  <span class="respuesta">{{date("d/m/Y", strtotime($estudiante->fecha_nacimiento))}}</span></span>
    <span class="numeracion" style="margin-left: 100px;"> 5. ESTADO CIVIL: <span class="respuesta">{{$estudiante->estado_civil}}</span></span>

    <p class="numeracion">6. LUGAR DE NACIMIENTO</p>
    <table class="table" >
         <tr>
             <td style=" text-align: center;" class="bg-gray">DEPARTAMENTO</td>
             <td style=" text-align: center;" class="bg-gray">PROVINCIA</td>
             <td style=" text-align: center;" class="bg-gray">DISTRITO</td>
             <td style=" text-align: center;" class="bg-gray">COMUNIDAD/ANEXO</td>
         </tr>  
         <tr>
             <td style="text-align: center;">{{buscar_ubigeo('departamento',$ficha['lugar_nacimiento']->id_departamento)}}</td>
             <td style="text-align: center;">{{buscar_ubigeo('provincia',$ficha['lugar_nacimiento']->id_provincia)}}</td>
             <td style="text-align: center;">{{buscar_ubigeo('distrito',$ficha['lugar_nacimiento']->id_distrito)}}</td>
             <td style="text-align: center;">{{$ficha['lugar_nacimiento']->comunidad}}</td>
         </tr>
     </table>
     <p class="numeracion">PROCEDENCIA <small>(Lugar de donde proviene antes de ingresar a la UNAMBA)</small></p>
     <table class="table" >
        <tr>
            <td style=" text-align: center;" class="bg-gray">DEPARTAMENTO</td>
            <td style=" text-align: center;" class="bg-gray">PROVINCIA</td>
            <td style=" text-align: center;" class="bg-gray">DISTRITO</td>
            <td style=" text-align: center;" class="bg-gray">COMUNIDAD/ANEXO</td>
        </tr>  
        <tr>
            <td style="text-align: center;">{{buscar_ubigeo('departamento',$ficha['lugar_procedencia']->id_departamento)}}</td>
            <td style="text-align: center;">{{buscar_ubigeo('provincia',$ficha['lugar_procedencia']->id_provincia)}}</td>
            <td style="text-align: center;">{{buscar_ubigeo('distrito',$ficha['lugar_procedencia']->id_distrito)}}</td>
            <td style="text-align: center;">{{$ficha['lugar_procedencia']->comunidad}}</td>
        </tr>
    </table>
   
    <span class="numeracion">7. IDIOMA O LENGUA CON EL QUE APRENDIO A HABLAR: <span class="respuesta">{{$ficha['primer_idioma']}}</span></span>
    <br>
  
    <span class="numeracion">8. DIFICULTAD O LIMITACIÓN: <span class="respuesta">{{$ficha['limitacion_fisica']}}</span></span>
    <br>
   
    <span class="numeracion">9. CON QUIEN VIVE USTED: <span class="respuesta">{{$ficha['vive_con']}}</span></span>
    <br>
    <span class="numeracion">10. CUAL ES EL TIEMPO DE DESPLAZAMIENTO DE SU DOMICILIO A LA UNIVERSIDAD: <span class="respuesta">{{$ficha['transporte']->tiempo_horas}} horas y {{$ficha['transporte']->tiempo_minutos}} minutos </span></span>
    <br>
    
    <span class="numeracion">11. MEDIO DE TRANSPORTE QUE USA CON MÁS FRECUENCIA PARA IR A LA UNIVERSIDAD: <span class="respuesta">{{$ficha['transporte']->medio_transporte}}</span></span>
    <br>
    
    <p class="seccion">III. CARACTERÍSTICAS DE LA VIVIENDA</p>

    <p class="numeracion">12. LA VIVIENDA EN ABANCAY, ACTUALMENTE ES:</p>
    <table class="table" >
         <tr>
             <td style=" text-align: center;" class="bg-gray">TENENCIA</td>
             <td style=" text-align: center;" class="bg-gray">MATERIAL</td>
             <td style=" text-align: center;" class="bg-gray">Nº DE HABITACIONES</td>
             <td style=" text-align: center;" class="bg-gray">TIPO</td>
         </tr>  
         <tr>
             <td style="text-align: center;">{{$ficha['vivienda']->tenencia}}</td>
             <td style="text-align: center;">{{$ficha['vivienda']->material}}</td>
             <td style="text-align: center;">{{$ficha['vivienda']->nro_habitaciones}}</td>
             <td style="text-align: center;">{{$ficha['vivienda']->tipo}}</td>
         </tr>
     </table>
     <span class="numeracion">13. SERVICIOS CON QUE CUENTA LA VIVIENDA: </span>
    
     <table class="table">
         <tr>
             <td class="bg-gray" style="width: 90px;">SERVICIOS:</td>
             <td>{{$ficha['servicios_vivienda']}}</td>
         </tr>
         <tr>
             <td class="bg-gray" style="width: 90px;">UDTED POSEE:</td>
             <td>{{$ficha['posesiones']}}</td>
         </tr>
     </table>
    
 
     <p class="seccion">IV.DATOS DEL HOGAR</p>
     <p class="numeracion">14. COMPOSICION FAMILIAR: <span class="respuesta"><small>Hermanos que actualmente dependen de los padres de usted, incluyase si es el caso</small></span></p>
     <table class="table ">
           
                <tr>
                    <td class="bg-gray">PARENTESCO</td>
                    <td class="bg-gray">NOMBRE Y APELLIDO</td>
                    <td class="bg-gray">EDAD</td>
                    <td class="bg-gray">DNI</td>
                    <td class="bg-gray">ESTADO CIVIL</td>
                    <td class="bg-gray">CENTRO TRABAJO/ESTUDIO</td>
                    <td class="bg-gray">GRADO DE INSTRUCCION</td>     
                </tr>
            
           
                @if (! empty($ficha['composicion_familiar']))
                @foreach ($ficha['composicion_familiar'] as $item)
                <tr>
                    <td>{{$item->parentesco}}</td>
                    <td>{{$item->nombre}}</td>
                    <td >{{$item->edad}}</td>
                    <td >{{$item->dni}}</td>
                    <td>{{$item->estado_civil}}</td>
                    <td>{{$item->centro_trabajo}}</td>
                    <td>{{$item->grado_instruccion}}</td>
                   
                </tr>
            @endforeach      
            @endif
                    
          
        </table>  

        <span class="numeracion">15. NUCLEO FAMILIAR: <span class="respuesta"><small>Llenar si es casado, conviviente,con hijos u otro familiar dependiente de usted.</small></span></span>
        <table class="table ">
              
                   <tr>
                       <td class="bg-gray">NOMBRE Y APELLIDO</td>
                       <td class="bg-gray">EDAD</td>
                       <td class="bg-gray">DNI</td>
                       <td class="bg-gray">ESTADO CIVIL</td>
                       <td class="bg-gray">CENTRO TRABAJO/ESTUDIO</td>
                       <td class="bg-gray">GRADO DE INSTRUCCION</td>     
                   </tr>
               
              
                   @if (! empty($ficha['nucleo_familiar']))
                   @foreach ($ficha['nucleo_familiar'] as $item)
                   <tr>
                       <td>
                               {{$item->nombre}}
                       </td>
                       <td >
                               {{$item->edad}}
                       </td>
                       <td >
                               {{$item->dni}}
                       </td>
                       <td>
                               {{$item->estado_civil}}
                       </td>
                       <td>
                               {{$item->centro_trabajo}}
                       </td>
                       <td>
                               {{$item->grado_instruccion}}
                       </td>
                      
                   </tr>
               @endforeach      
               @endif
           </table>
    <span class="numeracion">16. SITUACIÓN FAMILIAR:</span>

    <table class="table">
        <tr>
            <td class="bg-gray" style="width:250px;">SITUACIÓN DE LOS PADRES:</td>
            <td>{{$ficha['situacion_familiar']->situacion_padres}}</td>
            <td class="bg-gray">ORFANDAD:</td>
            <td>{{$ficha['situacion_familiar']->situacion_orfandad}}</td>
        </tr>
        <tr>
            <td class="bg-gray" style="width:250px;">VICTIMA DE VIOLENCIA POLÍTICA (TERRORISMO): </td>
            <td>{{$ficha['violencia_politica']->victima_terrorismo}}</td>
            <td class="bg-gray">RESOLUCIÓN:</td>
            <td>{{$ficha['violencia_politica']->resolucion_terrorismo}}</td>
        </tr>
        
    </table>
   
    <p class="numeracion">17. ALIMENTACION DEL ESTUDIANTE: <small class="respuesta">Lugar donde toma sus alimentos</small></p>
    <table class="table">
        <tr>
            <td class="bg-gray" style="text-align: center;">DESAYUNO</td>
            <td class="bg-gray" style="text-align: center;">ALMUERZO</td>
            <td class="bg-gray" style="text-align: center;">CENA</td>
        </tr>
        <tr >
           
            <td style="text-align: center;">{{$ficha['alimentacion']->desayuno}}</td>
         
            <td style="text-align: center;">{{$ficha['alimentacion']->almuerzo}}</td>
            
            <td style="text-align: center;">{{$ficha['alimentacion']->cena}}</td>
        </tr>
    </table>
   

    <p class="numeracion">18. SITUACION ECONOMICA FAMILIAR:</p>
    <table class="table">
        <tr>
            <td class="bg-gray" style="width: 70px; ">DEPENDE:</td>
            <td>{{$ficha['economia_familiar']->dependencia_economica}}</td>
            <td class="bg-gray" style="width: 150px;">MONTO QUE PERCIBE MES S/.:</td>
            <td>{{$ficha['economia_familiar']->ingreso_familiar}}</td>
        </tr>
    </table>
   
    <p class="numeracion"> SITUACION ECONOMICA DEL ESTUDIANTE: <small>(Señale sus gastos)</small></p>
    <table class="table" >
        <tr>
            <td class="bg-gray" >Nº</td>
            <td class="bg-gray" style="text-align: center;">CONCEPTO</td>
            <td class="bg-gray" style="text-align: center;">MONTO</td>
        </tr>    
        @php
            $total = 0;
        @endphp
        @foreach ($ficha['gastos'] as $item)
            @php
                $total += $item->monto;
            @endphp
            <tr>
                <td>{{$item->nro}}</td>
                <td>{{$item->concepto}}</td>
                <td style="text-align: right;">{{$item->monto}}</td>
            </tr>
        @endforeach
        <tr>
            <td  class="bg-gray"></td>
            <td class="bg-gray" style="text-align: right;">TOTAL MES S/.</td>
            <td  class="bg-gray" style="text-align: right;">{{$total}}</td>
        </tr>
    </table>

    <p class="numeracion">19. SITUACION DE SALUD DEL ESTUDIANTE Y DE LA FAMILIA: <small class="respuesta">¿Actualmente se encuentra afiliado a algún tipo de seguro de salud?</small></p>
    <table class="table">
        <tr>
            <td class="bg-gray" style="width: 100px;">SEGURO:</td>
            <td>{{$ficha['seguro_salud']->seguro_salud}}</td>
            <td class="bg-gray" style="width: 100px;">ESPECIFIQUE:</td>
            <td>{{$ficha['seguro_salud']->especificacion_salud}}</td>
        </tr>
    </table>
  

    <p class="numeracion">20. ¿USTED O ALGÚN MIEMBRO DE SU FAMILIA PRESENTA ALGUNA ENFERMEDAD?: <small class="respuesta">¿Actualmente se encuentra afiliado a algún tipo de seguro de salud?</small></p>
    <table class="table" >
        <tr>
            <td style=" text-align: center;" class="bg-gray">MIEMBRO DE FAMILIA</td>
            <td style=" text-align: center;" class="bg-gray">ENFERMEDAD</td>
        </tr>  
        @foreach ($ficha['salud_familia'] as $item)
            <tr>
                <td>{{$item->miembro}}</td>
                <td>{{$item->enfermedad}}</td>
            </tr>
        @endforeach
    </table>

    <p class="numeracion">21. ¿USTED O ALGÚN FAMILIAR DIRECTO(PADRES,HERMANOS,TIOS) PRESENTA ALGUNA DISCAPACIDAD?: <small class="respuesta">Especifique quien y que discapacidad.</small></p>
    <table class="table">
        <tr>
            <td class="bg-gray" style="width: 160px;">DISCAPACIDAD:</td>
            <td>{{$ficha['discapacidad_familar']->discapacidad}}</td>
            <td class="bg-gray" style="width: 80px;">ESPECIFIQUE:</td>
            <td colspan="3">{{$ficha['discapacidad_familar']->especificacion_discapacidad}}</td>
        </tr>
        <tr>
            <td class="bg-gray" style="width: 160px;">CUENTA CON CARNÉ CONADIS:</td>
            <td>{{$ficha['discapacidad_familar']->carne_conadis}}</td>
            <td class="bg-gray" style="width: 80px;"> Nº :</td>
            <td>{{$ficha['discapacidad_familar']->nro_conadis}}</td>
            <td class="bg-gray" style="width: 85px;"> RESOLUCIÓN Nº :</td>
            <td>{{$ficha['discapacidad_familar']->resolucion_conadis}}</td>
        </tr>
    </table>
    <br>
    @if ($estudiante->sexo =='F')
        <span class="numeracion">22. ¿ACTUALMENTE ESTAS GESTANDO?: <small class="respuesta">{{$ficha['embarazo']->embarazo}}</small></span>
        <span class="numeracion" style="margin-left: 12px; margin-right: 50px;">  EDAD GESTACIONAL EN MESES: <span class="respuesta">{{$ficha['embarazo']->meses_embarazo}}</span> </span>     
    @endif
   
   
    <p class="seccion">V. CARACTERÍSTICAS ACADÉMICAS</p>
        <p class="numeracion">23. ANTECEDENTES ACADÉMICOS:</p>
        <table class="table">
            <tr>
                <td class="bg-gray" style="width: 220px;">ESCUELA ACADÉMICO PROFESIONAL:</td>
                <td colspan="3">{{$estudiante->escuela->nombre}}</td>
            </tr>
            <tr>
                <td class="bg-gray" style="width: 220px;">CICLO DE ESTUDIOS:</td>
                <td>{{$ficha['antecedente_academico']->semestre}}</td>
                <td class="bg-gray" style="width: 220px;">CREDITOS APROBADOS ÚLTIMO SEMESTRE:</td>
                <td>{{$ficha['antecedente_academico']->total_creditos}}</td>
            </tr>
            <tr>
                <td class="bg-gray"> PROM. PONDERADO PENULTIMO SEMESTRE:</td>
                <td>{{$ficha['antecedente_academico']->penultimo_promedio}}</td>
                <td class="bg-gray"> PROM. PONDERADO ÚLTIMO SEMESTRE:</td>
                <td>{{$ficha['antecedente_academico']->ultimo_promedio}}</td>
            </tr>
            
        </table>     
</body>
</html>