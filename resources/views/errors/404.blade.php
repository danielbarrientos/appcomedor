<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>CoreUI Free Bootstrap Admin Template</title>
    <!-- Icons-->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
  </head>
  <body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <div class="clearfix">
            <h1 class="float-left display-3 mr-4 text-primary">404</h1>
            <h3 class="pt-3">Oops! Estas perdido.</h3>
            <p class="text-muted">La página que intentas solicitar no existe.</p>
          </div>
          <div class="input-prepend input-group">
            <a href="{{URL::previous()}}" class="btn btn-lg btn-primary btn-block "> Volver 
              
            </a>
           
           
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
   <script src="{{asset('js/jquery.min.js')}}"></script>
  
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
  </body>
</html>