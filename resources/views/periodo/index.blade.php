@extends('layouts.coreui')
  @section('breadcrumb')

    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Periodos de atención</li>

@endsection
 @section('content')

<div class="row">
  <div class="col-md-12 ">
    <div class="card">
      <div class="card-header"> <i class="fa fa-list">
        </i> Periodos de Atención ({{Session::get('nombre_semestre')}})
      </div>
      <div class="card-body">
        @if (@$periodos)
                <div class="row">
          <div class="col-md-6">
            <div class="pull-left">
              <a href="#" class="btn btn-primary btn-sm  " id="btnAbrirModal" data-toggle="modal" data-target="#modalCrear">
                <span class="fa fa-plus"></span> Nuevo registro
              </a>
              <a href="{{url('/periodoAtencion')}}" class="btn btn-light btn-sm">
                <span class="fa fa-refresh"></span> Refrescar página
              </a>
             </div>
           </div>
          <div class="col-md-6">
            <form class="navbar-form " method="GET" action="{{url('/periodoAtencion')}}" >
                <div class="input-group mb-3">
                  <input type="text" class="form-control form-control-sm" placeholder="fecha de inicio o fecha de fin" name="parametro" value="{{@$parametro}}">
                  <div class="input-group-append">
                    <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button>
                  </div>
                </div>
           </form>
          </div>
        </div><!--row-->

        <div class="table-responsive ">

          <table class="table table-striped table-sm table-hover" >
            <thead >
              <tr>
                <th>ID</th>
                <th>Fecha inicio</th>
                <th>Fecha fin</th>
                <th>Precio desayuno</th>
                <th>Precio almuerzo</th>
                <th>Precio cena</th>
                <th>Estado</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody

              @foreach($periodos as $periodo)
              <tr>
                <td>{{$periodo->id_periodoatencion}}</td>
                <td>{{$periodo->fecha_inicio->formatLocalized('%A %d %B %Y') }}</td>
                <td>{{$periodo->fecha_fin->formatLocalized('%A %d %B %Y') }}</td>
                <td>{{$periodo->precio_desayuno}}</td>
                <td>{{$periodo->precio_almuerzo}}</td>
                <td>{{$periodo->precio_cena}}</td>
                <td>
                    @if ($periodo->estado=='1')
                        <button class=" btn btn-link  badge badge-secondary" title="Cambiar estado a: En venta  " onclick="cambiarEstado('{{$periodo->id_periodoatencion}}','{{$periodo->estado}}')"  data-toggle='modal' data-target='#modalCambio'>Inactivo</button>
                    @elseif($periodo->estado=='2')
                        <button class=" btn btn-link badge badge-info" title="Cambiar estado a: En atención " onclick="cambiarEstado('{{$periodo->id_periodoatencion}}','{{$periodo->estado}}')"  data-toggle='modal' data-target='#modalCambio'>En venta</button>
                    @elseif($periodo->estado=='3')
                        <button class=" btn btn-link badge badge-success" title="Cambiar estado a: Cerrado " onclick="cambiarEstado('{{$periodo->id_periodoatencion}}','{{$periodo->estado}}')"  data-toggle='modal' data-target='#modalCambio'>En atención</button>
                    @elseif($periodo->estado=='4')
                        <button class=" btn btn-link badge badge-primary" >Cerrado</button>
                    @endif

                </td>
                <td>

                  <a href="#" OnClick="editarPeriodo('{{$periodo->id_periodoatencion}}','{{$periodo->fecha_inicio->toDateString()}}','{{$periodo->fecha_fin->toDateString()}}','{{$periodo->precio_desayuno}}','{{$periodo->precio_almuerzo}}','{{$periodo->precio_cena}}','{{$periodo->estado}}');"  class='' title="Editar" data-toggle='modal' data-target='#modalCrear'><i class='fa fa-edit'></i> Editar
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

      </div>
      <div class="row ">
        <div class="col-md-6">
          <div class="pull-left">
             Mostrando del {{ $periodos->firstItem()}} al  {{ $periodos->lastItem()}} de  {{ $periodos->total()}} registros
          </div>

        </div>
        <div class="col-md-6">
          <div class="">
             {{$periodos->appends(Request::only(['parametro']))->render()}}
          </div>
        </div>

      </div>
      <div class="row mt-2">
        <div class="col-md-6">
          <div class="mr-0">
             <label>Estados:</label>
             <button class=" btn btn-xs badge badge-secondary"  title="Inactivo" data-toggle="popover" data-trigger="hover"  data-content="Se puede editar los datos del periodo">Inactivo</button>
             <button class=" btn btn-xs badge badge-info" title="En venta" data-toggle="popover" data-trigger="hover" data-content="Las ventas seran enlazadas al periodo actual">En venta</button>
             <button class=" btn btn-xs badge badge-success" title="En atención" data-toggle="popover" data-trigger="hover" data-content="Los registros de atencion seran enlazadas al este periodo">En atención</button>
             <button class=" btn btn-xs badge badge-primary" title="Cerrado" data-toggle="popover" data-trigger="hover" data-content="El periodo ha concluido y no se puede realizar ningun cambio">Cerrado</button>
          </div>
        </div>
      </div>
      @else
          <div class="alert alert-info">
            <strong>No se ha seleccionado ningún semestre</strong> No se ha encontrado ningún semestre, debe crear un nuevo semestre
          </div>
      @endif
      </div>
    </div>
  </div>
</div>
    <div class="modal fade" id="modalCambio" data-controls-modal="modalCambio" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
      <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">  Cambiar estado</h5>
                <button type="button" class="close" data-dismiss="modal"  aria-label="close">
                  <span aria-hidden="true">x</span>
                </button>

            </div>

              @if (@$semestre->estado == '1')
                <form role="form" method="POST" action="{{url('periodoAtencion/cambiarEstado')}}" >
                  {{csrf_field()}}
                  <div class="modal-body">
                      <input type="hidden" name="id_periodo" value="" id="id_periodo">
                      <div class="row ">
                         <div class="col-md-12">
                            <div id="mensajeCambio"></div>
                        </div>
                      </div>
                   </div>
                    <div class="modal-footer">
                        <button  class="btn btn-default btn-sm" data-dismiss="modal" >Cancelar</button>
                        <button type="submit" class="btn btn-success btn-sm" id="btnGuardar"> <i class=""></i> Aceptar</button>
                    </div>

              </form>
              @else
                 <div class="alert alert-info">
                    <strong>
                      <i class="fa fa-info-circle fa-lg"></i>
                       El semestre no se encuentra Activo
                    </strong>
                    <p>No se puede realizar ningún cambio</p>
                  </div>
              @endif

              <div id="mensajes" class="m-t-5">
              </div>
            </div>

        </div>
    </div><!-- /.modal -->
     <div class="modal fade" id="modalCrear" data-controls-modal="modalCrear" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title"> <i class="icon-people icons "></i> Registro de periodo de atencion</h5>
                <button type="button" class="close" data-dismiss="modal"  aria-label="close"><span aria-hidden="true">x</span></button>

            </div>

              @if (@$semestre->estado == '1')
                <form role="form" class="formulario"  method="POST" action=""  id="formulario">
                <div class="modal-body">
                    <div class="row">
                      <input type="hidden" name="id_escuela" id="idEscuela">
                      <input type="hidden" name="ruta" id="ruta">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-2" class="">Fecha Inicio</label>
                                 <input type="date" class="form-control " required="required" id="fecha_inicio"  name="fecha_inicio" placeholder="Ingrese fecha de inicio">
                                 <em id="fecha_inicio-error" class="error invalid-feedback"></em>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-1" class="">Fecha fin</label>
                                <input type="date" class="form-control "  required="required" id="fecha_fin"  name="fecha_fin" placeholder=" Ingrese fecha de fin ">
                                <em id="fecha_fin-error" class="error invalid-feedback"></em>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-1" class="">Semestre</label>
                                <input type="text" class="form-control " id="semestre"  name="semestre" disabled="disabled" value="{{Session::get('nombre_semestre')}}">
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-1" class="">Precio desayuno</label>
                                <input type="number" placeholder="Ejm: 10.00"  name="precio_desayuno" id="precio_desayuno" min="0" value="" step="0.10" title="precio desayuno" pattern="^\d+(?:\.\d{1,2})?$" class="form-control form-control-sm">
                                <em id="precio_desayuno-error" class="error invalid-feedback"></em>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-2" class="">Precio almuerzo</label>
                                <input type="number" placeholder="Ejm: 10.00"  name="precio_almuerzo" id="precio_almuerzo" min="0" value="" step="0.10" title="precio almuerzo" pattern="^\d+(?:\.\d{1,2})?$" class="form-control form-control-sm">
                                 <em id="precio_almuerzo_-error" class="error invalid-feedback"></em>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-3" class="">Precio cena</label>
                               <input type="number" placeholder="Ejm: 10.00"  name="precio_cena" id="precio_cena" min="0" value="" step="0.10" title="precio cena" pattern="^\d+(?:\.\d{1,2})?$" class="form-control form-control-sm">
                                <em id="precio_cena-error" class="error invalid-feedback"></em>
                            </div>
                        </div>
                    </div>

                 </div>
                <div class="modal-footer">
                     <button  class="btn btn-default btn-sm" data-dismiss="modal" >Cancelar</button>
                        <button type="submit" class="btn btn-primary btn-sm" id="btnGuardar"> <i class=""></i> Guardar</button>

                </div>
              </form>
              @else
                 <div class="alert alert-info">
                    <strong>
                      <i class="fa fa-info-circle fa-lg"></i>
                       El semestre no se encuentra Activo
                    </strong>
                    <p>No se puede realizar ningún cambio</p>
                  </div>
              @endif

              <div id="mensajes" class="m-t-5">
              </div>
            </div>

        </div>
    </div><!-- /.modal -->
</div>
@endsection

@section('scripts')

  <script type="text/javascript">
    function recargarPagina(){
      window.location.href='{{ url()->full() }}';

      }

 function editarPeriodo(id_periodoatencion,fecha_inicio,fecha_fin,precio_desayuno,precio_almuerzo,precio_cena,estado){
   $("#fecha_inicio").val(fecha_inicio);
   $("#fecha_fin").val(fecha_fin);
   $("#precio_desayuno").val(precio_desayuno);
   $("#precio_almuerzo").val(precio_almuerzo);
   $("#precio_cena").val(precio_cena);
   $('#estado').val(estado);//asiganr un valor
   $('#ruta').val('{{ url('')}}/periodoAtencion/'+id_periodoatencion+'/actualizar');
   rmMsjValidacion();

 }
 function cambiarEstado(id_periodo,estado){
    var mensaje='';
    var textoEstado='';
    if (estado==1) {
        textoEstado='Inactivo';
        mensaje='El periodo seleccionado cambiara de estado ' +textoEstado +' a <strong> En venta </strong><p> Lo cual implica que se procedera a realizar las ventas con los datos especificados  </p>';
    }
    if (estado==2) {
       textoEstado='En venta';
        mensaje='El periodo seleccionado cambiara de estado ' +textoEstado +' a <strong> En atencion </strong><p> Lo cual implica que el proceso de venta ya culmino, y se procedera a la atención de comensales con los datos recolectados en el proceso de Venta</p> ';

    }
    if (estado==3) {
        textoEstado='En atención';
        mensaje='El periodo seleccionado cambiara de estado ' +textoEstado +' a <strong> Cerrado </strong><p> Lo cual implica que el proceso de atención ya culminó. y no se podra agregar mas datos.</p> ';
    }
    $('#mensajeCambio').html(mensaje);
    $('#id_periodo').val(id_periodo);
 }

 $("#btnAbrirModal").click(function(){// cuando click en el boton modal linpie todo lo que este llenado
    $("#fecha_inicio").val('');
   $("#fecha_fin").val('');
   $("#costo_desayuno").val('');
   $("#costo_almuerzo").val('');
   $("#costo_cena").val('');
   $('#estado').val('Inactivo');//asiganr un valor
    rmMsjValidacion();
    $('#ruta').val("{{ url('periodoAtencion/insertar') }}");
 });

  $(document).on("submit",".formulario", function(e){


    cargadorBoton(true);

    rmMsjValidacion();
    e.preventDefault();

    // var datos= new FormData($("#formEscuela")[0]);//esto es para capturar todos los input del formulario
      var datos= $('#formulario').serialize();
      var route=$("#ruta").val();
    $.ajax({
      url:route,
      headers:{'X-CSRF-TOKEN': "{{csrf_token()}}" },// envio del token
      type:'POST',
      datatype: 'json',
      data: datos,
      timeout:15000, //limite de tiempo 15 segundos
      success: function(respuesta){
        if (respuesta.parametro=='edicion') {
          msgSuccess(respuesta.mensaje);
          recargarPagina();
          $('#btnGuardar').attr("disabled", false);

        }
        else{
            $("#fecha_inicio").val('');
             $("#fecha_fin").val('');
             $("#precio_desayuno").val('');
             $("#precio_almuerzo").val('');
             $("#precio_cena").val('');
             $('#estado').val('Inactivo');//asiganr un valor

            msgSuccess(respuesta.mensaje);
            cargadorBoton(false);
            recargarPagina();
        }

      },
       error: function(xhr, textStatus, thrownError){
        if(xhr.status=422)
            addMsgValidation(xhr);
        else
            messagesxhr(xhr,textStatus); 
          
            cargadorBoton(false);
       }

    });
  });

  function cargadorBoton(estado)
  {
    if (estado==false) {
        $('#btnGuardar').children('i').removeClass("fa fa-spinner fa-spin");
        $('#btnGuardar').attr("disabled", false);
    }
    else{
        $('#btnGuardar').children('i').addClass("fa fa-spinner fa-spin");
        $('#btnGuardar').attr("disabled", true);
    }
  }

  function addMsgValidation(xhr)
  {
      let messages = xhr.responseJSON;
      
      msgError('Se encontraron errores, revice el formulario');
  
      $.each(messages, function (ind, elem) 
      {  
          $('#'+ind).addClass('is-invalid');
          $('#'+ind+'-error').text(elem);
      });
  }

function rmMsjValidacion()
{
    $('input').removeClass('is-invalid');
    $('select').removeClass('is-invalid');
    $('em').text('');
}


</script>
@parent

@endsection
