@extends('layouts.coreui')
  @section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Roles </li>
         
@endsection
 @section('content')
 
<div class="row">
  <div class="col-md-12 ">
    <div class="card">
      <div class="card-header"> <i class="fa fa-list">
        </i> Roles de usuario 
      </div>
      <div class="card-body">
      
        <div class="row">
          <div class="col-md-6">
            <div class="pull-left">
              <a href="{{url('rol/registrar')}}" class="btn btn-primary btn-sm  ">
                <span class="fa fa-plus"></span> Nuevo rol
              </a>   
              <a href="{{url('/rol')}}" class="btn btn-light btn-sm">
                <span class="fa fa-refresh"></span> Refrescar página
              </a>
             </div> 
           </div>
          <div class="col-md-6">
            <form class="navbar-form " method="GET" action="{{url('/rol')}}" >
                <div class="input-group mb-3">
                  <input type="text" class="form-control form-control-sm" placeholder="Nombre de rol" name="parametro" value="{{@$parametro}}">
                  <div class="input-group-append">
                    <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button> 
                  </div>
                </div>     
           </form>
          </div>  
        </div><!--row-->   
         
        <div class="table-responsive ">
        <table class="table table-hover table-striped table-sm">
          <thead>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Opciones</th>
          </thead>
          <tbody>
            @foreach ($roles as $rol)
              <tr>
                <td>{{$rol->nombre}}</td>
                <td>{{$rol->desripcion}}</td>
                <td>
                 
                  <a href="{{url('rol/'.$rol->id_rol.'/editar')}}"  title="Editar Rol"><i class="fa fa-edit fa-lg  " ></i></a>
                  <a href="#"  title="Eliminar Rol"><i class="fa fa-trash fa-lg text-danger " ></i></a>
                </td>
              </tr>
            @endforeach
          </tbody>
          
        </table>
          
       
      </div>
      <div class="row ">
        <div class="col-md-6">
          <div class="pull-left">
             Mostrando del {{ $roles->firstItem()}} al  {{ $roles->lastItem()}} de  {{ $roles->total()}} registros 
          </div>
         
        </div>
        <div class="col-md-6">
          <div class="">
             {{$roles->appends(Request::only(['parametro']))->render()}}   
          </div>
        </div>
        
      </div>
     
    
      </div>
    </div>
  </div>
</div>      
    
</div>
@endsection

@section('scripts')
  
  <script type="text/javascript">
    function recargarPagina(){
      window.location.href='{{ url()->full() }}';
      
      }

</script>
@parent

@endsection 