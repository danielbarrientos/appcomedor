@extends('layouts.coreui')
  @section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('/rol')}}">Rol</a>
    </li>
    <li class="breadcrumb-item active">Registrar/editar Rol </li>

         
@endsection
 @section('content')
 
<div class="row">
  <div class="col-md-8 offset-md-2 ">
    <div class="card">
      <div class="card-header"> <i class="fa fa-list">
        </i> Rol de usuario
      </div>
      <div class="card-body">
         <form method="POST" action="{{@$ruta}}">
           {{csrf_field()}}
          <input type="hidden" name="id_rol" value="{{old('id_rol',@$rol->id_nombre)}}">
           <div class="form-group">
             <label>Nombre</label>
             <input type="text" name="nombre" class="form-control" value="{{old('nombre',@$rol->nombre)}}"  placeholder="Ingrese un nombre para el Rol de usuario" required="required" maxlength="30">
             <em class="text-danger">{{ @$errors->first('nombre') }}</em>
           </div>
             <label>Descripción (opcional)</label>
             <textarea placeholder="Ingrese una breve descripción para el rol de usuario" name="descripcion" class="form-control"  rows="3" cols="40" maxlength="100">{{old('descripcion',@$descripcion)}}</textarea>
             <em class="text-danger">{{ @$errors->first('descripcion') }}</em>
          
        
          <hr>
          <div class="row">
            <div class="col-md-12">
              <h4 class="text-dark">Lista de permisos</h4>
              <div class="form-group">
                <ul class="list-unstyled">
                    @foreach ($permisos as $permiso)
                      <li><label><input type="checkbox" name="permisos[]" value="{{$permiso->id_permiso}}"  {{ (is_array(old('permisos',@$permisoAsignado)) and in_array($permiso->id_permiso, old('permisos',@$permisoAsignado))) ? 'checked' : '' }} > {{$permiso->nombre}} <em class="text-muted"> ({{$permiso->descripcion}})</em></label></li>
                    @endforeach     
                </ul>
              </div>
            </div>
          </div><!--row--> 
          <div class="modal-footer">
              <a href="{{url('/rol')}}" class="btn btn-sm btn-secondary"> Volver</a>
              <button type="submit" class="btn btn-primary btn-sm">Guardar</button>

          </div>
           </form> 
        </div>
         
      </div>
    </div>
  </div>
</div>      
    
</div>
@endsection

@section('scripts')
  
  <script type="text/javascript">
    function recargarPagina(){
      window.location.href='{{ url()->full() }}';
      
      }

</script>
@parent

@endsection 