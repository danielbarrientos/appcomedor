<div  class="horizontal preview hoja_report">
        <br>
        @php
            
        @endphp
        <div class="text-center"> <span id="business_description"></span> <b> OFICINA DE BIENESTAR UNIVERSITARIO</b> <br>( REPORTE DE ESTUDIANTES POR EDAD) </div>
        <div class="row mb-md">
            <div class="col-sm-6 text-uppercase text-left">
                      
            </div>
            <div class="col-sm-6 text-right">FECHA - HORA IMPRESION : <span id="datetime">{{date('d/m/Y H:i:s')}}</span></div>
        </div>
        <div class="row mt-md">
            <!-- start : tables -->
            <div class="col-sm-12">
                @php
                    $edad_total =0;
                    $cantidad_total =0;
                @endphp
                <!-- start : table item -->
              
                <table id="tblItems">
                    <thead>
                        <tr class="bg-vgray">
                          
                            <th class="text-center" style="width: 35%;">ESCUELA</th>
                            <th class="text-center">SEXO</th>
                            <th class="text-center">EDAD PROMEDIO</th>
                        </tr>
                    </thead>
                    @foreach ($query as $fila)
                        @php
                            $cantidad_grupo = 0;
                            $edad_grupo = 0; 
                        @endphp
                    <tbody class="tbody_template" >    
                        @foreach ($fila['groupeddata'] as $key => $item)
                            @php
                                $contador  = count($fila['groupeddata']);
                                $edad_grupo += $item['promedio'];
                                $cantidad_grupo++;
                            @endphp
                            @if ($key == 0)
                                <tr>
                                    <td rowspan="{{$contador+1}}" class="bg-gray text-weight-bold text-left">{{$fila['grupo']}}</td>
                                    <td>{{$item['subgrupo']}}</td>
                                    <td class="text-right">{{$item['promedio']}}</td>
                                   
                                </tr>
                            @else
                                <tr>
                                    <td>{{$item['subgrupo']}}</td>
                                    <td class="text-right">{{$item['promedio']}}</td>
                                    
                                </tr>
                            @endif
                           
                        @endforeach
                        <tr class="bg-gray">
                            <td class="text-right">EDAD PROMEDIO</td>
                            <td class="text-right"> {{$edad_grupo/$cantidad_grupo}}</td>
                          
                        </tr>      
                    </tbody>
                    @php
                       
                        $edad_total += $edad_grupo; 
                        $cantidad_total += $cantidad_grupo;
                    @endphp
                    @endforeach
                    <tbody>
                        <tr class="bg-gray">
                            <td  class="text-right" colspan="2">EDAD PROMEDIO GENERAL</td>
                            <td class="text-right" >{{ round($edad_total/$cantidad_total)}}</td>
                        </tr>
                    </tbody>
                 </table>
            </div>
        </div>
    </div>