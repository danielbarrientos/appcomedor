<form action="{{url('ficha/reporteProcedencia')}}" method="GET" id="formProcedencia">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label for="email">Estudiantes:</label>
                <select class="form-control form-control-sm"  style="width: 100%;" name="estado_matricula" id="estado_matricula" required>
                    <option value="0"> -- Todos -- </option>
                    <option value="1">Solo matriculados</option>
                </select>
                <em id="departamento_procedencia-error" class="error invalid-feedback"></em>
            </div>
        </div>
        <div class="col-md-4">
            
            <div class="form-group">
                <label for="email">Departamento:</label>
                <select class="form-control form-control-sm"  style="width: 100%;" name="id_departamento_procedencia" id="id_departamento_procedencia" required>
                    <option value="0"> -- Todos -- </option>
                    @foreach ($departamentos as $departamento)
                        <option value="{{$departamento->id_departamento}}">{{$departamento->nombre}}</option>
                    @endforeach
                </select>
                <em id="departamento_procedencia-error" class="error invalid-feedback"></em>
                
            </div>
        </div>
        
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-sm mt-4"> Generar reporte</button>
        </div>   
    </div>
    <div  class="dialog_reporte text-sm" title="Reporte de ventas"  >
        <div class="card">
            <div class="card-body" >
                <div class="row">
                    <div class="col-sm">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills" role="tablist" style="" id="ul_nav_procedencia">
                            <li class="nav-item">
                                <a id="link_table" class="nav-link active "  href="#tab_tabla_procedencia" data-toggle="tab" role="tab" aria-selected="true">TABLA</a>
                            </li>
                            
                            <li class="nav-item">
                                <a id="link_graphic" class="nav-link" href="#tab_grafico_procedencia" data-toggle="tab" role="tab" aria-selected="false">GRÁFICO</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" >
                            
                            <div class="tab-pane fade  show active content-hoja" id="tab_tabla_procedencia">
                                <div class="horizontal preview hoja_report">
                                </div>
                            </div>
                            <div class="tab-pane fade content-hoja" id="tab_grafico_procedencia">
                                <div class="horizontal preview hoja_report">
                                    
                                    <div class="row mb-md">
                                        <div class="col-md-12">
                                            <div id="grafico_bar_procedencia" style="min-width: 310px; height: 400px; margin: 1 auto">
                                            
                                            </div>
                                            <div id="grafico_pie_procedencia" style="min-width: 310px; height: 400px; margin: 1 auto">
                                            
                                            </div>
                                        </div>  
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer " >
                <center>
                        <button  type="button" class="btn btn-primary btn-sm btnImprimir" id="btnImprimirSeccionProcedencia">
                            IMPRIMIR
                        </button>
                </center>  
            </div>
        </div>
        </div>
</form>