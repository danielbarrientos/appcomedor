<form action="{{url('ficha/reporteEdadPromedio')}}" method="GET" id="formEdad">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="email">Estudiantes:</label>
                <select class="form-control form-control-sm"  style="width: 100%;" name="estado_matricula" id="estado_matricula" required>
                    <option value="0"> -- Todos -- </option>
                    <option value="1">Solo matriculados</option>
                </select>
                <em id="departamento_procedencia-error" class="error invalid-feedback"></em>
            </div>
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-sm mt-4"> Generar reporte</button>
        </div>   
    </div>
    <div  class="dialog_reporte text-sm"   >
        <div class="card">
            <div class="card-body" >
                <div class="row">
                    <div class="col-sm">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills" role="tablist" style="" id="ul_nav_edad">
                            <li class="nav-item">
                                <a id="link_table" class="nav-link active "  href="#tab_tabla_edad" data-toggle="tab" role="tab" aria-selected="true">TABLA</a>
                            </li>
                            
                            <li class="nav-item">
                                <a id="link_graphic" class="nav-link" href="#tab_grafico_edad" data-toggle="tab" role="tab" aria-selected="false">GRÁFICO</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" >
                            
                            <div class="tab-pane fade  show active content-hoja" id="tab_tabla_edad">
                                <div class="horizontal preview hoja_report">
                                </div>
                            </div>
                            <div class="tab-pane fade content-hoja" id="tab_grafico_edad">
                                <div class="horizontal preview hoja_report">
                                    
                                    <div class="row mb-md">
                                        <div class="col-md-12">
                                            <div id="grafico_bar_edad"style="min-width: 310px; height: 400px; margin: 0 auto">
                                            
                                            </div>
                                            <div id="grafico_pie_edad" style="min-width: 310px; height: 400px; margin: 1 auto">
                                            
                                            </div>
                                        </div>  
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer " >
                <center>
                        <button type="button" class="btn btn-primary btn-sm " id="btnImprimirSeccionEdad"  >
                            IMPRIMIR
                        </button>
                </center>  
            </div>
        </div>
        </div>
</form>