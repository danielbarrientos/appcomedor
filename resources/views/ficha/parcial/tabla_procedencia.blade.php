<div  class="horizontal preview hoja_report">
        <br>
        @php
            
        @endphp
        <div class="text-center"> <span id="business_description"></span> <b> OFICINA DE BIENESTAR UNIVERSITARIO</b> <br>( REPORTE DE ESTUDIANTES POR LUGAR DE PROCEDENCIA) <br> Departamento: {{@$departamento->nombre}}</div>
        <div class="row mb-md">
            <div class="col-sm-6 text-uppercase text-left">
                      
            </div>
            <div class="col-sm-6 text-right">FECHA - HORA IMPRESION : <span id="datetime">{{date('d/m/Y H:i:s')}}</span></div>
        </div>
        <div class="row mt-md">
            <!-- start : tables -->
            <div class="col-sm-12">
                @php
                    $cantidad_total =0;
                @endphp
                <!-- start : table item -->
              
                <table id="tblItems">
                    <thead>
                        <tr class="bg-vgray">
                            @if ($id_departamento =='0')
                            <th class="text-center">DEPARTAMENTO</th>
                            <th class="text-center">PROVINCIA</th>
                            @else
                            <th class="text-center">PROVINCIA</th>
                            <th class="text-center">DISTRITO</th>
                            @endif
                            <th class="text-center">CANTIDAD</th>
                        </tr>
                    </thead>
                    @foreach ($query as $fila)
                        @php
                            $cantidad = 0;
                        @endphp
                    <tbody class="tbody_template" >    
                        @foreach ($fila['groupeddata'] as $key => $item)
                            @php
                                $contador = count($fila['groupeddata']);
                                $cantidad += $item['cantidad'];
                            @endphp
                            @if ($key == 0)
                                <tr>
                                    <td rowspan="{{$contador+1}}" class="bg-gray text-weight-bold text-center">{{$fila['grupo']}}</td>
                                    <td>{{$item['subgrupo']}}</td>
                                    <td class="text-right">{{$item['cantidad']}}</td>
                                   
                                </tr>
                            @else
                                <tr>
                                    <td>{{$item['subgrupo']}}</td>
                                    <td class="text-right">{{$item['cantidad']}}</td>
                                    
                                </tr>
                            @endif
                           
                        @endforeach
                        <tr class="bg-gray">
                            <td class="text-right">TOTAL</td>
                            <td class="text-right"> {{$cantidad}}</td>
                          
                        </tr>      
                    </tbody>
                    @php
                       
                        $cantidad_total += $cantidad; 
                    @endphp
                    @endforeach
                    <tbody>
                        <tr class="bg-gray">
                            <td  class="text-right" colspan="2">TOTAL ESTUDIANTES</td>
                            <td class="text-right" >{{$cantidad_total}}</td>
                        </tr>
                    </tbody>
                 </table>
            </div>
        </div>
    </div>