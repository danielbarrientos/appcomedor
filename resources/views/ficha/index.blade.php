@extends('layouts.coreui')
  @section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Ficha Socioeconomica</li>
         
@endsection
 @section('content')

 <div class="row">
	<div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-bar-chart"></i>   Reportes
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" id="rpt_edad">
                                <a class="nav-link active" data-toggle="tab" href="#tab_edad" role="tab" aria-controls="tab_edad">
                                    <i class="icon-pie-chart"></i> Edad  
                                    <span class="badge badge-success">New</span>
                                </a>
                            </li>
                           
                            <li class="nav-item" id="rpt_modalidad">
                                    <a class="nav-link" data-toggle="tab" href="#tab_modalidad" role="tab" aria-controls="tab_modalidad">
                                        <i class="icon-pie-chart"></i> Modalidad ingreso
                                    <span class="badge badge-pill badge-danger">29</span>
                                    </a>
                                </li>
                            <li class="nav-item" id="rpt_procedencia">
                                <a class="nav-link" data-toggle="tab" href="#tab_procedencia" role="tab" aria-controls="tab_procedencia">
                                <i class="icon-pie-chart"></i> Lugar procedencia</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_edad" role="tabpanel">
                                @include('ficha.parcial.tab_edad')
                            </div>
                            <div class="tab-pane" id="tab_sexo" role="tabpanel">
                            </div>
                            <div class="tab-pane" id="tab_modalidad" role="tabpanel">
                                @include('ficha.parcial.tab_modalidad_ingreso')
                            </div>
                            <div class="tab-pane" id="tab_procedencia" role="tabpanel">
                                @include('ficha.parcial.tab_procedencia') 
                            </div>
                        </div><!-- tab-content-->
                    </div>
                </div>
            </div>      
        </div>
	</div>			
						
</div>
@endsection

@section('scripts')
<script src="{{asset('plugins/select2/dist/js/select2.min.js')}}"></script>
<script src="{{asset('plugins/highcharts/code/highcharts.js')}}"></script>
<script src="{{asset('plugins/highcharts/code/modules/data.js')}}"></script>
<script src="{{asset('plugins/highcharts/code/modules/drilldown.js')}}"></script>
<script src="{{asset('plugins/highcharts/code/modules/exporting.js')}}"></script>
<script src="{{asset('plugins/highcharts/code/modules/export-data.js')}}"></script>

<script src="{{asset('js/ficha/index_jquery.js')}}"></script>
<script src="{{asset('js/ficha/index_jscript.js')}}"></script>
@parent

@endsection 