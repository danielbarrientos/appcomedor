<table class="table  table-sm table-hover" id="table-sortable">
    <thead>
        <tr style="cursor: pointer;">
            <th onclick="sortTable(0, 'int')">Nº</th>
            <th onclick="sortTable(1, 'str')">Dni</th>
            <th onclick="sortTable(2, 'str')">Código</th>
            <th onclick="sortTable(3, 'str')">Apellidos y Nombres</th>
            <th >Tipo </th>
            <th onclick="sortTable(5, 'str')">Estado</th>
            <th  onclick="sortTable(6, 'int')">Creditos</th>
            <th  onclick="sortTable(7, 'int')">Promedio</th>   
            <th onclick="sortTable(8, 'str')">Historial/Ficha</th>
            <th>Acciones</th>

        </tr>
    </thead>
    <tbody id="lista_beneficiarios">
        @php
            $i=1;
        @endphp
        @foreach($beneficiarios as $beneficiario)
            <tr id="{{$beneficiario->id_beneficiario}}">
                <td>{{$i++}}</td>
                <td>{{$beneficiario->dni_estudiante}}</td>
                <td>{{$beneficiario->codigo}}</td>

                <td class="nombre_completo">{{$beneficiario->apellidos}} {{$beneficiario->nombres}}</td>
                <td>
                    <select  id="select_{{$beneficiario->dni_estudiante}}">
                        @foreach ($tipos as $tipo)
                            <option value="{{$tipo->id_tipo}}" {{($beneficiario->tipo == $tipo->descripcion)?'selected':''}} >{{$tipo->descripcion}}</option>
                        @endforeach
                    </select>
                  
                </td>
                <td> 
                    @if ($beneficiario->estado== 1)
                        <span class="badge badge-success">Activo</span>
                    @elseif($beneficiario->estado == 0)
                        <span class="badge badge-warning">Suspendido</span>
                    @endif

                </td>
                @php
                    $datos = obtenerPromedio($beneficiario->dni_estudiante);
                @endphp
                <td>{{$datos['creditos']}}</td>
                <td>{{$datos['promedio']}}</td>
                <td>
                    <a href="{{url('estudiante/'.$beneficiario->codigo.'/historial')}}" class=" btn btn-xs btn-outline-success" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"><i class="fa fa-address-book-o "></i> Historial</a>
                    @if (existeFicha($beneficiario->dni_estudiante))
                    <a href="{{url('estudiante/'.$beneficiario->dni_estudiante.'/ficha')}}" class=" btn btn-xs btn-outline-danger" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"><i class="fa fa-file-pdf-o "></i> Ficha</a>
                    @endif
                </td>
               
                <td>
                    <button class="btn btn-danger btn-xs eliminar" ><i class='fa fa-trash-o  '></i> Eliminar</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="float-left">
    Mostrando {{ $beneficiarios->firstItem()}} a  {{ $beneficiarios->lastItem()}} de  {{ $beneficiarios->total()}} entradas 
</div>
<div class="float-right">
    {{$beneficiarios->appends(Request::only(['dni','codigo','apellidos','nombres']))->render()}}   
</div>
