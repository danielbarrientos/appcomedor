@extends('layouts.coreui')
@section('breadcrumb')

<li class="breadcrumb-item">
  <a href="{{url('panel')}}">Inicio</a>
</li>
<li class="breadcrumb-item">
        <a href="{{url('/beneficiario')}}">Beneficiarios</a>
      </li>
<li class="breadcrumb-item active">{{$escuela->nombre}} </li>
@endsection
@section('content')


<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <span class="header-title "> <i class="icon-people icons fa-lg"></i> Beneficiarios   ({{ Session::get('nombre_semestre')}})</span>
            <div class="card-header-actions">
                <select id="ver_escuela" class="form-control-sm" >
                    @foreach ($escuelas as $item)
                        <option value="{{$item->id_escuela}}" {{($item->id_escuela == $escuela->id_escuela)?'selected':''}}>{{$item->nombre}}</option>
                    @endforeach
                </select>
            </div> 
        </div> 
        <div class="card-body">
                <div class="row mt-1 ">
                        <div class="col-md-6">
                            <div class="pull-left">
                                
                                <div class="">
                                    <a href="#" class="btn btn-primary btn-sm  " id="btnAbrirModal" ><span class="fa fa-plus"></span> Nuevo registro</a>
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-list"></i> Ver lista
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            <a class="dropdown-item" href="{{url('beneficiario/escuela/'.$escuela->id_escuela.'/listaPdf')}}" class=" btn btn-xs btn-outline-success" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"> Lista de beneficiarios</a>
                                            <a class="dropdown-item" href="{{url('beneficiario/escuela/'.$escuela->id_escuela.'/listaVentaPdf')}}" class=" btn btn-xs btn-outline-success" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"> Lista de venta</a>
                                        </div>
                                    </div>
                                    <button   type="button" class="btn btn-light btn-sm btn-refresh"><span class="fa fa-refresh"></span> Refrescar página</button>
                                   
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <form class="navbar-form " method="GET" action="{{url('/beneficiario/escuela/'.$escuela->id_escuela)}}" id="frmBuscar" >
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <select name="campo_busqueda" id="campo_busqueda" class="form-control form-control-sm" >
                                            <option value="apellidos">Apellidos</option>
                                            <option value="nombres">Nombres</option>
                                            <option value="dni">Dni</option>
                                            <option value="codigo">Código</option>
                                        </select>
                                    </div>
                                    <input type="search" class="form-control form-control-sm" placeholder="" name="apellidos" id="parametro">
                                    <div class="input-group-append" id="button-addon4">
                                       
                                        <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button> 
                                    </div>
                                </div>
                                    
                            </form>
                        </div>
                            
                    </div><!--row-->
                  <div class="table-responsive " id="contenedor_lista">
            
                  </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="modalEliminar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <h5 class="modal-title"> <i class="icon-people icons "></i> Eliminar beneficiario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">x</span></button>

        </div>
        <form role="form"  method="POST" action="{{ url('beneficiario/eliminar') }}" id="formEliminar">
          <div class="modal-body">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-sm-12">
                <input type="hidden" name="id_beneficiario" id="id_beneficiario">

                <label class="control-label">¿Esta seguro de eliminar de la lista de beneficiarios a: <b id="nombreBeneficiario"> </b> ?</label>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-danger btn-sm" id="btnEliminar"> <i class=" fa fa-trash-o fa-lg"></i> Eliminar</button>
          </div>
        </form>
        <div id="mensajes" class="m-t-5"></div>
      </div>
    </div>
  </div><!-- /.modal -->

    <div class="modal fade" id="modalRegistro"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title"> <i class="icon-people icons "></i> Registro de Beneficiarios</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">x</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10">
                                <form class="navbar-form " method="GET" action="{{url('/beneficiario/estudiante/buscar')}}" id="frmBusquedaIndividual" >
                                    <div class="input-group mb-3">
                                        
                                        <input type="number" class="form-control form-control-sm" placeholder="Ingrese codigo" name="codigo" id="buscar_codigo" required>
                                        <div class="input-group-append" id="button-addon4">
                                        
                                            <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button> 
                                        </div>
                                    </div>
                                        <input type="hidden" name="id_escuela" id="id_escuela" value="{{$escuela->id_escuela}}">
                                </form>
                        </div>
                    </div>
                    
                    <form class="formulario" id="frmRegistro" action="{{url('/beneficiario/insertar')}}">
                
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Dni</label>
                                <input type="text" class="form-control form-control-sm" value="" readonly  name="dni" id="dni" required>
                                
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class=" control-label ">Apellidos y Nombres </label>
                                <input type="text" class="form-control form-control-sm" value="" disabled="disabled" name="nombre_completo" id="nombre_completo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Escuela</label>
                                <input type="text" class="form-control form-control-sm" value="" disabled="disabled" name="nombre_escuela" id="nombre_escuela">
                            </div>
                        </div>
                    </div>
                    <div class="row" id="contenedor_documentos">
                        <div class="col-md-12">
                            <a href="" class=" btn btn-xs btn-outline-success" target="popup" id="link_historial" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"><i class="fa fa-address-book-o " ></i> Historial</a>
                        
                            <a href="" class=" btn btn-xs btn-outline-danger" target="popup" id="link_ficha" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"><i class="fa fa-file-pdf-o " ></i> Ficha</a>
                        </div>
                    </div>
                    <!--div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label "> Observaciones </label>
                                <textarea class="form-control form-control-sm" name="observacion" rows="3" id="observacion" placeholder="Ingrese Observaciones y/o coemntarios solo si es necesario....."></textarea>
                            </div>
                        </div>
                    </div-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary btn-sm" id="btnGuardar" disabled><i></i> Registrar</button>
                </div>
                </form>
            </div>
        </div>
    </div><!-- /.modal -->
</div>

@endsection

@section('scripts')
<script>
    var id_escuela_actual = "{{$escuela->id_escuela}}";
</script>
<script src="{{asset('js/beneficiario/index.js?345')}}"></script>

<script type="text/javascript">
 
</script>
@parent

@endsection
