@extends('layouts.coreui')
@section('breadcrumb')

<li class="breadcrumb-item">
  <a href="{{url('panel')}}">Inicio</a>
</li>
<li class="breadcrumb-item active">Beneficiarios</li>
@endsection
@section('content')


<div class="row">
  
    @foreach ($escuelas as $escuela)
        <div class="col-md-4">
            <div class="card">
                <div class="card-body  pb-0">
                    <div class="row">
                        <div class="col-sm-4">
                                <img src="{{asset('archivos/escuelas/'.$escuela->siglas.'.png')}}" alt="" style="width: 50px; height: 50px;">
                        </div>
                        <div class="col-sm-8" style="text-align: right;">
                            <div class="text-muted small text-uppercase font-weight-bold "> {{$escuela->nombre}}</div>
                        </div>
                    </div>
                    <div class="text-center">
                        <div class="text-value">{{$escuela->total_regular+$escuela->total_becaa+$escuela->total_becab+$escuela->total_becac+$escuela->total_adicional}}</div>
                        <small class="text-muted text-uppercase font-weight-bold">Estudiantes</small>
                    </div>
                    <table class="table table-sm table-bordered ">
                        <tr>
                            <td class="text-muted text-uppercase font-weight-bold text-center"><small>Regular</small></td>
                            <td class="text-muted text-uppercase font-weight-bold text-center"><small>Beca A</small></td>
                            <td class="text-muted text-uppercase font-weight-bold text-center"><small>Beca B</small></td>
                            <td class="text-muted text-uppercase font-weight-bold text-center"><small>Beca C</small></td>
                            <td class="text-muted text-uppercase font-weight-bold text-center"><small>Adicional</small></td>
                        </tr>
                        <tr>
                            <td class="text-muted text-center">{{$escuela->total_regular}}</td>
                            <td class="text-muted text-center">{{$escuela->total_becaa}}</td>
                            <td class="text-muted text-center">{{$escuela->total_becab }}</td>
                            <td class="text-muted text-center">{{$escuela->total_becac}}</td>
                            <td class="text-muted text-center">{{$escuela->total_adicional}}</td>
                        </tr>  
                    </table>
                </div>
                <div class="card-footer px-3 py-2">
                    <a href="{{url('beneficiario/escuela/'.$escuela->id_escuela)}}" class="btn-block text-muted d-flex justify-content-between align-items-center">
                       <span class="small font-weight-bold">Ver lista de beneficiarios</span> 
                       <i class="fa fa-angle-right " ></i>
                    </a>
                </div>
            </div>
        </div>
    @endforeach
  
</div>
@endsection

@section('scripts')

<script type="text/javascript">
  function recargarPagina() 
  {
    window.location.href = '{{ url()->full() }}';
  }

</script>
@parent

@endsection
