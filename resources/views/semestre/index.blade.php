@extends('layouts.coreui')
	@section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Semestre</li>
  	@endsection
 @section('content')
 

    
 <div class="row">
	<div class="col-lg-12 ">
		<div class="card-box">
			<h4 class=" header-title">Semestres</h4>
			
                

				<div class="row mt-4">
            <div class="col-md-6">
             
              <div class="pull-left">
                   
                   <div class="">
                      <a href='javascript:void(0);' onclick="window.open('{{ url('/semestre/registrar') }}', '_blank', 'width=500,height=400,scrollbars=yes,status=yes,resizable=yes,screenx=200,screeny=0');" class="btn btn-primary btn-sm">Registrar</a>	
                     <a href="{{url('/semestre')}}" class="btn btn-light btn-sm"><span class="fa fa-refresh"></span> Refrescar página</a>
                   
                   
                  </div>
               </div> 
             </div>
          <div class="col-md-6">
            <form class="navbar-form " method="GET" action="{{url('/semestre')}}" >
              {{-- csrf_field() --}}
                
                  
                  <div class="input-group mb-3">
                    <input type="text" class="form-control form-control-sm" placeholder="Descripcion" name="descripcion" value="{{@$descripcion}}">
                    <div class="input-group-append">
                      <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button> 
                    </div>
                  </div>     
             </form>
             
          </div>
          
        </div><!--row-->        
			<div class="table-responsive " >
				
				<table class="table table-striped table-hover table-sm">
					<thead>
						<tr>
							<th>ID</th>
							<th>Descripción</th>
							<th>Estado</th>	          					          				
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody >
						
						@foreach($semestres as $semestre)
						<tr>
						<td>{{$semestre->id_semestre}}</td>
						<td>{{$semestre->descripcion}}</td>
							
						<td> {!! ($semestre->estado) ? "<span class='label label-primary'> Abierto <span>" : "<span class='label label-success'> Cerrado<span>"!!} </td>
						<td>
							
							 <a href='javascript:void(0);' onclick="window.open('{{ url('/semestre/'.$semestre->id_semestre.'/editar') }}', '_blank', 'width=500,height=400,scrollbars=yes,status=yes,resizable=yes,screenx=200,screeny=0');" class="btn btn-default btn-xs"> <span class="fa fa-edit"></span> Editar</a>	
						</td>
						</tr>
						@endforeach
					</tbody>
						{{$semestres->appends(Request::only(['descripcion']))->render()}}
				</table>
			</div>	
		</div>
	</div>			
							
</div>


@endsection

@section('scripts')
	<script type="text/javascript">
		function recargarPagina(){
    	window.location.href='{{ url()->full() }}';
    	
    	}
	</script>
@parent

@endsection 