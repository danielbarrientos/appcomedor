<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
     
    <title>COMEDOR UNIVERSITARIO</title> 
  
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link href="{{asset('css/app.css?nocache=')}}" rel='stylesheet' type='text/css' >
	<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
	<link href="{{asset('css/core.css?nocache=')}}" rel='stylesheet' type='text/css' >
	<link href="{{asset('css/toastr.min.css')}}" rel='stylesheet' type='text/css' >
    <link href="{{asset('css/custom.css')}}" rel='stylesheet' type='text/css' >
    <link href="{{asset('css/preloader.css')}}" rel='stylesheet' type='text/css' >
	<link rel="stylesheet" href="{{asset('plugins/select2/dist/css/select2.min.css')}}" type='text/css' >
    @section('style')
    @show
	   
    
<style type="text/css">
	/*body { padding-top: 70px; }*/

</style>
  </head>
  <body>
        <div id="contenedor_carga">
            <div id="carga">
            </div>
        </div> 
  	<nav class="navbar navbar-default ">
  	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="{{ url('/') }}">
	       <img class="navbar-brand-full" src="{{asset('img/brand/logo_comedor.png')}}" width="110" height="28" alt="CoreUI Logo">
       </a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li ><a href="{{ url('/') }}">  Inicio </a></li>
			
			@if (Session::has('dni_estudiante'))
	        <li ><a href="{{ url('/fichaEstudiante') }}"> Ficha socioeconomica </a></li>
	         
			@endif
	      </ul>
	     @if (Session::has('dni_estudiante'))
	     
	      <ul class="nav navbar-nav navbar-right">

	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user fa-lg"></span> {{ Session::get('nombre') }}<!--span class="caret"--></span></a>
	          
            </li>
            <li ><a href="{{ url('/cerrarSesion') }}">  Salir <i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></a></li>
	      </ul>
		@endif

	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	 <div id="page-wrapper">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-lg-12">
	                
					@yield('content')
	            </div>
	            <!-- /.col-lg-12 -->
	        </div>
	        <!-- /.row -->
	    </div>
	    <!-- /.container-fluid -->
	</div> <!-- jQuery -->
    	
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>
      
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('js/toastr.min.js')}}"></script>
	<script src="{{asset('js/parcial/funciones_globales.js')}}"></script>
	<script>
            var csrf_token = "{{csrf_token()}}";
            var base_url   = "{{url('')}}";
    </script>
    @include('parcial.mensajeGeneral')
    @section('scripts')
	@show
		
		<script type="text/javascript">
            window.onload=function()
            {
		        var contenedor=document.getElementById('contenedor_carga');
		        contenedor.style.visibility='hidden';
		        contenedor.style.opacity='0';	
	        }
		</script>
</body>
</html>