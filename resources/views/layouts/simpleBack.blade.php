<!DOCTYPE html>
<html>
<head>
	 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Comedor universitario UNAMBA</title>

    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed ">
	<header class="app-header navbar">
     
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{asset('img/brand/logo_comedor.png')}}" width="105" height="30" alt="CoreUI Logo">
       
       
      </a>
    </header>
	
    <div class="app-body">
	 <main class="main">
        <!-- Breadcrumb-->
        
        
        <!-- Breadcrumb Menu-->
        <div class="container-fluid ">
         
             @yield('content')
        </div>
      </main>

	<script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/parcial/funciones_globales.js')}}"></script>
    <script>
        var csrf_token = "{{csrf_token()}}";
        var base_url   = "{{url('')}}";
        var url_current   = "{{url()->current()}}";
    </script>
    @include('parcial.mensajeGeneral')
    
	@section('scripts')
    @show
</body>
</html>