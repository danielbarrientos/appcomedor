@extends('layouts.coreui')
  @section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Estudiantes</li>
         
@endsection
 @section('content')

 <div class="row">
	<div class="col-md-12 ">
		<div class="card-box">
			<h4 class="header-title "> <i class="icon-people icons fa-lg"></i> Estudiantes</h4>
            <div class="row mt-4 ">
                <div class="col-md-6">
                    <div class="pull-left">
                        
                        <div class="">
                            <a href="#" class="btn btn-primary btn-sm  " id="btnAbrirModal" ><span class="fa fa-plus"></span> Nuevo registro</a>
                            <a href="{{url('/estudiante')}}" class="btn btn-light btn-sm"><span class="fa fa-refresh"></span> Refrescar página</a>
                        </div>
                    </div> 
                </div>
				<div class="col-md-6">
					<form class="navbar-form " method="GET" action="{{url('/estudiante')}}" id="frmBuscar" >
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <select name="campo_busqueda" id="campo_busqueda" class="form-control form-control-sm" >
                                    <option value="apellidos">Apellidos</option>
                                    <option value="nombres">Nombres</option>
                                    <option value="dni">Dni</option>
                                    <option value="codigo">Código</option>
                                </select>
                            </div>
                            <input type="search" class="form-control form-control-sm" placeholder="" name="apellidos" id="parametro">
                            <div class="input-group-append" id="button-addon4">
                               
                                <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button> 
                            </div>
                        </div>
                       	 
					</form>
				</div>
					
            </div><!--row-->
            
         
			<div class="table-responsive" id="contenedor_lista">
                    
			</div>
		</div>
	</div>			
							

	 <div class="modal fade" id="modalRegistro"  data-controls-modal="modalRegistro"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
            <div class="modal-header"> 
                <h5 class="modal-title"> <i class="icon-people icons "></i> Registro de estudiante</h5> 
                <button type="button" class="close" data-dismiss="modal"  aria-label="close"><span aria-hidden="true">x</span></button> 
            </div> 
               <div id='cargador'></div>
                <form role="form"  method="POST" action=""  id="frmRegistro"> 
                  <div class="modal-body"> 
                      <div class="row"> 
                        <input type="hidden" name="id_escuela" id="idEscuela">
                        <input type="hidden" name="ruta" id="ruta">
                          <div class="col-md-4"> 
                              <div class="form-group"> 
                                  <label for="field-2" class="">Dni</label> 
                                   <input type="text" class="form-control " id="dni" onkeypress="return pulsar(event)" name="dni" placeholder="Ingrese Dni">
                                   <em id="dni-error" class="error invalid-feedback"></em>  
                              </div> 
                          </div>
                          <div class="col-md-4"> 
                              <div class="form-group"> 
                                  <label for="field-1" class="">Código universitario</label> 
                                  <input type="text" class="form-control " onkeypress="return pulsar(event)" id="codigo"  name="codigo" placeholder=" Ingrese Código universitario  ">
                                  <em id="codigo-error" class="error invalid-feedback"></em>  
                              </div> 
                          </div>
                           <div class="col-md-4"> 
                              <div class="form-group"> 
                                  <label for="field-1" class="">Código RFID</label> 
                                  <input type="text" class="form-control " id="codigo_rfid" onkeypress="return pulsar(event)"  name="codigo_rfid" placeholder=" Ingrese Código RFID  ">
                                  <em id="codigo_rfid-error" class="error invalid-feedback"></em>  
                              </div> 
                          </div> 
                           
                      </div>
                      <div class="row"> 
                       
                          <div class="col-md-6"> 
                              <div class="form-group"> 
                                  <label for="field-1" class="">Apellidos</label> 
                                  <input type="text" class="form-control " id="apellidos"  name="apellidos" placeholder="Ingrese Apellidos">
                                  <em id="apellidos-error" class="error invalid-feedback"></em>  
                              </div> 
                          </div> 
                          <div class="col-md-6"> 
                              <div class="form-group"> 
                                  <label for="field-2" class="">Nombre</label> 
                                   <input type="text" class="form-control " id="nombres"  name="nombres" placeholder=" Ingrese Nombres">
                                   <em id="nombres-error" class="error invalid-feedback"></em>  
                              </div> 
                          </div> 
                      </div> 
                      <div class="row ">
                         <div class="col-md-6">
                           <div class="form-group">
                              
                              <label for="field-1" class="">Escuela académico profesional</label>  
          					    <select name="escuela" class="form-control  form-control-sm" id="escuela">
                                    <option value="0" selected>Seleccione</option>
                                    @foreach ($escuelas as $escuela)
                                    <option value="{{$escuela->id_escuela}}">{{$escuela->nombre}}</option>
								    @endforeach  
                                </select>
                              <em id="escuela-error" class="error invalid-feedback"></em> 	 
                          </div>
                             
                        </div>
                         <div class="col-md-6">
                           <div class="form-group">
                              
                              <label for="field-1" class="">Matriculado</label>  
                              @php
                                $matriculados=['0'=>'No','1'=>'Si'];
                              @endphp
                              {!!Form::select('matricula', $matriculados,null,['class'=>'form-control form-control-sm ','id'=>'matricula'])!!}
                                <em id="matricula-error" class="error invalid-feedback"></em>        
    
                          </div>
                             
                        </div>
                      </div>  
                   </div> 
                    <div class="modal-footer"> 
                        <button  class="btn btn-default btn-sm" data-dismiss="modal"  id="cerrarModalRegistro">Cancelar</button> 
                        <button type="submit" class="btn btn-primary btn-sm" id="btnGuardar"> <i class=""></i> Guardar</button>  
                    </div> 
                </form>
				 <div id="mensajes" class="m-t-5"></div> 
            </div>
        </div>
  </div><!-- /.modal -->
</div>
@endsection

@section('scripts')
<script src="{{asset('js/estudiante/index.js')}}"></script>

<script>
    function pulsar(e) 
    { 
        tecla = (document.all) ? e.keyCode :e.which; 
        
        return (tecla!=13); 
    }
</script> 
@parent

@endsection 