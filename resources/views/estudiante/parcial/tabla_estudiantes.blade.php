<table class="table  table-sm table-hover table-border" >
    <thead style="cursor:pointer">
        <tr>
            <th>Dni</th>
            <th>Código</th>
            <th>Apellidos y Nombres</th>          					          				
            <th>Escuela</th>
            <th>Matriculado</th>
            <th>Historial/Ficha</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody id="lista_estudiantes" >
        @php
        $i=1;
        @endphp
        @foreach($estudiantes as $estudiante)
        <tr>
            <td>{{$estudiante->dni}}</td>
            <td>{{$estudiante->codigo}}</td>
            <td>{{$estudiante->apellidos}}  {{$estudiante->nombres}}</td>
            <td>{{$estudiante->escuela->siglas}}</td>
            <td> {!! $variable = ($estudiante->matricula) ? "<span class='badge badge-success'> Si <span>" : "<span class='badge badge-secondary'> No <span>"!!}</td>
            <td>
                <a href="{{url('estudiante/'.$estudiante->codigo.'/historial')}}" class=" btn btn-xs btn-outline-success" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"><i class="fa fa-address-book-o "></i> Historial</a>
                @if (existeFicha($estudiante->dni))
                <a href="{{url('estudiante/'.$estudiante->dni.'/ficha')}}" class=" btn btn-xs btn-outline-danger" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=600,left=300pt,top=20pt'); return false;"><i class="fa fa-file-pdf-o "></i> Ficha</a>
                @endif
            </td>
            <td>	
                <a href="#" id="{{$estudiante->dni}}" class=" btn btn-xs btn-primary btn-square editar" ><i class='fa fa-edit'></i> Editar</a>
               
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div>
<div class="float-left">
    Mostrando {{ $estudiantes->firstItem()}} a  {{ $estudiantes->lastItem()}} de  {{ $estudiantes->total()}} entradas 
</div>
<div class="float-right">
    {{$estudiantes->appends(Request::only(['dni','codigo','apellidos','nombres']))->render()}}   
</div>
</div>