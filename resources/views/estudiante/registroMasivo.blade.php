@extends('layouts.coreui')
  @section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Registro masivo </li>
    @endsection
 @section('content')
 <div class="row">
  <div class="col-lg-12 ">
    <div class="card">
      <div class=" card-header"><i class="fa fa-list"></i> Registro masivo de estudiantes</div>      
            <div class="card-body">
              <form method="POST" action="{{url('estudiante/registroMasivo')}}" enctype="multipart/form-data">
                {{csrf_field()}}
              <div class="row mb-2">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Escuela</label>
                
                    <select class="form-control " name="escuela" id="escuela" required="required">
                      <option value="0">Todas</option>   
                      @foreach ($escuelas as $escuela)
                        <option value="{{$escuela->id_escuela}}" {{ (old('escuela',@$id_escuela) == $escuela->id_escuela) ? 'selected':'' }}>{{$escuela->nombre}}</option>  
                      @endforeach
                    </select>
                    <em class="text-danger">{{ @$errors->first('escuela') }}</em>
                  </div>  
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Matriculados</label>
                    <select class="form-control " name="matricula"  required="required">

                        <option value="si" {{ (old('matricula',@$matricula) == 'si') ? 'selected':'' }}>Si</option>
                        <option value="no" {{ (old('matricula',@$matricula) == 'si') ? 'selected':'' }}>No</option> 
                  
                    </select>
                    <em class="text-danger">{{ @$errors->first('matricula') }}</em>
                
                  </div>  
                </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Archivo</label>
                      <input type="file" name="archivo" class="form-control-file border">
                        <em class="text-danger">{{ @$errors->first('archivo') }}</em>  
                    </div>        
                  </div>
                  <div class="col-md-1">
                        <button class="btn btn-primary btn-square mt-4"><i class="fa fa-cloud-upload fa-lg "></i> Subir archivo</button>         
                  </div>  
              </div><!--row--> 
            </form>
          
          </div>    
    </div>
  </div>      
              
</div>


@endsection

@section('scripts')
  <script type="text/javascript">
    function recargarPagina(){
      window.location.href='{{ url()->full() }}';
      
      }
   
  </script>
@parent

@endsection 