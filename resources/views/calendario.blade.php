
<html>
<link href="{{asset('css/style.css')}}" >

<br>

<table class=" table table-sm table-hover table-bordered">
	<thead >
		<tr>
			<th>#</th> 
			<th>DNI</th> 
			<th>APELLIDOS Y NOMBRES</th>
			<th>TIPO VENTA</th>
			@foreach ($fechas as $fecha)
				<th width="5" class="m-0 p-0" ><p  class="texto-vertical-1 m-0 p-1 ">{{date("d/m", strtotime($fecha->dia))}}</p></th> 
			@endforeach
			<th></th>
			<th></th>
			<th>#</th> 
			<th>DNI</th> 
			<th>APELLIDOS Y NOMBRES</th>
			<th>TIPO VENTA</th>
			@foreach ($fechas as $fecha)
				<th width="5" class="m-0 p-0" ><p  class="texto-vertical-1 m-0 p-1 ">{{date("d/m", strtotime($fecha->dia))}}</p></th> 
			@endforeach
		</tr>
	</thead>
	<tbody>
			@php
				$i=1 ;
			@endphp
			@foreach ($comensales as $comensal)

			<tr>
				<td>{{$i++}}</td>
				<td >{{$comensal['dni_estudiante']}}</td>
				<td>{{$comensal['apellidos']}} {{$comensal['nombres']}}</td>
				<td> {{($comensal['tipo_venta']==1)? 'Regular':'Libre' }} </td>
				@foreach ($fechas as $fecha)
						
					<td >{{$comensal[$fecha->dia]}}</td> 
				@endforeach
				<td></td>
				<td></td>
			</tr>	
			@endforeach
			

			
		
	</tbody>
</table>

</html>