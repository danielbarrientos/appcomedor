  <div class="">
   
<div class="zona_impresion" id="imprimeme">
        <!-- codigo imprimir -->

<table  align="center" width="185px" style="font-size: 9px; font-style: normal;  ">
  <tr>
      <td align="center">
        .::<strong> COMEDOR UNIVERSITARIO</strong>::.<br>
         <strong> UNAMBA</strong><br>
        Periodo de atención <br>
        {{$periodo->fecha_inicio->format('d/m/Y')}} Al  {{$periodo->fecha_fin->format('d/m/Y')}}
    </td>
  </tr>
   
   
    <tr>
        <td><strong>Estudiante:</strong> {{ @$estudiante->apellidos}} {{@$estudiante->nombres}}</td>
    </tr>
   
      
</table>

<table border="0" align="center" width="185px" style="font-size: 9px; font-style: normal;">

    <tr>
      <td width="10">Nº </td>
      <td width="40" >DESCRIP.</td>
       <td> FECHA </td>
      <td align="right">IMPORTE</td>
    </tr>
    <tr>
      <td colspan="4">=============================== </td>
    </tr>
    @php
      $total=0.0;
    @endphp
     @foreach ($cuposVendidos as $cupo)
          @php
            $total=$total+$cupo->importe;
          @endphp
            <tr>
              <td>{{$cupo->id_venta}}</td>
              <td> {{$cupo->servicio->descripcion}}</td>
              <td align="right"  >{{$cupo->fecha_registro->format('d/m/y H:i:s')}}</td>
             
              <td align="right">{{$cupo->importe}}</td>
            </tr>

    @endforeach
   
   
    
  
    <tr>
    <td>&nbsp;</td>
    <td align="right"><b>TOTAL:</b></td>
    <td align="right" style="font-size: 10"><b>S/. {{number_format($total, 2, '.', '')}}</b></td>
    </tr>
     <tr>
      <td colspan="3"><strong>Cajero:</strong> {{Session::get('nombre')}}</td>
    </tr>      
   
     <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    
    
</table>

</div>
  </div>