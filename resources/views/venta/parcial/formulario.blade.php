
                     <form  class=" formulario form-horizontal " method="POST" action="{{ url($ruta) }}" id="formulario">
                  {{ csrf_field() }}

                      <div class="row">

                         <div class="col-md-5">

                          <div class="total_venta card text-center" >

                             <div id="cantidad" >S/. 0.00</div>
                              <h5>Total</h5>

                          </div>

                         </div>
                          <div class="col-md-3">
                              <input type="hidden" name="dni_estudiante" value="{{@$estudiante->dni}}">
                              <input type="hidden" name="id_escuela" value="{{@$escuela->id_escuela}}">

                                @if (in_array('1',$serviciosPermitidos))
                                 <div class="custom-control custom-checkbox">
                                  <input type="checkbox"   name="servicio[]" value="1" class=" micheckbox custom-control-input" id="desayuno">
                                  <label class="custom-control-label" for="desayuno">Desayuno</label>
                                </div>
                                @endif
                                @if (in_array('2',$serviciosPermitidos) )
                                  <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="servicio[]"   value="2" class="micheckbox custom-control-input" id="almuerzo">
                                    <label class="custom-control-label" for="almuerzo">Almuerzo</label>
                                  </div>
                                @endif
                                @if (in_array('3',$serviciosPermitidos) )
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" name="servicio[]"  value="3" class=" micheckbox custom-control-input" id="cena">
                                  <label class="custom-control-label" for="cena">Cena</label>
                                </div>
                                @endif
                         </div>
                         <div class="col-md-4">

                            <button  class="btn btn-primary btn-sm btn-block" id="btnGuardar ">Guardar</button>
                            <a href="{{route('ventaRegular')}}" class="btn btn-secondary btn-sm btn-block">Cancelar</a>
                         </div>
                      </div>

                </form>
