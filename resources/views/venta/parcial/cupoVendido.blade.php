<div class="row mt-2">
  <div class="col-md-12">
     <h4 class="m-t-0 header-title">Cupos adquiridos  <button  onclick="imprimir()"  class=" btn btn-xs btn-light float-right"> <i class="fa fa-print fa-lg"></i> Imprimir comprobante</button></h4>
       @if ($cuposVendidos)

       <div class="table-responsive">
          <table class="table table-sm" style="font-size: 12px">
          
          <thead>
         
            <tr>
              <th>Nº venta</th>
              <th>Servicio</th>
              <th>Fecha</th>
              <th>Tipo venta</th>
              <th>Importe</th>  
            </tr>
          </thead>
           <tbody>
          @foreach ($cuposVendidos as $cupo)
            <tr>
              <td>{{$cupo->id_venta}}</td>
              <td>{{$cupo->servicio->descripcion}}</td>
              <td>{{$cupo->fecha_registro->format('d/m/Y H:i:s')}}</td>
              <td>{{($cupo->tipo_venta) ? 'Regular' : 'Libre' }}</td>
              <td>{{$cupo->importe}}</td>
            </tr>
          @endforeach
        
        </tbody>
        </table>
       </div>
      @endif
  </div>  
</div>