<br>
<style type="text/css">

.cell {
    background-color: #346AA6;
    color: #FFFFFF;
    font-size: 11;

}
th{
	font-size: 11;
}
.titulo{
	text-align: center;
}

tr td {
    background-color: #ffffff;
    font-size: 10;
}
tr > td {
    border: 1px solid #000000;

}
.num {
  mso-number-format:General;
}
.text{
  mso-number-format:"\@";/*force text*/
}
</style>
<tr ><th class="titulo" colspan="5" >RESUMEN DE VENTAS  SEMESTRE: {{Session::get('nombre_semestre')}}</th></tr>

<br>
{{-- @foreach ($datos as $index => $dato) --}}

	<table >


		<thead  >
			<tr >
				<th class="titulo"  colspan="5"> PERIODO DE ATENCIÓN Nº {{$periodo->fecha_inicio->format('d/m/Y').' Al '.$periodo->fecha_fin->format('d/m/Y')}} 	</th>
			</tr>

			<tr class="cell">
				<th>#</th>
				<th>ESCUELA</th>
				<th>DESAYUNO</th>
				<th>ALMUERZO</th>
        <th>CENA</th>
        <th>TOTAL</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($datos as $key => $escuela)
      {{  $total_fila=$escuela['total_desayuno']+$escuela['total_almuerzo']+$escuela['total_cena']}}
				<tr>
				  <td>{{$key}}</td>
          <td>{{$escuela['nombre']}}</td>
          <td > {{$escuela['total_desayuno']}}</td>
          <td > {{$escuela['total_almuerzo']}}</td>
          <td > {{$escuela['total_cena']}}</td>
          <td > {{$total_fila}}</td>
				</tr>
			@endforeach

		</tbody>
	</table>

{{-- @endforeach --}}
