<div class="row ">
    <div class="col-md-4">
     <img src="{{asset('archivos/fotos/'.$estudiante->nombre_foto)}}" style="height: 180px;" class="img-thumbnail">
   </div> 
   <div class="col-md-8">
       <table class=" table-sm table" style="font-size: 12px">
        <tbody>
          <tr>
            <td width="100px;"><strong>Dni:</strong></td>
            <td>{{ @$estudiante->dni}}</td>
          </tr>
           <tr>
            <td><strong>Código:</strong> </td>
            <td>{{ @$estudiante->codigo}}</td>
          </tr>
           <tr>
            <td><strong>Apellidos y Nombres:</strong></td>
            <td>{{ @$estudiante->apellidos}} {{@$estudiante->nombres}}</td>
          </tr>
          
           <tr>
            <td><strong>EAP:</strong></td>
            <td>{{@$estudiante->escuela->nombre}}</td>
          </tr>
           @if (@$tipoBeneficiario)
           <tr>
            <td><strong>Tipo beneficiario:</strong></td>
            <td>
             
                @if ($tipoBeneficiario=='R')
                   Regular
                @elseif($tipoBeneficiario=='A')
                    <span class="badge badge-success">Beca integral - 100% de descuento</span>  
                                   
                @elseif($tipoBeneficiario=='B')
                    <span class="badge badge-success">Beca parcial - 70% de descuento</span> 
                     
                @elseif($tipoBeneficiario=='C')
                    <span class="badge badge-success">Semi Beca - 50% de descuento</span> 
                    
                @endif 
             
            </td>
          </tr>
           @endif
          @if (@$escuela && count($serviciosPermitidos)>0)
             <tr>
              <td><strong>Cupo de:</strong></td>
              <td>{{$escuela->nombre}}</td>
            </tr> 
          @endif
         
        </tbody>
        <tfoot></tfoot>
      </table>
   </div>
   
  </div>