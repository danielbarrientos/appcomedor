@extends('layouts.simpleBack')
  @section('breadcrumb')

    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>

    <li class="breadcrumb-item active">Venta de cupos</li>
  @endsection
 @section('content')
  <div class="container">
  <div class="row mt-2">
  <div class="col-sm-12">
     <div class="card-box">
      <h4 class=" header-title text-center text-primary"><i class="icon-people icons fa-lg"></i> VENTA DE CUPOS DE ATENCIÓN</h4>
    </div> 
  </div>  
   
  </div>
  <div class="row ">
    <div class="col-md-7">
      <div class="card">

        <div class=" card-header mb-2">
          <i class="fa fa-list"> </i> Venta de cupos de atención ({{ Session::get('nombre_semestre') }})
        </div>

       <div class="card-body">
          @if (@ !is_null($periodo))

          <form class="" action="{{url('/ventaCupo/registrarVentaLibre')}}" >
             <div class="row mb-2   ">
              <label class="col-sm-2 form-control-label">Cupo de:</label>
            <div class="col-sm-4">
             <select name="id_escuela" class="form-control form-control-sm" id="id_escuela">
               @for ($i = 0; $i < count($escuelas) ; $i++)
                <option value="{{$i}}">{{$escuelas[$i]}}</option>
               @endfor
             </select>
            </div>
           <div class="col-sm-6 ">
              <div class="input-group float-right ">
             <input type="number" name="parametro"  class="form-control form-control-sm solo-numero " value="" placeholder="Dni, Cód. universitario o Cód. rfid  " maxlength="11" minlength="6" required="required" autofocus="autofocus">
              <div class="input-group-append">
                <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button>
              </div>
            </div>
            </div>

             </div><!--row-->
          </form>
        <div class="row">
          <div class="col-sm-12">
             <ul class="nav nav-pills nav-justified">
                <li class="nav-item">
                  <a class="nav-link  " href="{{route('ventaRegular')}}">Venta Regular</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="{{route('ventaLibre')}}">Venta Libre</a>
                </li>
              </ul>
            @if (@$estudiante)
             <div class="card ">

              <div class="card-body">
                 @include('venta.parcial.datosEstudiante')
                  @if (count($serviciosPermitidos)>0)
                    @include('venta.parcial.formulario')
                  @endif
                  @if (count($cuposVendidos)>0)
                    @include('venta.parcial.cupoVendido')
                  @endif
              </div>
             </div>
            @endif
            <div class="mt-1">
           @if (Session::has('msj-warning'))

              <div class="alert alert-warning" role="alert">
                <h4 class="alert-heading"> Mensaje</h4>
                <p> {{Session::get('msj-warning')}}</p>
              </div>
            @endif
            @if (Session::has('msj-info'))
              <div class="alert alert-info" role="alert">
                <h4 class="alert-heading"> Mensaje</h4>
                <p> {{Session::get('msj-info')}}</p>
              </div>
            @endif
            @if (Session::has('msj-success'))
              <div class="alert alert-success" role="alert">
                <h4 class="alert-heading"> Mensaje</h4>
                <p> {{Session::get('msj-success')}}</p>
              </div>
            @endif
             </div>
         </div>
        </div>
          @else
            <div class="alert alert-info"><i class="fa fa-info-circle"> </i> No hay ningun periodo en proceso de Venta de cupos</div>
        @endif
        </div>
      </div><!--car box-->
    </div><!--col-->
    <div class="col-md-5 ">
      @if (@ !is_null($periodo))
         @include('venta.parcial.resumenVenta')
      @endif
    </div>
  </div>
  <div class="modal fade" id="modalCrearEscuela" data-controls-modal="modalCrearEscuela" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          @if (@$estudiante)
             @if (count($cuposVendidos)>0)
           @include('venta.parcial.ticket')
          @endif
          @endif

        </div>
      </div>
  </div><!-- /.modal -->
  </div>
@endsection

@section('scripts')

  <script type="text/javascript">
    var total=0.00;
    var precio_desayuno={{(empty($precioServicio[1])) ? "0.00" : $precioServicio[1] }};
    var precio_almuerzo={{(empty($precioServicio[2])) ? "0.00" : $precioServicio[2] }};
    var precio_cena={{(empty($precioServicio[3])) ? "0.00" : $precioServicio[3] }};


    function recargarPagina(){
      window.location.href='{{ url()->full() }}';

      }
  $(".micheckbox").on( 'change', function() {
    if( $(this).is(':checked') ) {
        // Hacer algo si el checkbox ha sido seleccionado

       if ($(this).val()=='1') {
        total=total+ precio_desayuno;
       }
       if ($(this).val()=='2') {
        total=total + precio_almuerzo;
       }
        if ($(this).val()=='3') {
        total=total + precio_cena;
       }

      $('#cantidad').text('S/. '+parseFloat(total).toFixed(2));


    } else {

       if ($(this).val()=='1') {
        total=total - precio_desayuno;

       }
       if ($(this).val()=='2') {
        total=total - precio_almuerzo;
       }
        if ($(this).val()=='3') {
        total=total - precio_cena;
       }
        $('#cantidad').text('S/. '+parseFloat(total).toFixed(2));

    }
});

  $('#formulario').submit(function(e){

    if (checkboxSeleccionados() === 0) {
        e.preventDefault();
        alert('Debe seleccionar al menos un servicio (desayuno,almuerzo o cena)');
    }
});
 function checkboxSeleccionados() {
    contador=0;
      $('.micheckbox:checked').each(
      function() {
          contador++;
      }
    );
      return contador;
 }
 function imprimir(){
  var objeto=document.getElementById('imprimeme');  //obtenemos el objeto a imprimir
  var ventana=window.open('','_blank');  //abrimos una ventana vacía nueva
  ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
  ventana.document.close();  //cerramos el documento
  ventana.print();  //imprimimos la ventana
  ventana.close();  //cerramos la ventana
}
 window.onload=function(){
      document.onkeydown= teclas;
    }
    function teclas(event){
      var codigo=event.keyCode;
      console.log(codigo);

      if (codigo==80) {
        imprimir();
      }
     }
  $(document).ready(function (){
      $('.solo-numero').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
      });
    });

</script>
@parent

@endsection
