@extends('layouts.coreui')
	@section('breadcrumb')

    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Reporte de ventas</li>
  	@endsection
 @section('content')
 <div class="row">
	<div class="col-lg-7 ">
		<div class="card">
			<div class=" card-header"><i class="fa fa-list"></i> Reporte de ventas por escuela</div>
	      	<div class="card-body">
		      		<form method="GET" action="{{url('ventaCupo/rptListaVentaPdf')}}" target="_blank">
		      			
		        		<div class="row mb-2">
		    				<div class="col-md-6">
		 					<div class="form-group">
		 						<label>Escuela</label>

		 						<select class="form-control form-control-sm " name="escuela" id="escuela" required="required">
		 							<option value="">Seleccione</option>
		 							@foreach ($escuelas as $escuela)
		 								<option value="{{$escuela->id_escuela}}" {{ (old('escuela',@$id_escuela) == $escuela->id_escuela) ? 'selected':'' }}>{{$escuela->nombre}}</option>
		 							@endforeach
		 						</select>
		 						<em class="text-danger">{{ @$errors->first('escuela') }}</em>
		 					</div>
		             	</div>
		             	<div class="col-md-6">
		 					<div class="form-group">
		 						<label>Periodo</label>
		 						<select class="form-control form-control-sm " name="periodo" id="periodo" required="required">
		 							<option value="">Seleccione</option>
		 							@foreach ($periodos as $periodo)

		 								<option value="{{$periodo->id_periodoatencion}}" {{ (old('periodo',@$id_periodo) == $periodo->id_periodoatencion) ? 'selected':'' }}>{{$periodo->fecha_inicio->formatLocalized('%A %d %B %Y')}} AL {{$periodo->fecha_fin->formatLocalized('%A %d %B %Y')}}</option>
		 							@endforeach
		 						</select>
		 						<em class="text-danger">{{ @$errors->first('periodo') }}</em>

		 					</div>
		             	</div>

		    			</div><!--row-->
		    			<button  class="btn btn-light btn-sm  "><i class="fa fa-file-pdf-o fa-lg text-danger"></i>  Exportar PDF</button>
		 					<!--button  class="btn btn-light btn-sm " target="_blank" formaction="{{url('ventaCupo/rptListaVentaExcel')}}"><i class="fa fa-file-excel-o fa-lg text-success "></i>  Exportar Excel</button-->
		  			</form>
	      	</div>
		</div>
	</div>
	<div class="col-lg-5">
		<div class="card">
			<div class=" card-header"><i class="fa fa-list"></i> Resumen de ventas</div>
        <div class="card-body">
          <form method="GET" action="{{url('ventaCupo/resumenVentaExcel')}}" >
	 					<div class="form-group">
	 						<label>Periodo</label>
	 						<select class="form-control form-control-sm " name="periodo" id="periodo" required="required">
	 							<option value="">Seleccione</option>
	 							@foreach ($periodos as $periodo)

	 								<option value="{{$periodo->id_periodoatencion}}" {{ (old('periodo',@$id_periodo) == $periodo->id_periodoatencion) ? 'selected':'' }}>{{$periodo->fecha_inicio->formatLocalized('%A %d %B %Y')}} AL {{$periodo->fecha_fin->formatLocalized('%A %d %B %Y')}}</option>
	 							@endforeach
	 						</select>
	 						<em class="text-danger">{{ @$errors->first('periodo') }}</em>
	 					</div>
			        <button  class="btn btn-light btn-sm  " target="_blank" formaction="{{url('ventaCupo/resumenVentaPdf')}}"><i class="fa fa-file-pdf-o fa-lg text-danger"></i>  Exportar PDF</button>
			 				<button  class="btn btn-light btn-sm " ><i class="fa fa-file-excel-o fa-lg text-success "></i>  Exportar Excel</button>
          </form>
        </div>
    </div>
  </div>

</div>

</div>


@endsection

@section('scripts')
	<script type="text/javascript">
		function recargarPagina(){
    	window.location.href='{{ url()->full() }}';

    	}




	</script>
@parent

@endsection
