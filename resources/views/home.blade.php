@extends('layouts.coreui')
@section('breadcrumb')
    <li class="breadcrumb-item">Home</li>
      <li class="breadcrumb-item">
        <a href="#">Admin</a>
      </li>
      <li class="breadcrumb-item active">Dashboard</li>
      <!-- Breadcrumb Menu-->
      
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8 offset-2 ">
            <div class="card  ">
                <div class="card-header ">Dashboard</div>

                <div class="card-body">
                    Bienvenido!
                </div>
            </div>
        </div>
    </div>

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    Bienvenido!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
