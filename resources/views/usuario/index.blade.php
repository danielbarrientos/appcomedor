@extends('layouts.coreui')
  @section('breadcrumb')
   
    <li class="breadcrumb-item">
        <a href="{{url('panel')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">Usuarios</li>
@endsection

 @section('content')
 

<div class="container-fluid">
    
 <div class="row">
	<div class="col-lg-12 ">
		<div class="card-box">
			<h4 class=" header-title"><i class="icon-people icons fa-lg"></i> Usuarios</h4>
		
          <div class="row mt-4">
            <div class="col-md-8">
             
              <div class="pull-left">
                   
              @if (permiso('usuario.registrar'))
                    <a href="#" class="btn btn-primary btn-sm  " id="btnAbrirModal" data-toggle="modal" data-target="#modalCrear"><span class="fa fa-plus"></span> Nuevo registro</a>
              @endif     
                 

                 <a href="{{url('/usuario')}}" class="btn btn-light btn-sm"><span class="fa fa-refresh"></span> Refrescar página</a>
 
               </div> 
             </div>
          <div class="col-md-4">
            <form class="navbar-form " method="GET" action="{{url('/usuario')}}" >
              {{-- csrf_field() --}}
                
                  
                  <div class="input-group mb-3">
                    <input type="text" class="form-control form-control-sm" placeholder="Nombre" name="nombres" value="{{@$nombres}}">
                    <div class="input-group-append">
                      <button class="btn btn-light btn-sm" type="submit"><i class="fa fa-search"></i> Buscar</button> 
                    </div>
                  </div>     
             </form>
             
          </div>
          
          
        </div><!--row-->   
             
			<div class="table-responsive " >
				
				<table class="table table-striped table-sm ">
					<thead>
						<tr>
              
							<th>ID</th>
							<th>Nombres</th>
							<th>Apellidos</th>
							<th>Email</th>
              <th>Estado</th>	
              <th>Rol</th>           					          				
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="datos">
					
						@foreach($usuarios as $usuario)
						<tr >
            <td>{{ $usuario->id_usuario }}</td>  
					
						<td>{{$usuario->nombres}}</td>
            <td>{{$usuario->apellidos}}</td>
            <td>{{$usuario->email}}</td>
          
						<td> {!! $variable = ($usuario->estado) ? "<span class='badge badge-success'> Activo <span>" : "<span class='badge badge-secondary'> Inactivo <span>"!!}</td>
            <td>{{$usuario->rol->nombre}}</td>
						
						<td>
							@if (permiso('usuario.editar'))
                <button   OnClick="editarUsuario('{{$usuario->id_usuario}}','{{$usuario->nombres}}','{{$usuario->apellidos}}','{{$usuario->email}}','{{$usuario->estado}}','{{$usuario->id_rol}}');"  title="Editar" class="btn btn-outline-primary btn-xs"  data-toggle='modal' data-target='#modalCrear'><i class='fa fa-edit fa-lg '></i></button>
              @endif
							
						</td>
						</tr>
						@endforeach
					</tbody>
						{{$usuarios->appends(Request::only(['nombres']))->render()}}
				</table>
			</div>	
		</div>
	</div>			
							
</div>
	
	   <div class="modal fade" id="modalCrear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
                <div class="modal-header"> 
                    <h5 class="modal-title"> <i class="icon-people icons "></i> Registro de usuarios</h5> 
                    <button type="button" class="close" data-dismiss="modal"  aria-label="close"><span aria-hidden="true">x</span></button> 
                   
                </div> 
               
                <form role="form" class="formUsuario"  method="POST" action=""  id="formUsuario"> 
                  <div class="modal-body"> 
                        <input type="hidden" name="ruta" id="ruta">
                      <div class="row"> 
                        
                          <div class="col-md-6"> 
                              <div class="form-group"> 
                                  <label for="nombre-error" class="">Nombres</label> 
                                  <input type="text" class="form-control "  id="nombres"  name="nombres" placeholder=" Ingrese Nombres" aria-describedby="nombres-error" aria-invalid="true"  required="required"> 
                                  <em id="nombres-error" class="error invalid-feedback"></em>
                              </div> 
                          </div> 
                          <div class="col-md-6"> 
                              <div class="form-group"> 
                                  <label for="field-2" class="">Apellidos</label> 
                                   <input type="text" class="form-control " id="apellidos"  name="apellidos" placeholder=" Ingrese apellidos" required="required">
                                    <em id="apellidos-error" class="error invalid-feedback"></em> 
                              </div> 
                          </div> 
                      </div> 
                      <div class="row ">
                         <div class="col-md-6"> 
                              <div class="form-group"> 
                                  <label for="field-2" class="">E-mail</label> 
                                   <input type="text" class="form-control " id="email"  name="email" placeholder="Ingrese su dirección  de correo electrónico" required="required">
                                    <em id="email-error" class="error invalid-feedback"></em> 
                              </div> 
                          </div> 
                         <div class="col-md-6">
                           <div class="form-group">
                              
                              <label for="field-1" class="">Estado</label>  
                             	@php
              									$estados=['0'=>'Inactivo','1'=>'Activo'];
              								@endphp
									            {!!Form::select('estado', $estados,null,['class'=>'form-control ','id'=>'estado'])!!}
					                      <em id="estado-error" class="error invalid-feedback"></em>        
    
                          </div>
                             
                        </div>
                      </div> 
                      <div class="row ">
                         <div class="col-md-6"> 
                              <div class="form-group"> 
                                  <label for="field-2" class="">Rol</label> 
                                  
                                    <select class="form-control" name="rol" id="idRol">
                                         <option selected value="" >Seleccione un rol</option>
                                         @foreach($roles as $rol)
                                         <option value="{{ $rol->id_rol }}">{{ $rol->nombre }}</option>
                                         @endforeach
                                    </select>
                                     <em id="rol-error" class="error invalid-feedback"></em>
                              </div> 
                          </div> 
                        
                      </div>  
                   </div> 
                  <div class="modal-footer"> 
                       <button  class="btn btn-default btn-sm" type="button" data-dismiss="modal">Cancelar</button> 
                          <button type="submit" class="btn btn-primary btn-sm" id="btnGuardar">Guardar</button> 
                     
                  </div> 
                </form>
				        <div id="mensajes" class=""></div> 
            </div>

        </div>
    </div><!-- /.modal -->
</div>
@endsection

@section('scripts')
	
<script type="text/javascript">
function recargarPagina()
{
    window.location.href='{{ url()->full() }}';
}
 function editarUsuario(idUsuario,nombres,apellidos,email,estado,idRol)
 {
    $("#btnGuardar").text('Guardar cambios');
    $("#mensajes").html('');
    $('#nombres').val(nombres);//asiganr un valor
    $('#apellidos').val(apellidos);//asiganr un valor
    $('#email').val(email);
    $('#estado').val(estado);
    $('#idRol').val(idRol);
    $('#ruta').val('{{ url('')}}/usuario/'+idUsuario+'/actualizar');
    rmMsjValidacion();
    
 }
 

 $("#btnAbrirModal").click(function()
 {
    $("#btnGuardar").text('Guardar');
    $("#mensajes").html('');
    $('#dni').val(''); 
    $('#nombres').val('');
    $('#apellidos').val('');
    $('#email').val('');
    $('#estado').val(0);
    $('#idRol').val('');
    $('#ruta').val("{{ url('usuario/insertar') }}");

    rmMsjValidacion();
 });

  $(document).on("submit",".formUsuario", function(e)
  {

    e.preventDefault();
      rmMsjValidacion();
   
     var datos= $('#formUsuario').serialize();
      var route=$("#ruta").val();
    $.ajax({
        url      : route,
        headers  : {'X-CSRF-TOKEN': "{{csrf_token()}}" },// envio del token
        type     :'POST',
        datatype : 'json',
        data     : datos,
        timeout  : 10000, //limite de tiempo 15 segundos 
        success  : function(respuesta)
        {
            if (respuesta.parametro=='edicion') 
            {
                $('#modalCrear').modal('toggle');
                setTimeout("recargarPagina()",1000);
            }
            else
          	    resetform();
          
            msgSuccess(respuesta.mensaje);
      },
      error: function(xhr, textStatus, thrownError)
      {
        if(xhr.status=422)
                addMsgValidation(xhr);
            else
                messagesxhr(xhr,textStatus);     
      }  
    });
  });

  function addMsgValidation(xhr)
  {
      let messages = xhr.responseJSON;
      
      msgError('Se encontraron errores, revice el formulario');
  
      $.each(messages, function (ind, elem) 
      {  
          $('#'+ind).addClass('is-invalid');
          $('#'+ind+'-error').text(elem);
      });
  }
  
function rmMsjValidacion()
{
    $('input').removeClass('is-invalid');
    $('select').removeClass('is-invalid');
    $('em').text('');
}

function resetform()
{
    $('#nombres').val('');
    $('#apellidos').val('');
    $('#email').val('');
    $('#estado').val('0');
    $('#idRol').val('');
}
 

</script>
@parent

@endsection 