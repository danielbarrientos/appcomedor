<?php


//rutas del front

//rutas de prueba
//Route::get('/calendario','PruebaController@calendario');
//Route::get('/graficoExcel','PruebaController@graficoExcel');
//Route::get('/exportarPdf','PruebaController@exportarPdf');
//Route::get('/subirExcelVenta','PruebaController@importarExcelVenta');
//Route::get('/subirExcelAtencion','PruebaController@importarExcelVenta');

//Route::get('/login', function () {
  //  return redirect('/');
//});
Route::get('/','front\PanelController@index');
Route::post('/iniciarSession', 'front\PanelController@iniciarSession');
Route::match(['get','post'],'/iniciarSesionAdmin', 'LoginController@iniciarSesionAdmin');
Route::get('/cerrarSesion', 'LoginController@cerrarSesion');

//Route::get('/olvido','back\UsuarioController@olvido');

Route::middleware(['ident'])->group(function () 
{
	//rutas del back
	Route::get('/panel','back\PanelController@index');
	Route::get('/semestre','back\SemestreController@index');
	Route::match(['get','post'],'/semestre/registrar','back\SemestreController@registrar');
	Route::match(['get','post'],'/semestre/{id_semestre}/editar','back\SemestreController@editar');

	//rutas para escuela
	Route::get('/escuela','back\EscuelaController@index');
	Route::post('/escuela/insertar','back\EscuelaController@insertar');
	Route::post('/escuela/{id_escuela}/actualizar','back\EscuelaController@actualizar');

	//rutas para servicio
	Route::get('/servicio','back\ServicioController@index');
	Route::post('/servicio/insertar','back\ServicioController@insertar');
	Route::post('/servicio/{id_escuela}/actualizar','back\ServicioController@actualizar');

	//rutas para usuario
	Route::get('/usuario','back\UsuarioController@index')->name('usuario.index')->middleware('permiso:usuario.index');
	Route::post('/usuario/insertar','back\UsuarioController@insertar')->name('usuario.registrar')->middleware('permiso:usuario.registrar');
	Route::post('/usuario/{id_escuela}/actualizar','back\UsuarioController@actualizar')->name('usuario.editar')->middleware('permiso:usuario.editar');;


	//rutas para cambiar de contraseña
	Route::match(['get','post'],'/cambiarPassword','back\UsuarioController@cambiarPassword');
	//Route::get('/consumo','PruebaController@index');
	//rutas para estudiante
	Route::get('/estudiante','back\EstudianteController@index');
	Route::post('/estudiante/insertar','back\EstudianteController@insertar');
	Route::get('/estudiante/{dni}/buscar','back\EstudianteController@buscar');
	Route::post('/estudiante/{codigo}/actualizar','back\EstudianteController@actualizar');
	Route::match(['get','post'],'/estudiante/registroMasivo','back\EstudianteController@registroMasivo');
	Route::get('/estudiante/exportarExcel','back\EstudianteController@exportarExcel');
	Route::get('estudiante/{dni}/ficha','back\EstudianteController@fichaPdf');
	Route::get('estudiante/{dni}/historial','back\EstudianteController@historialPdf');


	//rutas para beneficiario
	Route::get('/beneficiario','back\BeneficiarioController@index');
	Route::get('/beneficiario/escuela/{id}','back\BeneficiarioController@listaEscuela');
	Route::get('/beneficiario/estudiante/buscar','back\BeneficiarioController@buscarEstudiante');
	Route::post('/beneficiario/insertar','back\BeneficiarioController@insertar');
	Route::post('/beneficiario/eliminar','back\BeneficiarioController@eliminar');
	Route::post('/beneficiario/cambiarTipo','back\BeneficiarioController@cambiarTipo');
	Route::post('/beneficiario/{id_beneficiario}/actualizar','back\BeneficiarioController@actualizar');
	Route::post('/beneficiario/{id_beneficiario}/suspender','back\BeneficiarioController@suspender');
	Route::get('/beneficiario/escuela/{id_escuela}/listaPdf','back\BeneficiarioController@listaBeneficiariosPdf');
	Route::get('/beneficiario/escuela/{id_escuela}/listaVentaPdf','back\BeneficiarioController@listaVentaPdf');
	

	//rutas cupo programado
	Route::get('/cupoprogramado','back\CupoProgramadoController@index');
	Route::post('/cupoprogramado/insertar','back\CupoProgramadoController@insertar');
	Route::post('/cupoprogramado/actualizar','back\CupoProgramadoController@actualizar');

	//rutas periodo de atencion
	Route::get('/periodoAtencion','back\PeriodoAtencionController@index');
	Route::post('/periodoAtencion/insertar','back\PeriodoAtencionController@insertar');
	Route::post('/periodoAtencion/{id_periodoAtencion}/actualizar','back\PeriodoAtencionController@actualizar');
	Route::post('/periodoAtencion/cambiarEstado','back\PeriodoAtencionController@cambiarEstado');

	//rutas para venta de cupos
	Route::get('/ventaCupo','back\VentaController@index');
	Route::match(['get','post'],'/ventaCupo/registrarVentaRegular','back\VentaController@registrarVentaRegular')->name('ventaRegular');
	Route::match(['get','post'],'/ventaCupo/registrarVentaLibre','back\VentaController@registrarVentaLibre')->name('ventaLibre');

	Route::match(['get','post'],'/ventaCupo/reporteVenta','back\ReporteVentaController@index')->name('reporteVenta');
	Route::match(['get','post'],'ventaCupo/resumenVentaPdf','back\ReporteVentaController@resumenVentaPdf');
	Route::match(['get','post'],'ventaCupo/resumenVentaExcel','back\ReporteVentaController@resumenVentaExcel');
	Route::get('/ventaCupo/rptListaVentaPdf','back\ReporteVentaController@rptListaVentaPdf');

	//rutas para atencion de comensales
	Route::get('/atencion','back\AtencionController@index');
	Route::match(['get','post'],'/atencion/registrarAtencionRegular','back\AtencionController@registrarAtencionRegular')->name('atencionRegular');
	Route::match(['get','post'],'/atencion/registrarAtencionLibre','back\AtencionController@registrarAtencionLibre')->name('atencionLibre');
	Route::match(['get','post'],'/atencion/reporteAtencion','back\ReporteAtencionController@index')->name('reporteAtencion');

	Route::match(['get','post'],'/atencion/rptAtencionExcel','back\ReporteAtencionController@rptAtencionExcel');
	Route::match(['get','post'],'/atencion/rptAtencionPdf','back\ReporteAtencionController@rptAtencionPdf');

	//rutas para roles de usuario
	Route::get('/rol','back\RolController@index');
	Route::match(['get','post'],'/rol/registrar','back\RolController@registrar');
	Route::match(['get','post'],'/rol/{id_rol}/editar','back\RolController@editar');


	//Route::get('/excelestudiantes','PruebaController@exportarExcel');
	Route::get('/subirExcel','PruebaController@importarExcel');
	Route::post('/subirExcel','PruebaController@procesarExcel');
	//Route::match(['get','post'],'/subirFoto','PruebaController@subirFoto');

	//Rutas de ficha back
	Route::get('ficha/','back\FichaController@index');
	Route::get('ficha/reporteProcedencia','back\FichaController@reporteProcedencia');
	Route::get('ficha/reporteModalidadIngreso','back\FichaController@reporteModalidadIngreso');
	Route::get('ficha/reporteEdadPromedio','back\FichaController@reporteEdadPromedio');

});

//rutas del front
Route::middleware(['estudiante'])->group(function () 
{
	Route::get('fichaEstudiante/','front\PanelController@ficha');
	Route::post('fichaEstudiante/tab1','front\PanelController@fichaTab1');
	Route::post('fichaEstudiante/tab2','front\PanelController@fichaTab2');
	Route::post('fichaEstudiante/tab3','front\PanelController@fichaTab3');
	Route::post('fichaEstudiante/tab4','front\PanelController@fichaTab4');
	Route::post('fichaEstudiante/tab5','front\PanelController@fichaTab5');
	Route::get('fichaEstudiante/pdf','front\PanelController@fichaPdf');

	Route::get('fichaEstudiante/listProvincias/{id_departamento}','front\PanelController@listProvincias');
	Route::get('fichaEstudiante/listDistritos/{id_provincia}','front\PanelController@listDistritos');
});