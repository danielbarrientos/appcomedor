<?php

use App\Model\CupoProgramado;
use App\Model\Permiso;
use App\Model\PermisoRol;
use App\Model\Atencion;
use App\Model\Usuario;
use App\Model\Departamento;
use App\Model\Distrito;
use App\Model\Provincia;
use App\Model\Ficha;


//funcion q devuelve los cupos asignados para una escuela (devuelve )
function total_cupos($id_servicio,$id_escuela,$id_semestre=null)
{		
	if (is_null($id_semestre)) {
		$id_semestre=\session::get('id_semestre');
	}

	//$id_semestre=\Session::get('id_semestre');
	$cantidad=0;
	$cupo=CupoProgramado::where('id_servicio',$id_servicio)->where('id_escuela',$id_escuela)->where('id_semestre',$id_semestre)->select('cantidad')->first();
	if ($cupo) {
		return $cupo->cantidad;
	}
	return $cantidad;
}
//funcion q devuelve los cupos asignados para una escuela (devuelve un objeto)
function find_cupo($id_servicio,$id_escuela,$id_semestre=null)
{		
	if (is_null($id_semestre)) {
		$id_semestre=\session::get('id_semestre');
	}

	$cupo=CupoProgramado::where('id_servicio',$id_servicio)->where('id_escuela',$id_escuela)->where('id_semestre',$id_semestre)->select('id_cupoprogramado','cantidad')->first();
	if ($cupo) {
		return $cupo;
	}
	return null;
}

function buscar_asistencia($id_servicio,$dni_estudiante,$id_periodoatencion,$fecha=null)
{		
	 //verificamos si el comensal ya recibio su racion 
        //fecha aactual

        if (is_null($fecha)) {
            $dia=date('d');
            $mes=date('m');
            $anio=date('Y');    
        }else{
        	$fecha= strtotime($fecha);
            $dia=date('d',$fecha);
            $mes=date('m',$fecha);
            $anio=date('Y',$fecha);
        }
        $asistencia='-';
        $atencion=Atencion::where('id_periodoatencion',$id_periodoatencion)->where('dni_estudiante',$dni_estudiante)->where('id_servicio',$id_servicio)->whereDay('fecha',$dia )->whereMonth('fecha',$mes)->whereYear('fecha',$anio)->first();
        if ($atencion) {
        	$asistencia=1;
        }

        return $asistencia;
}
//funcion que evalua si el usuario tiene permiso para una acción
function permiso($ruta)
{
	$id_rol=Session::get('id_rol');
	$permiso=Permiso::where('ruta',$ruta)->first();
	
	if ($id_rol && $permiso) {
		$permisoRol=PermisoRol::where('id_permiso',$permiso->id_permiso)->where('id_rol',$id_rol)->first();
		//dump($permisoRol);
		//si existe el permiso
		if ($permisoRol) {
			return true;
		}
	}
	return false;
}

function buscar_ubigeo($tabla,$id)
{
	$query = null;
	switch ($tabla) {
		case 'departamento':
					$query = Departamento::find($id,['nombre']);
			break;
		case 'provincia':
					$query = Provincia::find($id,['nombre']);
			break;
		case 'distrito':
					$query = Distrito::find($id,['nombre']);
			break;	
	}

	return $query->nombre;
}

function group_array($temp_array,$groupkey)
{
	//convertimos el array de objetos en array de array
	$array = [];

	foreach ($temp_array as $item) 
	{
		$array[] = (array)$item;
	} 
		
	if( is_null( $array ) )
		return [];

	if (count($array)>0)
	{
		$keys = array_keys($array[0]);
		$removekey = array_search($groupkey, $keys);	

		if ($removekey===false)
			return false;
			//return array("Clave \"$groupkey\" no existe");
		else
			unset($keys[$removekey]);

		$groupcriteria = [];
		$return = [];

		foreach($array as $value)
		{
			$item=null;
			foreach ($keys as $key)
			{
				
				$item[$key] = $value[$key];
			}
			$busca = array_search($value[$groupkey], $groupcriteria);
			if ($busca === false)
			{
				$groupcriteria[]=$value[$groupkey];
				$return[]=[$groupkey=>$value[$groupkey],'groupeddata'=>[]];
				$busca=count($return)-1;
			}
			$return[$busca]['groupeddata'][]=$item;
		}
		return $return;
	}
	else
		return [];
}

 function formatoDatosFicha($ficha)
	{
		$data = [];
		
		$data['contacto'] = json_decode($ficha->contacto_emergencia);
		$data['lugar_nacimiento'] = json_decode($ficha->lugar_nacimiento);
		$data['lugar_procedencia'] = json_decode($ficha->lugar_procedencia);
		$data['ubicacion_domicilio'] = json_decode($ficha->ubicacion_domicilio);
		$data['alimentacion'] = json_decode($ficha->alimentacion);
		$data['composicion_familiar'] = json_decode($ficha->composicion_familiar);
		$data['discapacidad_familar'] = json_decode($ficha->discapacidad_familiar);
		$data['economia_familiar'] = json_decode($ficha->economia_familiar);
		$data['embarazo'] = json_decode($ficha->embarazo);
		$data['gastos'] = json_decode($ficha->gastos);
		$data['primer_idioma'] = $ficha->primer_idioma;
		$data['limitacion_fisica'] = $ficha->limitacion_fisica;
		$data['posesiones'] = $ficha->posesiones;
		$data['servicios_vivienda'] = $ficha->servicios_vivienda;
		$data['transporte']            = json_decode($ficha->transporte);;
		$data['nucleo_familiar'] = json_decode($ficha->nucleo_familiar);
		$data['composicion_familiar'] = json_decode($ficha->composicion_familiar);
		$data['salud_familia'] = json_decode($ficha->salud_familia);
		$data['seguro_salud'] = json_decode($ficha->seguro_salud);
		$data['situacion_familiar'] = json_decode($ficha->situacion_familiar);
		$data['violencia_politica'] = json_decode($ficha->violencia_politica);
		$data['vive_con'] = $ficha->vive_con;
		$data['vivienda'] = json_decode($ficha->vivienda);
		$data['antecedente_academico'] = json_decode($ficha->antecedente_academico);
		
		return $data;
	}

	function existeFicha($dni_estudiante)
	{
		$ficha = Ficha::where('dni_estudiante',$dni_estudiante)
						->where('antecedente_academico','<>',NULL)->first();
		if($ficha)
			return true;
		else
			return false;
	}
	function obtenerPromedio($dni_estudiante)
	{
		$data = [];
		$ficha = Ficha::where('dni_estudiante',$dni_estudiante)
						->select('creditos_semestre_anterior','promedio_semestre_anterior')->first();
		if($ficha)
		{
			$data['creditos'] = $ficha->creditos_semestre_anterior;
			$data['promedio'] = $ficha->promedio_semestre_anterior;
		}
		else{
			$data['creditos'] = '';
			$data['promedio'] = '';
		}
		return $data;
	}


