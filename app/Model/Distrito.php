<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    protected $table='distrito';
	protected $primaryKey='id_distrito';
	public $incrementing=false;
    public $timestamps=false;
    

    public function provincia()
    {
		return $this->belongsTo('App\Model\Provincia','id_provincia');
	}
}
