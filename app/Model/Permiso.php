<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table='permiso';
	protected $primaryKey='id_permiso';
	public $incrementing=true;
	public $timestamps=false;
	
	public function roles(){
		return $this->belongsToMany('App\Model\Rol','permiso_rol','id_rol','id_permiso');
	}
}
