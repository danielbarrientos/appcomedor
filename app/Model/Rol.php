<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
     protected $table='rol';
	protected $primaryKey='id_rol';
	public $incrementing=true;
	public $timestamps=false;

  public function permisos(){
		return $this->belongsToMany('App\Model\Permiso','permiso_rol','id_rol','id_permiso');
	}
}
