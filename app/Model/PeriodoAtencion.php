<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class PeriodoAtencion extends Model
{
    protected $table='periodo_atencion';
    protected $primaryKey='id_periodoatencion';
	public $timestamps=false;
	protected $casts=['fecha_inicio' => 'date','fecha_fin'=>'date', 'fecha_creacion'=>'timestamp','fecha_actualizacion'=>'timestamp','id_periodoatencion'=>'integer','estado'=>'string'];

	protected $dates=['fecha_inicio','fecha_fin','fecha_registro','fecha_actualizacion'];

	protected $dateFormat ='Y-m-d';

	public function semestre()
	{
		return $this->belongsTo('App\Model\Semestre','id_semestre');
	}
	public function consumos()
	{
		return $this->hasMany('App\Model\Atencion','id_periodoatencion', 'id_periodoatencion');
	}

	public function ventas_cupo()
	{
		return $this->hasMany('App\Model\Venta','id_periodoatencion', 'id_periodoatencion');
	}

}
