<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Ficha extends Model
{
    protected $table='ficha';
	protected $primaryKey='id_ficha';
	public $incrementing=true;
    public $timestamps=false;
 
    //relaciones
    public function estudiante()
    {
		return $this->belongsTo('App\Model\Estudiante','dni');
    }
    
    public function fichas()
	{
	    return $this->hasMany('App\Model\Ficha','dni_estudiante', 'dni');
    }
    
    //consultas para reportes

    public static function procedenciaEstudiantes($id_departamento,$estado_matricula)
    {
        DB::statement("set session sql_mode='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
        $sub_query ='';
        if($estado_matricula == 1)
            $sub_query ='AND est.matricula = 1';

        if($id_departamento == '0')
        {
           
            $procedencias = DB::select("SELECT dep.id_departamento AS id_departamento,pro.id_provincia AS id_provincia,dep.nombre AS grupo,pro.nombre AS subgrupo, COUNT(*) AS cantidad 
                                        FROM estudiante est,ficha f,departamento dep,provincia pro 
                                        WHERE est.dni=f.dni_estudiante AND  dep.id_departamento = pro.id_departamento 
                                        AND JSON_EXTRACT(f.lugar_procedencia, '$.id_provincia') = pro.id_provincia $sub_query
                                        GROUP BY pro.id_provincia");
        }
        else
        {
           
            $procedencias = DB::select("SELECT dep.id_departamento AS id_departamento,pro.id_provincia AS id_provincia,dep.nombre AS departamento,pro.nombre AS grupo, dis.nombre AS subgrupo, COUNT(*) AS cantidad 
                                        FROM estudiante est,ficha f,departamento dep,provincia pro,distrito dis 
                                        WHERE  est.dni=f.dni_estudiante AND dep.id_departamento = pro.id_departamento 
                                        AND pro.id_provincia = dis.id_provincia 
                                        AND JSON_EXTRACT(f.lugar_procedencia, '$.id_distrito') = dis.id_distrito AND dep.id_departamento = $id_departamento $sub_query
                                        GROUP BY dis.id_distrito");
        } 
        
        $data = group_array( $procedencias, 'grupo' );
        
        return $data;                    
    }

    public static function modalidadIngreso($estado_matricula)
    {
        DB::statement("set session sql_mode='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
        $sub_query ='';
        if($estado_matricula == 1)
            $sub_query ='AND est.matricula = 1';

        $modalidades = DB::select(" SELECT esc.id_escuela,esc.nombre AS subgrupo, f.antecedente_academico->'$.modalidad_ingreso' AS grupo, COUNT(*) AS cantidad 
                        FROM ficha f, estudiante est,escuela esc 
                        WHERE f.dni_estudiante = est.dni AND est.id_escuela = esc.id_escuela    
                        AND f.antecedente_academico->'$.modalidad_ingreso' IS NOT NULL  $sub_query
                        GROUP BY  f.antecedente_academico->'$.modalidad_ingreso',esc.id_escuela ORDER BY esc.id_escuela");

        $data = group_array( $modalidades, 'grupo' );
        
        return   $data;              
       
    }

    public static function edadPromedio($estado_matricula)
    {
        
        DB::statement("SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
        $sub_query ='';
        if($estado_matricula == 1)
            $sub_query ='AND est.matricula = 1';

        $edades = DB::select("SELECT esc.id_escuela, esc.nombre AS grupo,est.sexo AS subgrupo, FORMAT(AVG(YEAR(CURRENT_DATE) - YEAR(est.fecha_nacimiento)) - (RIGHT(CURRENT_DATE,5) < RIGHT(est.fecha_nacimiento,5)),0) AS promedio 
                            FROM estudiante est,escuela esc 
                            WHERE est.id_escuela = esc.id_escuela 
                            AND est.fecha_nacimiento IS NOT NULL $sub_query
                            GROUP BY esc.id_escuela,est.sexo
                            ORDER BY promedio, esc.id_escuela ASC");

        $data = group_array( $edades, 'grupo' );

        return   $data;  
    }
}
