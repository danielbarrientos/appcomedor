<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table = 'estudiante';
	protected $primaryKey = 'dni';
	
	public    $incrementing = false;
	protected $keyType = 'string';
	public    $timestamps = false;

	protected $fillable = [
		'dni_estudiante',
		'codigo_universitario',
		'apellidos',
		'nombres',
		'fecha_nacimiento',
		'matricula',
		'id_escuela',
		'codigo_rfid'
    ];

	protected $casts=[
		'codigo_universitario' => 'string',
		'dni_estudiante'       => 'string', 
		'nombres'              => 'string',
		'apellidos'            => 'string',
		'id_escuela'           => 'integer',
		'matricula'            => 'integer',
		'codigo_rfid'          => 'string'
	];

	public function escuela()
	{
		return $this->belongsTo('App\Model\Escuela','id_escuela');
	}

	public function beneficiario()
	{
		return $this->hasMany('App\Model\BeneficiarioTemp','dni_estudiante', 'dni');
	}

	public function beneficiarios()
	{
		return $this->hasMany('App\Model\Beneficiario','dni_estudiante', 'dni');
	}

	public function ventas_cupo()
	{
		return $this->hasMany('App\Model\Venta','dni_estudiante', 'dni');
	}

	public function getNombreFotoAttribute($value)
    {
    	if (is_null($value) || empty($value)) {
    		$value='foto_default.jpg';
		}
		
        return $value;
	}
	
	public function scopeDni($query, $parametro)
    {
        if ($parametro) 
			return $query->where('dni', 'LIKE', "%".$parametro."%");   
	}

	public function scopeCodigo($query, $parametro)
    {
        if ($parametro) 
			return $query->where('codigo', 'LIKE', "%".$parametro."%");   
	}
	
	public function scopeNombres($query, $parametro)
    {
        if ($parametro) 
            return $query->where('nombres', 'LIKE', "%".$parametro."%");   
	}
	public function scopeApellidos($query, $parametro)
    {
        if ($parametro) 
            return $query->where('apellidos', 'LIKE', "%".$parametro."%");   
    }
}
