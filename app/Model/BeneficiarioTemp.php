<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class BeneficiarioTemp extends Model
{
    protected $table='beneficiario_temp';
	protected $primaryKey='id_beneficiario';

	public $incrementing=true;
	public $timestamps=false;
	protected $fillable = [
        'id_semestre','id_escuela', 'dni_estudiante', 'tipo','estado','observacion'
    ];
	

	public function estudiante()
	{
		return $this->belongsTo('App\Model\Estudiante','dni_estudiante','dni');
	}
	public function semestre()
	{
		return $this->belongsTo('App\Model\Semestre','id_semestre','id_semestre');
	}

	public static function adicional()
	{
		return 1;
	}
	public static function regular()
	{
		return 2;
	}
	public static function becaA()
	{
		return 5;
	}
	public static function becaB()
	{
		return 4;

	}
	public static function becaC()
	{
		return 3;
	}

	public static function totalBeneficiarios()
	{
		$data = DB::select("SELECT id_escuela,nombre,siglas, total_adicional, total_regular,total_becac,total_becab,total_becaa
			FROM (
				SELECT e.id_escuela as id_escuela, e.nombre AS nombre, e.siglas AS siglas, 
				(
					SELECT IFNULL( COUNT( bt.id_beneficiario ) , 0 ) 
					FROM beneficiario_temp bt
					WHERE e.id_escuela = bt.id_escuela AND bt.id_tipo= 1
				) AS total_adicional, 
				(   
					SELECT IFNULL( COUNT( bt.id_beneficiario ) , 0 ) 
					FROM beneficiario_temp bt
					WHERE e.id_escuela = bt.id_escuela AND bt.id_tipo= 2
				) AS total_regular,
				(   
					SELECT IFNULL( COUNT( bt.id_beneficiario ) , 0 ) 
					FROM beneficiario_temp bt
					WHERE e.id_escuela = bt.id_escuela AND bt.id_tipo= 3
				) AS total_becac,
				(   
					SELECT IFNULL( COUNT( bt.id_beneficiario ) , 0 ) 
					FROM beneficiario_temp bt
					WHERE e.id_escuela = bt.id_escuela AND bt.id_tipo= 4
				) AS total_becab,
				(   
					SELECT IFNULL( COUNT( bt.id_beneficiario ) , 0 ) 
					FROM beneficiario_temp bt
					WHERE e.id_escuela = bt.id_escuela AND bt.id_tipo= 5
				) AS total_becaa
				FROM escuela e ORDER BY e.id_escuela ASC
			)QA");

		return $data;	
	}
	
}
