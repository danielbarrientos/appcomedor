<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table='provincia';
	protected $primaryKey='id_provincia';
	public $incrementing=false;
    public $timestamps=false;
    
    public function distritos()
	{
	    return $this->hasMany('App\Model\Distrito','id_provincia', 'id_provincia');
    }
    
    public function departamento()
    {
		return $this->belongsTo('App\Model\Departamento','id_departamento');
	}
}
