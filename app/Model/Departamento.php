<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table='departamento';
	protected $primaryKey='id_departamento';
	public $incrementing=false;
    public $timestamps=false;
    
    public function provincias()
	{
	    return $this->hasMany('App\Model\Provincia','id_departamento', 'id_departamento');
	}
}
