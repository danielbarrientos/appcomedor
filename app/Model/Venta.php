<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table='venta';
    protected $primaryKey='id_venta';
	public $timestamps=false;

	protected $dates=['fecha_registro','fecha_actualizacion'];

	public function periodo_atencion(){
		return $this->belongsTo('App\Model\PeriodoAtencion','id_periodoatencion');
	}
	public function estudiante(){
		return $this->belongsTo('App\Model\Estudiante','dni_estudiante');
	}
	public function servicio(){
		return $this->belongsTo('App\Model\Servicio','id_servicio');
	}
	public function encuela(){
		return $this->belongsTo('App\Model\Escuela','id_escuela');
	}
}
