<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PermisoRol extends Model
{
    protected $table='permiso_rol';
	protected $primaryKey='id_permiso_rol';
	public $incrementing=true;
	public $timestamps=false;
}
