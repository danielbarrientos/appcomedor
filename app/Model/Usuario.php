<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table='usuario';
    protected $primaryKey='id_usuario';
	public $timestamps=false;
	protected $fillable = [
        'nombres','apellidos','email','password','estado','id_rol'
    ];

    public function rol(){
		return $this->belongsTo('App\Model\Rol','id_rol','id_rol');
	}
}
