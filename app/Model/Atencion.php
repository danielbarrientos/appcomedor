<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Atencion extends Model
{
  protected $table='atencion';
	protected $primaryKey='id_atencion';
	public $incrementing=true;
	public $timestamps=false;

	protected $dates=['fecha','fecha_registro','fecha_actualizacion'];
	public function periodo_atencion()
	{
		return $this->belongsTo('App\Model\PeriodoAtencion','id_periodoatencion');
	}
}
