<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table='tipo';
	protected $primaryKey='id_tipo';
	public $incrementing=true;
	public $timestamps=false;
}
