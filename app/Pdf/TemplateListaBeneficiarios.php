<?php

namespace App\Pdf;

use Crabbly\FPDF\FPDF;

class TemplateListaBeneficiarios extends FPDF
{
    protected $escuela;

    function __construct($orientation='P', $unit='mm', $size='A4',$escuela = '') 
    {
        parent::__construct();
       
        $this->escuela = $escuela;
    }

    function Header() 
    {
        $this->Image('img/brand/unamba.png',10,7,33);

         $this->Image('archivos/escuelas/'.$this->escuela->siglas.'.png',185,7,12);

        // Arial bold 15
        $this->SetFont('Times','B',10);
        // Movernos a la derecha

        $this->Cell(60);
        // Título
        $this->Cell(80,5,utf8_decode('OFICINA DE BIENESTAR UNIVERSITARIO'),0,0,'C');
        $this->Ln();
        $this->SetFont('Times','',8);
        $this->Cell(200,5,utf8_decode('LISTA DE BENEFICIARIOS EAP '.$this->escuela->nombre),0,0,'C');
        $this->Ln(7);
    }

    function Footer() 
    {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'C');
    }
}
