<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Estudiante;
use App\Model\Escuela;
use Session;
//use Excel;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Contracts\Routing\ResponseFactory as Response;
use Crabbly\FPDF\FPDF;

class PruebaController extends Controller
{
    public function index()
    {   
        $escuelas=Escuela::withCount('estudiantes')->where('grupo','A')->get();
        return view('consumo',compact('escuelas')); 
    }
    public function procesar( Request $request){
    	/*if (Request::isMethod('get')) {
            return redirect->route('/');
        }*/
        $this->validate($request,['codigo'=>'required|digits_between:6,10']);//validamos los datos
		$codigo=trim($request->get('codigo'));
        if (strlen($codigo)===8) {
             $estudiante=Estudiante::whereRaw('dni=? ',[$codigo])->first();//consulta a la BD    
        }
        else{
            $codigo=substr($codigo,-6);
            $estudiante=Estudiante::whereRaw('codigo=? ',[$codigo])->first();//consulta a la BD    
        }
        $escuelas=Escuela::withCount('estudiantes')->where('grupo','A')->get();  
        if ($estudiante) {
            return view('consumo',compact('estudiante','escuelas'));    
        }
        $request->session()->flash('msj-warning', 'El alumno no se encuentra en la lista ');
        return redirect()->back();	    	
    }

    public function exportarExcel()
    {   
        $estudiantes=Estudiante::All();
         $datos;//arreglo de arreglos
         $i=1;
           foreach ($estudiantes as $estudiante) {
            $i++;
                $datos[]=[$estudiante->codigo, $estudiante->dni,$estudiante->nombre,$estudiante->apellido,$estudiante->escuela_id];
            }
        Excel::create('Lista de estudiantes', function($excel) use($datos,$i)
        {
            $excel->sheet('Hoja 1', function($sheet) use($datos,$i)
            {
                $sheet->setOrientation('landscape');
                //$sheet->mergeCells('A1:E1');
                $sheet->setAutoSize(true);
                //$sheet->setHeight(1,50);

                $sheet->cells('A1:E1',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    //$cells->setBackground('#1497cc');
                    $cells->setBackground('#2467BA');
                    $cells->setFontColor('#ffffff');
                    $cells->setFontSize(12);
                   
                });

                $sheet->setBorder('A2:E'.$i, 'thin');//borde de todo
                $sheet->cell('A1', function($cell){$cell->setValue('CODIGO');});
                $sheet->cell('B1', function($cell){$cell->setValue('DNI');});
                $sheet->cell('C1', function($cell){$cell->setValue('NOMBRE');});
                $sheet->cell('D1', function($cell){$cell->setValue('APELLIDO');});
                $sheet->cell('E1', function($cell){$cell->setValue('ESCUELA_ID');});   

               $sheet->fromArray($datos,null,'A2',false,false);//aqui se forma la matriz con los datos

                          
            });
        })->export('xls');
        
        
    }
    public function calendario()
    {
          Excel::create('calendario', function($excel) {

            $excel->sheet('mes1', function($sheet) {

                $fechas=DB::select('SELECT DISTINCT DATE_FORMAT(fecha, "%d-%m-%Y") AS dia FROM atencion WHERE id_periodoatencion=?',[1]);
         
            $comensales= DB::table('venta_cupo')
                    ->join('estudiante', 'venta_cupo.dni_estudiante', '=', 'estudiante.dni_estudiante')
                    ->join('periodo_atencion', 'venta_cupo.id_periodoatencion', '=', 'periodo_atencion.id_periodoatencion')
                    ->where('periodo_atencion.id_periodoatencion',1)->where('venta_cupo.id_servicio',2)->where('estudiante.id_escuela',1)
                    ->select('venta_cupo.*', 'estudiante.nombres','estudiante.apellidos','estudiante.dni_estudiante')->orderBy('tipo_venta','DESC')
                    ->get();
            
            //convertimos la coleccion en array       
            $comensales=$comensales->toArray();
            
           if (count($comensales)>0) {
                //asignamos las columnas deasistencia por cada fecha
                for ($i=0;  $i <count($comensales) ; $i++) {
                    //convertimos el objeto en array
                    $datos[$i]=(array) $comensales[$i];
                    //generamos una columna por cada dia atendido y buscamos la asistencia
                    foreach ($fechas as $fecha){
                        $datos[$i]=array_add($datos[$i], $fecha->dia, buscar_asistencia($comensales[$i]->id_servicio,$comensales[$i]->dni_estudiante,$comensales[$i]->id_periodoatencion,$fecha->dia));
                    }
                    
                }
                
                $comensales=$datos;

                $sheet->loadView('calendario',compact('comensales','fechas') );
             }   

            });

        })->export('xls');

    }
   public function graficoExcel()
    {
        $excel = new \PHPExcel();

        $excel->createSheet();
        $excel->setActiveSheetIndex(1);
        $excel->getActiveSheet()->setTitle('ChartTest');

        $objWorksheet = $excel->getActiveSheet();
        $objWorksheet->fromArray(
                array(
                    array('', 'Rainfall (mm)', 'Temperature (°F)', 'Humidity (%)'),
                    array('Jan', 78, 52, 61),
                    array('Feb', 64, 54, 62),
                    array('Mar', 62, 57, 63),
                    array('Apr', 21, 62, 59),
                    array('May', 11, 75, 60),
                    array('Jun', 1, 75, 57),
                    array('Jul', 1, 79, 56),
                    array('Aug', 1, 79, 59),
                    array('Sep', 10, 75, 60),
                    array('Oct', 40, 68, 63),
                    array('Nov', 69, 62, 64),
                    array('Dec', 89, 57, 66),
                )
        );

        $dataseriesLabels1 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Grafico!$B$1', NULL, 1), //  Temperature
        );
        $dataseriesLabels2 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Grafico!$C$1', NULL, 1), //  Rainfall
        );
        $dataseriesLabels3 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Grafico!$D$1', NULL, 1), //  Humidity
        );

        $xAxisTickValues = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Grafico!$A$2:$A$13', NULL, 12), //  Jan to Dec
        );

        $dataSeriesValues1 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'Grafico!$B$2:$B$13', NULL, 12),
        );

        //  Build the dataseries
        $series1 = new \PHPExcel_Chart_DataSeries(
                \PHPExcel_Chart_DataSeries::TYPE_BARCHART, // plotType
                \PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED, // plotGrouping
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataseriesLabels1, // plotLabel
                $xAxisTickValues, // plotCategory
                $dataSeriesValues1                              // plotValues
        );
        //  Set additional dataseries parameters
        //      Make it a vertical column rather than a horizontal bar graph
        $series1->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);

        $dataSeriesValues2 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'Grafico!$C$2:$C$13', NULL, 12),
        );

        //  Build the dataseries
        $series2 = new \PHPExcel_Chart_DataSeries(
                \PHPExcel_Chart_DataSeries::TYPE_LINECHART, // plotType
                \PHPExcel_Chart_DataSeries::GROUPING_STANDARD, // plotGrouping
                range(0, count($dataSeriesValues2) - 1), // plotOrder
                $dataseriesLabels2, // plotLabel
                NULL, // plotCategory
                $dataSeriesValues2                              // plotValues
        );

        $dataSeriesValues3 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'Grafico!$D$2:$D$13', NULL, 12),
        );

        //  Build the dataseries
        $series3 = new \PHPExcel_Chart_DataSeries(
                \PHPExcel_Chart_DataSeries::TYPE_AREACHART, // plotType
                \PHPExcel_Chart_DataSeries::GROUPING_STANDARD, // plotGrouping
                range(0, count($dataSeriesValues2) - 1), // plotOrder
                $dataseriesLabels3, // plotLabel
                NULL, // plotCategory
                $dataSeriesValues3                              // plotValues
        );


        //  Set the series in the plot area
        $plotarea = new \PHPExcel_Chart_PlotArea(NULL, array($series1, $series2, $series3));
        $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        $title = new \PHPExcel_Chart_Title('Grafica anhelada maternofetal :(');

        //  Create the chart
        $chart = new \PHPExcel_Chart(
                'chart1', // name
                $title, // title
                $legend, // legend
                $plotarea, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                NULL, // xAxisLabel
                NULL            // yAxisLabel
        );

        //  Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition('F2');
        $chart->setBottomRightPosition('O16');

        //  Add the chart to the worksheet
        $objWorksheet->addChart($chart);

        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->setIncludeCharts(TRUE);

        // Save the file.
        $writer->save(public_path().'/file.xlsx');
    }
    public function importarExcel(){
        return view('subirExcel');

    }
    public function procesarExcel(Request $request){
        set_time_limit(180);
        $archivo=$request->file('archivo');
        $nombreOriginal=$archivo->getClientOriginalName();
        //$r1=\Storage::disk('archivos')->put($nombreOriginal,\File::get($archivo));
        $r1= $request->file('archivo')->move(public_path().'/archivos/', $nombreOriginal);
        if ($r1) {
           
               Excel::selectSheetsByIndex(0)->load('public/archivos/'.$nombreOriginal,function($hoja){
             
                $hoja->each(function($fila){
                  
                    $codigo=Estudiante::where('codigo_estudiante',$fila->CODIGO_ESTUDIANTE)->first();
                        
                    if (count($codigo)==0) {

                        $estudiante= new Estudiante;
                        $estudiante->codigo_estudiante=$fila->codigo_estudiante;
                        //$estudiante->dni=$fila->dni;
                        $estudiante->nombres=$fila->nombres;
                        $estudiante->apellidos=$fila->apellidos;
                        $estudiante->id_escuela=$fila->id_escuela;
                        $estudiante->save();
                        
                    }
                });

            });
            $request->session()->flash('msj-success',' Alumnos registrados correctamente');
            return redirect()->back();
        }
         $request->session()->flash('msj-error',' Ocurrio un error al subir el archivo');
            return redirect()->back();
    }
    public function subirFoto(Request $request){
        if ($request->isMethod('get')) {
            return view('subirFoto');
        }
         if ($request->isMethod('post')) {

            $archivo=$request->file('archivo');
            $nombreOriginal=$archivo->getClientOriginalName();
            $r1=\Storage::disk('local')->put($nombreOriginal,\File::get($archivo));
            $request->session()->flash('msj-success',' foto subida correctamente!!!');
            return redirect()->back();
        }
    }

    public function cambiarPassword(){
        $estudiante=Estudiante::find('73219797');
        $estudiante->password=\Hash::make('73219797');
        $estudiante->save();
        echo " clave cambiada correctamente";
    }

    public function exportarPdf(Response $response)
    {   
            $fechas=DB::select('SELECT DISTINCT DATE_FORMAT(fecha, "%d-%m-%Y") AS dia FROM atencion WHERE id_periodoatencion=?',[1]);
         
            $comensales= DB::table('venta_cupo')
                    ->join('estudiante', 'venta_cupo.dni_estudiante', '=', 'estudiante.dni_estudiante')
                    ->join('periodo_atencion', 'venta_cupo.id_periodoatencion', '=', 'periodo_atencion.id_periodoatencion')
                    ->where('periodo_atencion.id_periodoatencion',1)->where('venta_cupo.id_servicio',2)->where('estudiante.id_escuela',5)
                    ->select('venta_cupo.*', 'estudiante.nombres','estudiante.apellidos','estudiante.dni_estudiante')->orderBy('tipo_venta','DESC')
                    ->get();
            
            //convertimos la coleccion en array       
            $comensales=$comensales->toArray();
            
           if (count($comensales)>0) {
                //asignamos las columnas deasistencia por cada fecha
                for ($i=0;  $i <count($comensales) ; $i++) {
                    //convertimos el objeto en array
                    $datos[$i]=(array) $comensales[$i];
                    //generamos una columna por cada dia atendido y buscamos la asistencia
                    foreach ($fechas as $fecha){
                        $datos[$i]=array_add($datos[$i], $fecha->dia, buscar_asistencia($comensales[$i]->id_servicio,$comensales[$i]->dni_estudiante,$comensales[$i]->id_periodoatencion,$fecha->dia));
                    }
                    
                }
                
                $comensales=$datos;

       
            $pdf = new FPDF('P','mm','A4');//P:vertical,L: hirizontal
              //$pdf->SetTopMargin(15.0);
            $pdf->SetMargins(6.0, 10.0, 6.0);
            $pdf->AliasNbPages();
            $pdf->AddPage();

            $this->header($pdf);
          // $pdf->SetXY(10, 10);
            $pdf->SetFont('Arial','B',7);
            //(rojo,azul,verde)
            //$pdf->SetFillColor(2,157,116);//Fondo verde de celda
            $pdf->SetFillColor(2,110,200);//Fondo verde de celda
            $pdf->setDrawColor(200, 220, 255);//color  delos bordes
            $pdf->SetTextColor(240, 255, 240); //Letra color blanco

           
            $pdf->Cell(6, 3,'',1,0,'C',true);
            $pdf->Cell(12, 3,'',1,0,'C',true);
            $pdf->Cell(42, 3,'',1,0,'C',true);
            $pdf->Cell(10, 3,'' ,1,0,'C',true);
             
            $pdf->Cell(8*count($fechas), 3,'Periodo 1',1,0,'C',true);
            
            $pdf->Ln();

            $pdf->Cell(6, 5,'#',1,0,'C',true);
            $pdf->Cell(12, 5,'DNI',1,0,'C',true);
            $pdf->Cell(42, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
            $pdf->Cell(10, 5,'VENTA' ,1,0,'C',true);
            foreach ($fechas as $fecha) {
                 $pdf->Cell(8, 5,date("d/m", strtotime($fecha->dia)) ,1,0,'C',true);
            }
     
            $headers=['Content-Type' => 'application/pdf'];

            return $response->make($pdf->Output('I'), 200, $headers);

        }
        
    }
    public static function header($pdf){
        //$pdf->Image('images/logo.png',10,8,33);
        // Arial bold 15
        $pdf->SetFont('Arial','B',12);
        // Movernos a la derecha
        $pdf->Cell(60);
        // Título
        $pdf->Cell(55,10,utf8_decode('Consolidado de asistencia'),0,0,'C');
        // Salto de línea
        $pdf->Ln(8);
    }
    
}
