<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Usuario;
use App\Model\Semestre;
use Illuminate\Support\Facades\Hash;
//use Illuminate\Support\Facades\Validator;
use Session;

class LoginController extends Controller
{

	public function iniciarSesionAdmin(Request $request)
	{
		if($request->isMethod('get'))
		{
			if (Session::has('id_usuario')) 
			{
				return redirect('/panel');
			}
			
			 return view('general.login');
		}
    	
    	//if ($this->bloqueo()) {return back()->with('msg-error','Esta bloqueado');}//llamamos a la funcion de bloqueo 
    	//$this->validate($request,['email'=>'bail|required|email','password'=>'required']);//validamos los datos
		$usuario=Usuario::whereRaw('email=? and estado=1',[trim($request->get('email'))])->first();//consulta a la BD
			
		if ($usuario && Hash::check(trim($request->get('password')),$usuario->password)) 
		{
			$semestre = Semestre::where('estado',1)->first();

			$request->session()->put('id_usuario', $usuario->id_usuario);
			$request->session()->put('nombre', $usuario->nombres);
			$request->session()->put('id_rol', $usuario->id_rol);
			$request->session()->put('id_semestre', $semestre->id_semestre);

			$request->session()->put('nombre_semestre',$semestre->descripcion);
			$request->session()->flash('msg-info','Bienvenido(a) '.$usuario->nombres);

			return redirect('/panel');
		}

		Session::flash('msg-error','Usuario y/o contraseña incorectos');
		
		return redirect('iniciarSesionAdmin');
	}
	
	

    public function cerrarSesion(Request $request){
    	$request->session()->flush();//eliminamis todas las variables de sesión
    	$request->session()->flash('msg-success','Has cerrado sessión');
    	return redirect('/');
    }

	public function bloqueo()
	{
		if (Session::has('contadorBloqueo')) 
		{
    		$contador=Session::get('contadorBloqueo');
		
			Session::put('contadorBloqueo',$contador +=1);
		
			if ($contador>3 && Session::has('horaBloqueo')) 
			{
    			$horaBloqueo=Session::get('horaBloqueo');
    			$horaActual=date('Hi');
    			if ($horaBloqueo < $horaActual) {
    				Session::forget('contadorBloqueo');//borramos la hora de bloqueo
    				Session::forget('horaBloqueo');//borramos la hora de bloqueo
    				return false;
    			}
    		}
			
			if ($contador>3 && !Session::has('horaBloqueo')) 
			{
    			Session::put('horaBloqueo',date('Hi'));
    			Session::flash('msg-error','Se ha realizado 4 intentos fallidos,intentelo otra vez en 1 minuto');
				return true;
    		}
			
			return false;
    	}
    	else
    		Session::put('contadorBloqueo',1);
    		return false;
    }
}