<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\BeneficiarioTemp;
use App\Model\CupoProgramado;
use App\Model\Escuela;
use App\Model\Estudiante;
use App\Model\Tipo;
use App\Model\Semestre;
use DB;
use Session;
//use Crabbly\FPDF\FPDF;
use Illuminate\Contracts\Routing\ResponseFactory as Response;
use \App\Pdf\TemplateListaBeneficiarios;

class BeneficiarioController extends Controller
{
    public function index()
    {
        $datos['escuelas'] = BeneficiarioTemp::totalBeneficiarios();;
        
        return view('beneficiario.index',$datos);
    }

    public function listaEscuela(Request $request,$id_escuela)
    {
       
        if($request->ajax())
        {
            $beneficiarios = Estudiante::join('escuela', 'escuela.id_escuela', '=', 'estudiante.id_escuela')
                            ->join('beneficiario_temp', 'beneficiario_temp.dni_estudiante', '=', 'estudiante.dni')
                            ->join('tipo', 'tipo.id_tipo', '=', 'beneficiario_temp.id_tipo')
                            ->where('beneficiario_temp.id_escuela',$id_escuela)
                            ->nombres($request->nombres)
                            ->apellidos($request->apellidos)
                            ->codigo($request->codigo)
                            ->dni($request->dni)
                            ->orderBy('estudiante.apellidos', 'ASC')
                            ->select('estudiante.dni','estudiante.codigo','estudiante.nombres','estudiante.apellidos','estudiante.sexo','beneficiario_temp.*','escuela.siglas as escuela','tipo.descripcion as tipo')
                            ->paginate(5); 

            $tipos   = Tipo::all(); 
            $vista = view('beneficiario.parcial.tabla_beneficiarios',compact('beneficiarios','tipos'))->render();

            return response()->json([
                "vista" => $vista
            ]);  
        }  
        $datos['escuelas'] =  Escuela::all(); 
        $datos['escuela'] = Escuela::find($id_escuela);  

        return view('beneficiario.lista_escuela', $datos);
    }

    public function buscarEstudiante(Request $request)
    {
        if($request->ajax())
        {
            $beneficiario = null;
            $ficha = null;
            
            $estudiante = Estudiante::with('escuela')
                                    ->where('codigo',$request->codigo)
                                    ->first();
            if($estudiante){
                 $beneficiario = BeneficiarioTemp::where('dni_estudiante',$estudiante->dni)->select('dni_estudiante')->first();
                 $ficha = existeFicha($estudiante->dni);
            }
               
               
            return response()->json([
                'datos' => $estudiante,
                'beneficiario' => $beneficiario,
                'ficha' =>   $ficha
            ]);

        }
    }

    public function insertar(Request $request)
    {   
        if($request->ajax())
        {
            try 
            {
                DB::beginTransaction();

                $semestre     = Semestre::where('estado',1)->first();
                $estudiante   = Estudiante::find($request->dni);
                $beneficiario = BeneficiarioTemp::where('dni_estudiante',$estudiante->dni)->select('dni_estudiante')->first();

                if($semestre && $estudiante && !$beneficiario)
                {
                    $beneficiario                 = new BeneficiarioTemp();
                    $beneficiario->estado         = 1;
                    $beneficiario->dni_estudiante = $request->dni;
                    $beneficiario->id_semestre    = $semestre->id_semestre;
                    $beneficiario->id_escuela     = $estudiante->id_escuela;
                    $beneficiario->id_tipo        = BeneficiarioTemp::adicional();
                    $beneficiario->save();

                    DB::commit();
                    return response()->json([
                        'message' => 'Beneficiario registrado correctamente',
                        'status' => 'success'
                    ]);
                }
                else 
                {
                    DB::rollback();
                    return response()->json([
                        'message' => 'No se pudo realizar el registro, Asegurece qu exista un semeste activo.',
                        'status' => 'error'
                    ]);    
                }
            } catch (\Exception $th) 
            {
                DB::rollback();
                abort(500);
            }
        }
    }

    public function tablaResumen(Request $request)
    {
        if ($request->ajax()) {
            $escuelas = DB::table('escuela')
                ->join('estudiante', 'escuela.id_escuela', '=', 'estudiante.id_escuela')
                ->join('beneficiario', 'beneficiario.codigo_universitario', '=', 'estudiante.codigo_universitario')
                ->where('beneficiario.id_semestre', Session::get('id_semestre'))->select('escuela.nombre', DB::raw('COUNT(escuela.id_escuela) AS total'))->groupBy('escuela.nombre')->get();
            return response()->json(view('beneficiario.registrar', compact('escuelas')))->render();

        }

    }

    public function eliminar(Request $request)
    {
        if ($request->ajax()) 
        {
            try 
            {
                $beneficiario = BeneficiarioTemp::find($request->id_beneficiario);
                $beneficiario->delete();

                return response()->json([
                    'message' => 'Registro eliminado correctamente',
                    'status' => 'success'
                ]);    
            
            } catch (\Exception $th) 
            {
                abort(500);
            }
        }

    }

    public function cambiarTipo(Request $request)
    {
        if ($request->ajax()) 
        {
            try 
            {
                $beneficiario = BeneficiarioTemp::find($request->id_beneficiario);
                $beneficiario->id_tipo = $request->tipo;
                $beneficiario->save();

                return response()->json([
                    'message' => 'Tipo modificado correctamente',
                    'status' => 'success'
                ]);    
            
            } 
            catch (\Exception $th) 
            {
                abort(500);
            }
        }

    }

    public function suspender(Request $request, $id_beneficiario)
    {
        try 
        {
            DB::beginTransaction(); //iniciamos la transaccion
            $beneficiario = BeneficiarioTemp::find($id_beneficiario);
            if ($beneficiario) {
                $estudiante    = Estudiante::find($beneficiario->codigo_universitario);
                $cupo          = CupoProgramado::where('id_semestre', Session::get('id_semestre'))->where('id_escuela', $estudiante->id_escuela)->where('id_servicio', 2)->first();
                $cupo->ocupado = $cupo->ocupado - 1;
                $cupo->save();

                $beneficiario->observacion = $request->observacion;
                $beneficiario->estado      = "Suspendido";
                $beneficiario->save();
                DB::commit(); //confirmamos la transaccion
                return response()->json(["mensaje" => " Beneficiario suspendido correctamente !!", "parametro" => "suspendido"]);
            }
        } 
        catch (\Exception $e) 
        {
            DB::rollback(); 
            abort(500);
        }
    }

    public function listaBeneficiariosPdf(Response $response,$id_escuela)
    {

        $beneficiarios = Estudiante::join('escuela', 'escuela.id_escuela', '=', 'estudiante.id_escuela')
                            ->join('beneficiario_temp', 'beneficiario_temp.dni_estudiante', '=', 'estudiante.dni')
                            ->join('tipo', 'tipo.id_tipo', '=', 'beneficiario_temp.id_tipo')
                            ->where('beneficiario_temp.id_escuela',$id_escuela)
                            ->orderBy('tipo.id_tipo', 'DESC')
                            ->orderBy('estudiante.apellidos', 'ASC')
                            ->select('estudiante.dni','estudiante.codigo','estudiante.nombres','estudiante.apellidos','estudiante.sexo','beneficiario_temp.*','escuela.siglas as escuela','tipo.descripcion as tipo')
                            ->get(); 

        $regulares = $beneficiarios->filter(function ($beneficiario, $key) {
            return $beneficiario->id_tipo == 2;
        });

        $adicionales = $beneficiarios->filter(function ($beneficiario, $key) {
            return $beneficiario->id_tipo == 1;
        });

        $becados = $beneficiarios->filter(function ($beneficiario, $key) {
            return ($beneficiario->id_tipo != 1 &&  $beneficiario->id_tipo !=2);
        });


        $escuela = Escuela::find($id_escuela);

        $pdf = new TemplateListaBeneficiarios('P','mm','A4',$escuela);//P:vertical,L: hirizontal
    

        $pdf->AddPage();
        $pdf->AliasNbPages();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(5,116,162);//Fondo verde de celda
        $pdf->setDrawColor(234, 234, 234);//color  delos bordes
        $pdf->SetTextColor(246, 246, 246); //Letra color blanco

        $pdf->Cell(8, 5,utf8_decode('Nº'),1,0,'C',true);
        $pdf->Cell(20, 5,'DNI',1,0,'C',true);
        $pdf->Cell(20, 5,utf8_decode('CÓDIGO'),1,0,'C',true);
        $pdf->Cell(100, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
        $pdf->Cell(20, 5,'TIPO' ,1,0,'C',true);
    
        $pdf->Ln();

        $pdf->SetFont('Arial','',7);
        $pdf->SetFillColor(255, 228, 225); //Gris tenue de cada fila
        $pdf->SetTextColor(32, 32, 32); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno
        $i=1;
        foreach ($regulares as $key => $beneficiario) 
        {
            $pdf->Cell(8, 4,$i++ ,1,0,'L',$bandera);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->dni) ,1,0,'C',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->codigo) ,1,0,'C',false);
            $pdf->Cell(100, 4,utf8_decode(str_limit($beneficiario->apellidos.' '.$beneficiario->nombres,40)) ,1,0,'L',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->tipo) ,1,0,'C',false);
           
            $bandera = false; //Alterna el valor de la bandera
            $pdf->Ln();
        }
        //becados 
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(5,116,162);//Fondo verde de celda
        $pdf->setDrawColor(234, 234, 234);//color  delos bordes
        $pdf->SetTextColor(246, 246, 246); //Letra color blanco

        $pdf->Cell(8, 5,utf8_decode('Nº'),1,0,'C',true);
        $pdf->Cell(20, 5,'DNI',1,0,'C',true);
        $pdf->Cell(20, 5,utf8_decode('CÓDIGO'),1,0,'C',true);
        $pdf->Cell(100, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
        $pdf->Cell(20, 5,'TIPO' ,1,0,'C',true);
    
        $pdf->Ln();

        $pdf->SetFont('Arial','',7);
        $pdf->SetFillColor(255, 228, 225); //Gris tenue de cada fila
        $pdf->SetTextColor(32, 32, 32); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno
        $i=1;
        foreach ($becados as $key => $beneficiario) 
        {
            $pdf->Cell(8, 4,$i++ ,1,0,'L',$bandera);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->dni) ,1,0,'C',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->codigo) ,1,0,'C',false);
            $pdf->Cell(100, 4,utf8_decode(str_limit($beneficiario->apellidos.' '.$beneficiario->nombres,40)) ,1,0,'L',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->tipo) ,1,0,'C',false);
           
            $bandera = false; //Alterna el valor de la bandera
            $pdf->Ln();
        }

        //adicionales
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(5,116,162);//Fondo verde de celda
        $pdf->setDrawColor(234, 234, 234);//color  delos bordes
        $pdf->SetTextColor(246, 246, 246); //Letra color blanco

        $pdf->Cell(8, 5,utf8_decode('Nº'),1,0,'C',true);
        $pdf->Cell(20, 5,'DNI',1,0,'C',true);
        $pdf->Cell(20, 5,utf8_decode('CÓDIGO'),1,0,'C',true);
        $pdf->Cell(100, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
        $pdf->Cell(20, 5,'TIPO' ,1,0,'C',true);
    
        $pdf->Ln();

        $pdf->SetFont('Arial','',7);
        $pdf->SetFillColor(255, 228, 225); //Gris tenue de cada fila
        $pdf->SetTextColor(32, 32, 32); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno
        $i=1;
        foreach ($adicionales as $key => $beneficiario) 
        {
            $pdf->Cell(8, 4,$i++ ,1,0,'L',$bandera);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->dni) ,1,0,'C',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->codigo) ,1,0,'C',false);
            $pdf->Cell(100, 4,utf8_decode(str_limit($beneficiario->apellidos.' '.$beneficiario->nombres,40)) ,1,0,'L',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->tipo) ,1,0,'C',false);
           
            $bandera = false; //Alterna el valor de la bandera
            $pdf->Ln();
        }
    
        $headers=['Content-Type' => 'application/pdf'];

        return $response->make($pdf->Output('I',utf8_decode('Lista de beneficiarios EAP '.$escuela->nombre.'.pdf')), 200, $headers);
    }

    public function listaVentaPdf(Response $response,$id_escuela)
    {

        $beneficiarios = Estudiante::join('escuela', 'escuela.id_escuela', '=', 'estudiante.id_escuela')
                            ->join('beneficiario_temp', 'beneficiario_temp.dni_estudiante', '=', 'estudiante.dni')
                            ->join('tipo', 'tipo.id_tipo', '=', 'beneficiario_temp.id_tipo')
                            ->where('beneficiario_temp.id_escuela',$id_escuela)
                            ->orderBy('tipo.id_tipo', 'DESC')
                            ->orderBy('estudiante.apellidos', 'ASC')
                            ->select('estudiante.dni','estudiante.codigo','estudiante.nombres','estudiante.apellidos','estudiante.sexo','beneficiario_temp.*','escuela.siglas as escuela','tipo.descripcion as tipo')
                            ->get(); 
        
        $regulares = $beneficiarios->filter(function ($beneficiario, $key) {
            return $beneficiario->id_tipo == 2;
        });

        $adicionales = $beneficiarios->filter(function ($beneficiario, $key) {
            return $beneficiario->id_tipo == 1;
        });

        $becados = $beneficiarios->filter(function ($beneficiario, $key) {
            return ($beneficiario->id_tipo != 1 &&  $beneficiario->id_tipo !=2);
        });

        $escuela = Escuela::find($id_escuela);

        $pdf = new TemplateListaBeneficiarios('P','mm','A4',$escuela);//P:vertical,L: hirizontal
    
        $pdf->AddPage();
        $pdf->AliasNbPages();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(5,116,162);//Fondo verde de celda
        $pdf->setDrawColor(234, 234, 234);//color  delos bordes
        $pdf->SetTextColor(246, 246, 246); //Letra color blanco

        $pdf->Cell(8, 5,utf8_decode('Nº'),1,0,'C',true);
        $pdf->Cell(14, 5,'DNI',1,0,'C',true);
        $pdf->Cell(14, 5,utf8_decode('CÓDIGO'),1,0,'C',true);
        $pdf->Cell(60, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
        $pdf->Cell(20, 5,'TIPO' ,1,0,'C',true);
        
            $pdf->Cell(20, 5,'DESAYUNO' ,1,0,'C',true);
            $pdf->Cell(20, 5,'ALMUERZO' ,1,0,'C',true);
            $pdf->Cell(20, 5,'CENA' ,1,0,'C',true);
       
        $pdf->Ln();

        $pdf->SetFont('Arial','',7);
        $pdf->SetFillColor(255, 228, 225); //Gris tenue de cada fila
        $pdf->SetTextColor(32, 32, 32); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno
        $i=1;
        foreach ($regulares as $key => $beneficiario) 
        {
            $pdf->Cell(8, 4,$i++ ,1,0,'L',$bandera);
            $pdf->Cell(14, 4,utf8_decode($beneficiario->dni) ,1,0,'C',false);
            $pdf->Cell(14, 4,utf8_decode($beneficiario->codigo) ,1,0,'C',false);
            $pdf->Cell(60, 4,utf8_decode(str_limit($beneficiario->apellidos.' '.$beneficiario->nombres,40)) ,1,0,'L',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->tipo) ,1,0,'C',false);
          
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
            
            $bandera = false; //Alterna el valor de la bandera
            $pdf->Ln();
        }
        //becados
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(5,116,162);//Fondo verde de celda
        $pdf->setDrawColor(234, 234, 234);//color  delos bordes
        $pdf->SetTextColor(246, 246, 246); //Letra color blanco

        $pdf->Cell(8, 5,utf8_decode('Nº'),1,0,'C',true);
        $pdf->Cell(14, 5,'DNI',1,0,'C',true);
        $pdf->Cell(14, 5,utf8_decode('CÓDIGO'),1,0,'C',true);
        $pdf->Cell(60, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
        $pdf->Cell(20, 5,'TIPO' ,1,0,'C',true);
        
            $pdf->Cell(20, 5,'DESAYUNO' ,1,0,'C',true);
            $pdf->Cell(20, 5,'ALMUERZO' ,1,0,'C',true);
            $pdf->Cell(20, 5,'CENA' ,1,0,'C',true);
       
        $pdf->Ln();

        $pdf->SetFont('Arial','',7);
        $pdf->SetFillColor(255, 228, 225); //Gris tenue de cada fila
        $pdf->SetTextColor(32, 32, 32); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno
        $i=1;
        foreach ($becados as $key => $beneficiario) 
        {
            $pdf->Cell(8, 4,$i++ ,1,0,'L',$bandera);
            $pdf->Cell(14, 4,utf8_decode($beneficiario->dni) ,1,0,'C',false);
            $pdf->Cell(14, 4,utf8_decode($beneficiario->codigo) ,1,0,'C',false);
            $pdf->Cell(60, 4,utf8_decode(str_limit($beneficiario->apellidos.' '.$beneficiario->nombres,40)) ,1,0,'L',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->tipo) ,1,0,'C',false);
          
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
            
            $bandera = false; //Alterna el valor de la bandera
            $pdf->Ln();
        }
        //adicionales
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(5,116,162);//Fondo verde de celda
        $pdf->setDrawColor(234, 234, 234);//color  delos bordes
        $pdf->SetTextColor(246, 246, 246); //Letra color blanco

        $pdf->Cell(8, 5,utf8_decode('Nº'),1,0,'C',true);
        $pdf->Cell(14, 5,'DNI',1,0,'C',true);
        $pdf->Cell(14, 5,utf8_decode('CÓDIGO'),1,0,'C',true);
        $pdf->Cell(60, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
        $pdf->Cell(20, 5,'TIPO' ,1,0,'C',true);
        
            $pdf->Cell(20, 5,'DESAYUNO' ,1,0,'C',true);
            $pdf->Cell(20, 5,'ALMUERZO' ,1,0,'C',true);
            $pdf->Cell(20, 5,'CENA' ,1,0,'C',true);
       
        $pdf->Ln();

        $pdf->SetFont('Arial','',7);
        $pdf->SetFillColor(255, 228, 225); //Gris tenue de cada fila
        $pdf->SetTextColor(32, 32, 32); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno
        $i=1;
        foreach ($adicionales as $key => $beneficiario) 
        {
            $pdf->Cell(8, 4,$i++ ,1,0,'L',$bandera);
            $pdf->Cell(14, 4,utf8_decode($beneficiario->dni) ,1,0,'C',false);
            $pdf->Cell(14, 4,utf8_decode($beneficiario->codigo) ,1,0,'C',false);
            $pdf->Cell(60, 4,utf8_decode(str_limit($beneficiario->apellidos.' '.$beneficiario->nombres,40)) ,1,0,'L',false);
            $pdf->Cell(20, 4,utf8_decode($beneficiario->tipo) ,1,0,'C',false);
          
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
                $pdf->Cell(20, 4,'' ,1,0,'C',false);
            
            $bandera = false; //Alterna el valor de la bandera
            $pdf->Ln();
        }
     
    
        $headers=['Content-Type' => 'application/pdf'];

        return $response->make($pdf->Output('I',utf8_decode('Lista de beneficiarios EAP '.$escuela->nombre.'.pdf')), 200, $headers);
    }

    
}
