<?php

namespace App\Http\Controllers\back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Ficha;
use App\Model\Departamento;
use App\Model\Distrito;
use App\Model\Provincia;


class FichaController extends Controller
{
    public function index()
    {
        $data['departamentos'] = Departamento::all();

        return view('ficha.index',$data);
    }

    public function reporteProcedencia(Request $request)
    {
        if($request->ajax())
        {
            $datos['departamento'] = Departamento::find($request->id_departamento_procedencia,['nombre']);
            $datos['id_departamento'] = $request->id_departamento_procedencia;
            $datos['query'] = Ficha::procedenciaEstudiantes($datos['id_departamento'],$request->estado_matricula);
            $datos['vista'] = view('ficha.parcial.tabla_procedencia',$datos)->render();

            return response()->json([
                "datos" => $datos,
                
            ]);
        }
    }

    public function reporteModalidadIngreso(Request $request)
    {
        if($request->ajax())
        {
            $datos['query'] = Ficha::modalidadIngreso($request->estado_matricula);
            $datos['vista'] = view('ficha.parcial.tabla_modalidad',$datos)->render();

            return response()->json([
                "datos" => $datos,
            ]);
        }
    }

    public function reporteEdadPromedio(Request $request)
    {
        if($request->ajax())
        {
            $datos['query'] = Ficha::edadPromedio($request->estado_matricula);
            $datos['vista'] = view('ficha.parcial.tabla_edad',$datos)->render();

            return response()->json([
                "datos" => $datos,
            ]);
        }
    }
}
