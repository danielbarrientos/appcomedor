<?php

namespace App\Http\Controllers\back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PeriodoAtencion;
use App\Model\Escuela;
use App\Model\Semestre;
use Session;
use Carbon\Carbon;

class PeriodoAtencionController extends Controller
{
	public function __construct()
	{
		Carbon::setLocale('es');

        setlocale(LC_TIME, 'Spanish');
        
		Carbon::setUtf8(true);

	}
    public function index()
    {
        $semestre = Semestre::find(Session::get('id_semestre'),['id_semestre','descripcion','estado']);

        if ($semestre) 
        {
            $periodos=PeriodoAtencion::where('id_semestre',$semestre->id_semestre)->orderBy('id_periodoatencion', 'DESC')->paginate(15);
            
            return view('periodo.index',compact('periodos','semestre'));
        }

        return view('periodo.index');
    }

    public function insertar(Request $request)
    {
         if ($request->ajax()) 
         {

            $this->validate($request, [

                'fecha_inicio'    	=> 'bail|required|date_format:"Y-m-d"',
                'fecha_fin' 		=> 'bail|required|date_format:"Y-m-d"|after_or_equal:fecha_inicio',
                'precio_desayuno'   => 'sometimes|nullable|regex:/^\d+(?:\.\d{1,2})?$/',
                'precio_almuerzo'   => 'sometimes|nullable|regex:/^\d+(?:\.\d{1,2})?$/',
                'precio_cena'       => 'sometimes|nullable|regex:/^\d+(?:\.\d{1,2})?$/'
            ]);

            $periodo                    = new PeriodoAtencion();
            $periodo->fecha_inicio 		= $request->fecha_inicio;
            $periodo->fecha_fin         = $request->fecha_fin;
            $periodo->precio_desayuno   = $request->precio_desayuno;
            $periodo->precio_almuerzo   = $request->precio_almuerzo;
            $periodo->precio_cena       = $request->precio_cena;
            $periodo->estado           	= '1';
            $periodo->id_semestre       = Session::get('id_semestre');

            $periodo->save();

            return response()->json([
                "mensaje" => " Periodo registrado correctamente !!",
                "parametro" => "insercion"
            ]);

        }
    }
    public function actualizar(Request $request, $id_peridoatencion)
    {
    	if ($request->ajax()) {

            $this->validate($request,
             [

                'fecha_inicio'    	=> 'bail|required|date_format:"Y-m-d"',
                'fecha_fin' 		=> 'bail|required|date_format:"Y-m-d"|after:fecha_inicio',
                'precio_desayuno'   => 'sometimes|nullable|regex:/^\d+(?:\.\d{1,2})?$/',
                'precio_almuerzo'   => 'sometimes|nullable|regex:/^\d+(?:\.\d{1,2})?$/',
                'precio_cena'       => 'sometimes|nullable|regex:/^\d+(?:\.\d{1,2})?$/',

            ]);

            $periodo                    = PeriodoAtencion::find($id_peridoatencion);
            $periodo->fecha_inicio 		= $request->fecha_inicio;
            $periodo->fecha_fin         = $request->fecha_fin;
            $periodo->precio_desayuno   = $request->precio_desayuno;
            $periodo->precio_almuerzo   = $request->precio_almuerzo;
            $periodo->precio_cena       = $request->precio_cena;

            $periodo->save();

            return response()->json([
                "mensaje" => " Datos actualizados correctamente !!",
                "parametro" => "edicion"
            ]);

        }
    }
    public function cambiarEstado(Request $request)
    {
        $id_periodo=$request->id_periodo;

         $this->validate($request,[
                'id_periodo'=> 'required|numeric'
        ]);

         $periodo = PeriodoAtencion::find($id_periodo);
        
         if ($periodo) 
         {
             if ($periodo->estado=='1') {
                    $cambio='2';
             }
             if ($periodo->estado=='2') {
                    $cambio='3';
             }
             if ($periodo->estado=='3' || $periodo->estado=='4') {

                    $cambio='4';
                    $periodo->estado=$cambio;
                    $periodo->save();

                    return redirect()->back()->with('msg-success','Se cambio el estado correctamente');
             }
            $conflicto=PeriodoAtencion::where('estado',$cambio)->count();
            if ($conflicto==0) 
            {
                $periodo->estado=$cambio;
                $periodo->save();
                
                return redirect()->back()->with('msg-success','Se cambio el estado correctamente');
            }
            
            return redirect()->back()->with('msg-warning','No se pudo realizar el cambio, existe otro periodo con el estado al que desea cambiar');
         }
         
         return redirect()->back()->with('msg-warning','No se pudo realizar el cambio');
    }
}
