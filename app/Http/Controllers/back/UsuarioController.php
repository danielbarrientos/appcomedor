<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\Model\Rol;
use App\Model\Usuario;
use Illuminate\Http\Request;
use Session;

class UsuarioController extends Controller
{
    public function index(Request $request)
    {
        $roles=Rol::all();

        $nombres  = $request->get('nombres');

        if (! empty($nombres))
        {
            $usuarios = Usuario::where('nombres', "like", "%$nombres%")
                        ->orWhere('apellidos', 'like', "%$nombres%")
                        ->paginate(10);
        }
        else
        {
            $usuarios = Usuario::paginate(10);
        }

        return view('usuario.index', compact('usuarios', 'roles', 'nombres'));
    }

    public function insertar(Request $request)
    {
        if ($request->ajax())
        {
            $this->validate($request,
                [ 'nombres'   => 'required',
                  'apellidos' => 'required',
                  'email'     => 'required|email',
                  'estado'    => 'in:0,1',
                  'rol'       => 'bail|required|exists:rol,id_rol'
                ]);

            $usuario            = new Usuario();
            $usuario->nombres   = $request->nombres;
            $usuario->apellidos = $request->apellidos;
            $usuario->email     = $request->email;
            $usuario->password  = \Hash::make($request->email);
            $usuario->estado    = $request->estado;
            $usuario->id_rol    = $request->rol;
            $usuario->save();

            return response()->json([
                "mensaje" => " usuario registrado correctamente !!", 
                "parametro" => "insercion"
            ]);

        }
    }
    public function olvido(Request $request)
    {
        $usuario            = new Usuario();
        $usuario->nombres   ='daniel';
        $usuario->apellidos = 'fdsfsd';
        $usuario->email     = 'admin@admin';
        $usuario->password  = \Hash::make('12345678');
        $usuario->estado    = 1;
        $usuario->id_rol    = 1;
        $usuario->save();
        dd('usurio');
    }

    public function actualizar(Request $request, $id_usuario)
    {
        if ($request->ajax()) {

            $this->validate($request,
                ['nombres'   => 'required',
                 'apellidos' => 'required',
                 'email'     => 'required|email',
                 'estado'    => 'in:0,1',
                 'rol'       => 'bail|required|exists:rol,id_rol'
                ]);

            $usuario            = Usuario::find($id_usuario);
            $usuario->nombres   = $request->nombres;
            $usuario->apellidos = $request->apellidos;
            $usuario->email     = $request->email;
            $usuario->estado    = $request->estado;
            $usuario->id_rol    = $request->rol;
            $usuario->save();

            return response()->json([
                "mensaje" => "Datos modificados correctamente !!", 
                "parametro" => "edicion"
            ]);
        }
    }

    public function cambiarPassword(Request $request)
    {
        if ($request->isMethod('get'))
        {
            return view('usuario.cambiarClave');
        }

        if ($request->isMethod('post'))
        {
            $this->validate($request,
                ['password'          => 'required',
                    'nuevo_password' => 'required|min:8',
                    'repetir_nueva'  => 'required|same:nuevo_password',
                ],
                ['password.required'       => 'El campo contraseña es obligatorio',
                 'nuevo_password.required' => 'El campo contraseña nueva es obligatorio',
                 'repetir_nueva.required'  => 'El campo repetir contraseña es obligatorio',
                 'repetir_nueva.same'      => 'Los campos nueva contraseña y repita nueva no coinciden'
                ]);

            $usuario = Usuario::findOrFail(Session::get('id_usuario'));

            if ($usuario && \Hash::check(trim($request->password), $usuario->password))
            {
                $usuario->password = \Hash::make($request->nuevo_password);

                if ($usuario->save())
                {
                    Session::flash('msg-success', 'Contraseña cambiada correctamente !!');
                    return redirect()->back();
                }

                Session::flash('msg-error', 'Ocurrio un error, intentalo otra vez !!');
                return redirect()->back();
            }

            Session::flash('msg-error', 'La contraseña actual es incorrecta   !!');
            return redirect()->back();
        }
    }
}
