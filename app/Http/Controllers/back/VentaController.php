<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Venta;
use App\Model\PeriodoAtencion;
use App\Model\Estudiante;
use App\Model\Beneficiario;
use App\Model\CupoProgramado;
use App\Model\Escuela;
use Carbon\Carbon;
use Session;
use DB;
use Excel;
use Illuminate\Contracts\Routing\ResponseFactory as Response;
use Crabbly\FPDF\FPDF;

class VentaController extends Controller
{
    public function __construct()
    {
        Carbon::setLocale('es');

        setlocale(LC_TIME, 'Spanish');
        Carbon::setUtf8(true);

    }
    public function index()
    {

    }

    public function registrarVentaRegular(Request $request)
    {
        $periodo = PeriodoAtencion::where('estado','2')->first();

        if ($request->isMethod('get')) 
        {
            $parametro = trim($request->parametro);

            //periodo en venta (2)

            if ($parametro != null && $periodo) 
            {
                $this->validate($request, ['parametro' => 'required|digits_between:6,11']); //validamos los datos

                $columns = ['dni','codigo','nombres','apellidos','nombre_foto','id_escuela'];

                if (strlen($parametro) === 8)
                {
                     $estudiante = Estudiante::whereRaw('dni=? ', [$parametro])->select($columns)->where('matricula',1)->first();
                }
                else
                {
                	$codigo     = substr($parametro, -6);//tomamos los ultimos 6 digitos del carnet
                    $estudiante = Estudiante::whereRaw('codigo=? ', [$codigo])->select($columns)->where('matricula',1)->first();

                    if (!$estudiante) 
                    {//buscamos por codigo rfid
                     	$estudiante = Estudiante::whereRaw('codigo_rfid=? ', [$parametro])->select($columns)->where('matricula',1)->first(); //consulta a la BD
                    }
                }

                if ($estudiante) 
                {
                	//si existe en la bd
                    $beneficiario = Beneficiario::where('dni_estudiante', $estudiante->dni)->where('id_semestre', Session::get('id_semestre'))->select('tipo')->first();
                    
                    if ($beneficiario) 
                    {
                    	$serviciosPermitidos=[];
                    	$precioServicio=[1=>$periodo->precio_desayuno,2=>$periodo->precio_almuerzo,3=>$periodo->precio_cena];

                        for ($i=1; $i <=3 ; $i++) 
                        {
                    		// si ya se le vendio el cupo
                    		$cupo=$this->cupoVendido($estudiante->dni,$periodo->id_periodoatencion,$i);
                    		//verificamos si hay cupos disponibles
                    		$stock=$this->stock($i,$estudiante->id_escuela,$periodo->id_periodoatencion);

                            if ($cupo==0 && $stock>0 && !is_null($precioServicio[$i])) 
                            {
                    			//servicios q se mostraran en la vista
                    			$serviciosPermitidos[$i]=$i;
                    		}
                    	}

                        $precioServicio=$this->calcularPrecios($beneficiario->tipo,$periodo);

                    	$ruta='ventaCupo/registrarVentaRegular';
                        $tipoBeneficiario=$beneficiario->tipo;

                    	$cuposVendidos = Venta::where('id_periodoatencion',$periodo->id_periodoatencion)->where('dni_estudiante',$estudiante->dni)->select('id_venta','id_servicio','importe','fecha_registro','tipo_venta')->get();

                    	$resumenVentas = $this->resumenVentas($periodo->id_periodoatencion);

                        $escuelas = Escuela::all();

                        return view('venta.registroVentaRegular', compact('estudiante', 'parametro','cuposVendidos','serviciosPermitidos','precioServicio','escuelas','resumenVentas','ruta','tipoBeneficiario','periodo'));

                    } 
                    else 
                    {
                        return redirect('/ventaCupo/registrarVentaRegular')->with('msg-warning', 'El estudiante no pertenece a la lista de beneficiarios del semestre ' . Session::get('nombre_semestre'))->withInput($request->only('parametro'));
                    }
                } 
                else 
                {
                    return redirect('/ventaCupo/registrarVentaRegular')->with('msg-warning', 'El estudiante no se encuentra registrado en la base de datos o no se encuentra matriculado')->withInput($request->only('parametro'));
                }
            }
            else 
            {
                if ($periodo ) 
                {
                    $resumenVentas= $this->resumenVentas($periodo->id_periodoatencion);
                    
                    return view('venta.registroVentaRegular', compact('parametro','resumenVentas','periodo'));
                }

            	 return view('venta.registroVentaRegular', compact('parametro','periodo'));
            }

        } //si viene por post
        else 
        {

            $this->validate($request, [
            	'servicio.*' => 'bail|sometimes|nullable|in:1,2,3',
                'dni_estudiante' => 'bail|required|digits:8']); //validamos los datos
                
            $dni = trim($request->dni_estudiante);
            $dni = substr($dni, -8);

            try 
            {
                DB::beginTransaction(); //iniciamos la transaccion
                
                $columns = ['dni','codigo','nombres','apellidos','nombre_foto','id_escuela'];
                
                $estudiante = Estudiante::find($dni,$columns); //consulta a la BD
                
                if ($estudiante) 
                {
                    $beneficiario = Beneficiario::where('dni_estudiante', $dni)
                    				->where('id_semestre', Session::get('id_semestre'))->select('tipo')->first();
                    
                    if ($beneficiario) 
                    {
                    	$periodo=PeriodoAtencion::where('estado','2')->first();

                        $precioServicio=$this->calcularPrecios($beneficiario->tipo,$periodo);

                        foreach ($request->servicio as $servicio ) 
                        {
                            if ($servicio=='1') 
                            {
                    			$precio=$precioServicio[1];
                    		}
                            if ($servicio=='2') 
                            {
                    			$precio=$precioServicio[2];
                    		}
                            if ($servicio=='3') 
                            {
                    			$precio=$precioServicio[3];
                    		}
                            //verficamos si ya se le vendio
                    		$cupo=$this->cupoVendido($estudiante->dni,$periodo->id_periodoatencion,$servicio);

                    		if ($cupo==0) {
                    			$venta=new Venta();
                    			$venta->id_servicio=$servicio;
                    			$venta->dni_estudiante=$request->dni_estudiante;
                    			$venta->tipo_venta=1;
                    			$venta->importe=$precio;
                    			$venta->id_escuela=$estudiante->id_escuela;
                    			$venta->id_periodoatencion=$periodo->id_periodoatencion;
                    			$venta->id_usuario=Session::get('id_usuario');
                    			$venta->save();
                    		}

                    	}
                    	 DB::commit(); //confirmamos la transaccion

                        return redirect()->route('ventaRegular',['parametro' => $estudiante->dni])->with('msg-success', 'Venta realizada correctamente ');
                    } else {
                        return redirect('ventaCupo/registrarVentaRegular')->with('msg-info', 'El estudiante No se encuantra  registrado como beneficiario en el presente semestre ' . Session::get('nombre_semestre'));
                    }
                } else {

                    return redirect('ventaCupo/registrarVentaRegular')->with('msg-warning', 'El estudiante no se encuentra registrado en la base de datos')->withInput($request->only('parametro'));
                }

            }
            catch (\Exception $e) 
            {
                DB::rollback(); //si se produce algun error al insertar, restablecemos la bd a como estaba antes

                return redirect('ventaCupo/registrarVentaRegular')->with('msg-error', 'Ocurrio un error , intentelo otra vez');
            }
        }
    }

    public function registrarVentaLibre(Request $request)
    {
        //periodo en venta estado (2)
        $periodo=PeriodoAtencion::where('estado','2')->first();

        if ($request->isMethod('get')) 
        {
            $parametro = trim($request->parametro);

            $id_escuela=$request->id_escuela;
            
            if ($parametro != null && $periodo) 
            {
                $this->validate($request, [
                    'parametro' => 'required|digits_between:6,11',
                    'id_escuela'=> 'bail|sometimes|nullable|in:0,1,2,3,4,5,6,7,8'
                ]); 

                $columns = ['dni','codigo','nombres','apellidos','nombre_foto','id_escuela'];

                if (strlen($parametro)==8)
                {
                    $estudiante = Estudiante::whereRaw('dni=? ', [$parametro])->select($columns)->where('matricula',1)->first();
                }
                else
                {
                	$codigo     = substr($parametro, -6);//tomamos los ultimos 6 digitos del carnet
                    $estudiante = Estudiante::whereRaw('codigo=? ', [$codigo])->select($columns)->where('matricula',1)->first();

                    if (!$estudiante) {//buscamos por codigo rfid
                     	$estudiante = Estudiante::whereRaw('codigo_rfid=? ', [$parametro])->select($columns)->where('matricula',1)->first(); //consulta a la BD
                     }
                }

                if ($estudiante) {
                	//si existe en la bd
                	$precioServicio=[1=>$periodo->precio_desayuno,2=>$periodo->precio_almuerzo,3=>$periodo->precio_cena];

                	$serviciosPermitidos=[];
                	//$servicio=[1=>'Desayuno',2=>'Almuerzo',3=>'Cena'];

                    for ($i=1; $i <=3 ; $i++) 
                    {
                		// si ya se le vendio el cupo
                		$cupo=$this->cupoVendido($estudiante->dni,$periodo->id_periodoatencion,$i);
                		//verificamos si hay cupos disponibles
                        if ($id_escuela==0) 
                        {
                            $escuela=$estudiante->id_escuela;
                        }
                        else
                        {
                             $escuela=$id_escuela;
                        }
                        
                        $stock=$this->stock($i,$escuela,$periodo->id_periodoatencion);
                        
                        if ($cupo==0 && $stock>0 && !is_null($precioServicio[$i])) 
                        {
                			//servicios q se mostraran en la vista
                			$serviciosPermitidos[$i]=$i;
                		}
                    }
                    
                    $precioServicio = $this->calcularPrecios('R',$periodo);

                    $escuela = Escuela::find($escuela,['id_escuela','nombre']);//escuela del cupo q se desea vender

                	$ruta = 'ventaCupo/registrarVentaLibre';

                	$cuposVendidos =  Venta::where('id_periodoatencion',$periodo->id_periodoatencion)->where('dni_estudiante',$estudiante->dni)->select('id_venta','id_servicio','importe','fecha_registro')->get();

                	$resumenVentas = $this->resumenVentas($periodo->id_periodoatencion);

                    $escuelas = Escuela::pluck('nombre', 'id_escuela')->prepend('Escuela por defecto');

                    return view('venta.registroVentaLibre', compact('estudiante', 'parametro','cuposVendidos','serviciosPermitidos','precioServicio','escuelas','escuela','resumenVentas','ruta','periodo'));

                } 
                else 
                {
                    return redirect('/ventaCupo/registrarVentaLibre')->with('msg-warning', 'El estudiante no se encuentra registrado en la base de datos o no se encuentra matriculado')->withInput($request->only('parametro'));
                }
            } 
            else 
            {
                if ($periodo) 
                {
                    $resumenVentas= $this->resumenVentas($periodo->id_periodoatencion);

                    $escuelas    = Escuela::pluck('nombre', 'id_escuela')->prepend('Escuela por defecto');

                    return view('venta.registroVentaLibre', compact('parametro','resumenVentas','escuelas','periodo'));
                }

                return view('venta.registroVentaLibre', compact('parametro','periodo'));
            }

        } //si viene por post
        else 
        {
           
            $this->validate($request, [
            	'servicio.*' => 'bail|sometimes|nullable|in:1,2,3',
            	'dni_estudiante' => 'bail|required|digits:8',
                'id_escuela'=> 'bail|required|in:1,2,3,4,5,6,7,8'
            ]);
           
            $dni = trim($request->dni_estudiante);
            $dni = substr($dni, -8);

            try 
            {
                DB::beginTransaction(); //iniciamos la transaccion
                
                $columns = ['dni','codigo','nombres','apellidos','nombre_foto','id_escuela'];
                
                $estudiante = Estudiante::find($dni,$columns); //consulta a la BD
                
                if ($estudiante) 
                {
                    $periodo=PeriodoAtencion::where('estado','2')->first();
                    
                    foreach ($request->servicio as $servicio ) 
                    {
                        if ($servicio=='1') 
                        {
                			$precio=$periodo->precio_desayuno;
                		}
                        
                        if ($servicio=='2') 
                        {
                			$precio=$periodo->precio_almuerzo;
                		}
                        
                        if ($servicio=='3') 
                        {
                			$precio=$periodo->precio_cena;
                		}
                        
                        $cupo=$this->cupoVendido($estudiante->dni,$periodo->id_periodoatencion,$servicio);

                        if ($cupo==0) 
                        {
                			$venta                 = new Venta();
                			$venta->id_servicio    = $servicio;
                			$venta->dni_estudiante = $request->dni_estudiante;
                			$venta->tipo_venta     = 0;//venta libre
                			$venta->importe        = $precio;
                			$venta->id_escuela     = $request->id_escuela;
                			$venta->id_periodoatencion = $periodo->id_periodoatencion;
                			$venta->id_usuario         = Session::get('id_usuario');
                			$venta->save();
                		}

                	}
                	 DB::commit(); //confirmamos la transaccion

                    return redirect()->route('ventaLibre',['parametro' => $estudiante->dni])->with('msg-success', 'Venta realizada correctamente ');

                } else 
                {
                    return redirect('ventaCupo/registrarVentaLibre')->with('msg-warning', 'El estudiante no se encuentra registrado en la base de datos')->withInput($request->only('parametro'));
                }

            } catch (\Exception $e) 
            {
                DB::rollback(); //si se produce algun error al insertar, restablecemos la bd a como estaba antes

                return redirect('ventaCupo/registrarVentaLibre')->with('msg-error', 'Ocurrio un error , intentelo otra vez');
            }
        }
    }


    private function resumenVentas($id_periodo){
    	//$escuelas=Escuela::withCount('ventas_cupo')->get();
    	$escuelas = Escuela::select('id_escuela','nombre','siglas')->withCount([
		    'ventas_cupo AS cupos_desayuno' => function ($query) use($id_periodo) {
		        $query->where('id_servicio', 1)->where('id_periodoatencion',$id_periodo);
		    },
		    'ventas_cupo AS cupos_almuerzo' => function ($query) use($id_periodo) {
		        $query->where('id_servicio', 2)->where('id_periodoatencion',$id_periodo);
		    },
		    'ventas_cupo AS cupos_cena' => function ($query) use($id_periodo) {
		        $query->where('id_servicio', 3)->where('id_periodoatencion',$id_periodo);
		    }
		])->get();
        //filtramos : solo se dejan las escuelas q tienen alguna venta
        $filtered = $escuelas->filter(function ($escuela, $key) {
             $total=$escuela->cupos_desayuno_count+$escuela->cupos_almuerzo_count + $escuela->cupos_cena_count;
            if ($total>0){
                 return $escuela;
            }
        });

        return $filtered;
    }


    private function stock($id_servicio,$id_escuela,$id_periodoatencion){
    	//cupos asignados ala escuela
        $totalCupos= total_cupos($id_servicio,$id_escuela,Session::get('id_semestre'));
        //cupos vendidos hasta el monento
        $contador= Venta::where('id_periodoatencion',$id_periodoatencion)->where('id_servicio',$id_servicio)->where('id_escuela',$id_escuela)->count();

        $stock=$totalCupos-$contador;

        return $stock;

    }

    private function cupoVendido($dni_estudiante,$id_periodoatencion,$id_servicio)
    {
        //
    	$cupo=Venta::where('id_periodoatencion',$id_periodoatencion)->where('dni_estudiante',$dni_estudiante)->where('id_servicio',$id_servicio)->select('id_servicio')->count();

    	return $cupo;
    }

 private function importeEscuela($id_escuela,$id_servicio,$id_periodo)
 {
    //busca el importe acumulado por cada servicio
        $importe=DB::table('venta_cupo')
                ->select( DB::raw('SUM(importe) as total'))
                ->join('estudiante', 'venta_cupo.dni_estudiante', '=', 'estudiante.dni')
                ->where('estudiante.id_escuela',$id_escuela)
                ->where('venta_cupo.id_servicio',$id_servicio)
                ->where('venta_cupo.id_periodoatencion',$id_periodo)
                ->first();
                
                if ($importe->total==null) 
                {
                    return '0.00';
                }
                return $importe->total;

    }
    //funcion que calcula el precio por persona
    private function calcularPrecios($tipo_beneficio,$periodo){
         if ($tipo_beneficio=='R') { //regular
             $porcentajeDescuento=1;
        }elseif($tipo_beneficio=='A'){//beca tipo A
             $porcentajeDescuento=0;
        }
        elseif($tipo_beneficio=='B'){//beca tipo B
             $porcentajeDescuento=0.3;
        }
        elseif($tipo_beneficio=='C'){// beca tipo C
             $porcentajeDescuento=0.5;
        }

        $precio_desayuno= empty($periodo->precio_desayuno) ? 0.00 : $periodo->precio_desayuno * $porcentajeDescuento;
        $precio_almuerzo= empty($periodo->precio_almuerzo) ? 0.00 : $periodo->precio_almuerzo * $porcentajeDescuento;
        $precio_cena= empty($periodo->precio_cena) ? 0.00 : $periodo->precio_cena * $porcentajeDescuento;

        $precio_servicios=[1=>$precio_desayuno,2=>$precio_almuerzo,3=>$precio_cena];
        return $precio_servicios;

    }
    
}
