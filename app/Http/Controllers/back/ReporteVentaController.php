<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Model\Venta;
use App\Model\PeriodoAtencion;
use App\Model\Estudiante;
use App\Model\Beneficiario;
use App\Model\CupoProgramado;
use App\Model\Escuela;
use Carbon\Carbon;
use Session;
use DB;
use Excel;
use Illuminate\Contracts\Routing\ResponseFactory as Response;
//use Crabbly\FPDF\FPDF;
use \App\Pdf\HeaderListaVenta;



class ReporteVentaController extends Controller
{
  //protected $pdf;
  public function __construct()
  {
      //$this->pdf = new  HeaderListaVenta();
      Carbon::setLocale('es');

      setlocale(LC_TIME, 'Spanish');
      Carbon::setUtf8(true);
  }

  public function index(Request $request){
      if ($request->isMethod('get')) {
           $escuelas   = Escuela::all();
          $periodos   = PeriodoAtencion::where('id_semestre',Session::get('id_semestre'))->orderBy('id_periodoatencion','DESC')->select('id_periodoatencion','fecha_inicio','fecha_fin')->get();
          return view('venta.reporteVenta', compact('escuelas','periodos'));
      }
      else{
          $this->validate($request,
          ['escuela' => 'bail|required|exists:escuela,id_escuela',
           'periodo'=>'bail|required|exists:periodo_atencion,id_periodoatencion',
           'servicio'=>'bail|required|in:1,2,3']);

          $id_periodo=$request->periodo;
          $id_escuela=$request->escuela;
          $id_servicio=$request->servicio;

          $fechas=DB::select('SELECT DISTINCT DATE_FORMAT(fecha, "%Y-%m-%d") AS dia FROM atencion WHERE id_periodoatencion=?',[$id_periodo]);

          $comensales= DB::table('venta')
                  ->join('estudiante', 'venta.dni_estudiante', '=', 'estudiante.dni_estudiante')
                  ->join('periodo_atencion', 'venta.id_periodoatencion', '=', 'periodo_atencion.id_periodoatencion')
                  ->where('periodo_atencion.id_periodoatencion',$id_periodo)->where('venta.id_servicio',$id_servicio)->where('estudiante.id_escuela',$id_escuela)
                  ->select('venta.*', 'estudiante.nombres','estudiante.apellidos','estudiante.dni_estudiante')->orderBy('tipo','DESC')
                  ->get();


          $escuelas   = Escuela::all();

          $periodos   = PeriodoAtencion::where('id_semestre',Session::get('id_semestre'))->orderBy('id_periodoatencion','DESC')->select('id_periodoatencion','fecha_inicio','fecha_fin')->get();

          return view('venta.reporteVenta', compact('escuelas','periodos','comensales','fechas','id_periodo','id_escuela','id_servicio'));

      }
  }
   public function resumenVentaPdf(Request $request, Response $response){

          $this->validate($request, ['periodo'=>'bail|required|exists:periodo_atencion,id_periodoatencion']);
          $id_periodo=$request->periodo;
          $escuelas=Escuela::select('id_escuela','nombre')->get()->toArray();
          //$escuelas=$escuelas->toArray();
         $periodo=PeriodoAtencion::find($id_periodo);

         // dd($escuelas,$total);
          for ($i=0; $i <count($escuelas) ; $i++) {


              $datos[$i]=$escuelas[$i];

               $total_desayuno=$this->importeEscuela($escuelas[$i]['id_escuela'],1,$id_periodo);
               $total_almuerzo=$this->importeEscuela($escuelas[$i]['id_escuela'],2,$id_periodo);
               $total_cena=$this->importeEscuela($escuelas[$i]['id_escuela'],3,$id_periodo);

               $datos[$i]=array_add($datos[$i],'total_desayuno', $total_desayuno);
               $datos[$i]=array_add($datos[$i],'total_almuerzo', $total_almuerzo);
               $datos[$i]=array_add($datos[$i],'total_cena', $total_cena);
          }
           $pdf = new HeaderListaVenta();
          //$pdf = new FPDF('P','mm','A4');//P:vertical,L: hirizontal

          //$pdf->SetTopMargin(30.0);
          $pdf->SetMargins(15.0, 10.0, 9.0);
          $pdf->AliasNbPages();
          $pdf->AddPage();
          $titulo='RESUMEN DE VENTAS PERIODO ('.$periodo->fecha_inicio->format('d/m/Y').' Al '.$periodo->fecha_fin->format('d/m/Y').')';
          $this->titulo($pdf,$titulo);
        // $pdf->SetXY(10, 10);
          $pdf->SetFont('Arial','B',8);

          //$pdf->SetFillColor(2,157,116);//Fondo verde de celda
          $pdf->SetFillColor(2,110,200);//Fondo verde de celda
          $pdf->setDrawColor(200, 220, 255);//color  delos bordes
          $pdf->SetTextColor(240, 255, 240); //Letra color blanco

          $pdf->Cell(6, 5,'',0,0,'C',false);
          $pdf->Cell(70, 5,'',0,0,'C',false);
          $pdf->Cell(90, 5,'IMPORTES',1,0,'C',true);

          $pdf->Ln();

          $pdf->Cell(6, 5,'#',1,0,'C',true);
          $pdf->Cell(70, 5,'ESCUELA',1,0,'C',true);
          $pdf->Cell(20, 5,'DESAYUNO',1,0,'C',true);
          $pdf->Cell(20, 5,'ALMUERZO' ,1,0,'C',true);
          $pdf->Cell(20, 5,'CENA' ,1,0,'C',true);
          $pdf->Cell(30, 5,'TOTAL ' ,1,0,'C',true);
          $pdf->Ln();

          $pdf->SetFont('Arial','',7);
          $pdf->SetFillColor(200, 220, 255);
          $pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
          $bandera = false; //Para alternar el relleno
          $i=1;
          $total_importes=0.00;

          foreach ($datos as $key => $escuela) {
              $pdf->Cell(6, 5,$i++,1,0,'L',false);

              $pdf->Cell(70, 5,utf8_decode($escuela['nombre']),1,0,'L',false);
              $pdf->Cell(20, 5, ($escuela['total_desayuno']==0.00)?'-':$escuela['total_desayuno'],1,0,'R',false);
              $pdf->Cell(20, 5, ($escuela['total_almuerzo']==0.00)?'-':$escuela['total_almuerzo'] ,1,0,'R',false);
              $pdf->Cell(20, 5, ($escuela['total_cena']==0.00)?'-':$escuela['total_cena'] ,1,0,'R',false);

              $total_fila=$escuela['total_desayuno']+$escuela['total_almuerzo']+$escuela['total_cena'];
              $total_importes=$total_importes+$total_fila;

              $pdf->Cell(30, 5,($total_fila==0.00)?'-': number_format($total_fila,2,'.','') ,1,0,'R',false);
              $pdf->Ln();
          }
              $pdf->Cell(6, 5,'',0,0,'R',false);
              $pdf->Cell(70, 5,'',0,0,'R',false);
              $pdf->Cell(20, 5,'',0,0,'R',false);
              $pdf->Cell(20, 5,'' ,0,0,'R',false);
              $pdf->SetFont('Arial','B',8);
              $pdf->Cell(20, 5,utf8_decode('TOTAL S/. ') ,1,0,'R',true);

              $pdf->Cell(30, 5,number_format($total_importes,2,'.','') ,1,0,'R',false);
              $pdf->Ln();
          $headers=['Content-Type' => 'application/pdf'];

          return $response->make($pdf->Output('I'), 200, $headers);
  }
  public function resumenVentaExcel(Request $request, Response $response){

         $this->validate($request, ['periodo'=>'bail|required|exists:periodo_atencion,id_periodoatencion']);
         $id_periodo=$request->periodo;
         $escuelas=Escuela::select('id_escuela','nombre')->get()->toArray();
         //$escuelas=$escuelas->toArray();
        $periodo=PeriodoAtencion::find($id_periodo);

        // dd($escuelas,$total);
         for ($i=0; $i <count($escuelas) ; $i++) {

              $datos[$i]=$escuelas[$i];
              $total_desayuno=$this->importeEscuela($escuelas[$i]['id_escuela'],1,$id_periodo);
              $total_almuerzo=$this->importeEscuela($escuelas[$i]['id_escuela'],2,$id_periodo);
              $total_cena=$this->importeEscuela($escuelas[$i]['id_escuela'],3,$id_periodo);

              $datos[$i]=array_add($datos[$i],'total_desayuno', $total_desayuno );
              $datos[$i]=array_add($datos[$i],'total_almuerzo', $total_almuerzo);
              $datos[$i]=array_add($datos[$i],'total_cena', $total_cena);
         }
         //dd($datos);

             Excel::create('Resumen ventas'.Session::get('nombre_semestre'), function($excel) use($periodo,$datos) {

                 $excel->sheet('hoja 1', function($sheet) use($periodo,$datos) {

                     $sheet->loadView('venta.parcial.rptResumenVentaExcel',compact('periodo','datos'));
                 });
             })->download('xlsx');
 }

  public function rptListaVentaPdf(Request $request, Response $response)
  {
    $this->validate($request,
    [
      'periodo' => 'bail|required|exists:periodo_atencion,id_periodoatencion',
      'escuela' => 'bail|required|exists:escuela,id_escuela'
    ]);

    $id_periodo = $request->periodo;
    $id_escuela = $request->escuela;
    $escuela = Escuela::find($id_escuela);

    $periodo=PeriodoAtencion::find($id_periodo);


    $estudiantes = DB::table('estudiante')
            ->join('venta', 'estudiante.dni_estudiante', '=', 'venta.dni_estudiante')
            ->join('periodo_atencion', 'venta.id_periodoatencion', '=', 'periodo_atencion.id_periodoatencion')
            ->where('venta.id_escuela',$id_escuela)
            ->where('venta.id_periodoatencion',$id_periodo)
            ->select('estudiante.dni_estudiante', 'estudiante.nombres', 'estudiante.apellidos')
            ->get()->toArray();

    foreach ($estudiantes as $key => $value) {
        $estudiantes[$key] = (array)$value;

         $venta_desayuno = $this->ventaCupo($estudiantes[$key]['dni_estudiante'],1,$id_periodo);
         $venta_almuerzo = $this->ventaCupo($estudiantes[$key]['dni_estudiante'],2,$id_periodo);
         $venta_cena = $this->ventaCupo($estudiantes[$key]['dni_estudiante'],3,$id_periodo);

         $estudiantes[$key]=array_add($estudiantes[$key],'tipo_venta_desayuno', $venta_desayuno[1]);
         $estudiantes[$key]=array_add($estudiantes[$key],'tipo_venta_almuerzo', $venta_almuerzo[1]);
         $estudiantes[$key]=array_add($estudiantes[$key],'tipo_venta_cena', $venta_cena[1]);

         $estudiantes[$key]=array_add($estudiantes[$key],'importe_desayuno', $venta_desayuno[0]);
         $estudiantes[$key]=array_add($estudiantes[$key],'importe_almuerzo', $venta_almuerzo[0]);
         $estudiantes[$key]=array_add($estudiantes[$key],'importe_cena', $venta_cena[0]);
    }
    //dd($estudiantes);
    //$pdf = new FPDF('P','mm','A4');//P:vertical,L: hirizontal
      $pdf = new HeaderListaVenta();
     //$this->pdf->SetTopMargin(15.0);
   $pdf->SetMargins(9.0, 10.0, 9.0);
   $pdf->AliasNbPages();
   $pdf->AddPage();
   $titulo='LISTA DE VENTA '.$escuela->nombre.' PERIODO ('.$periodo->fecha_inicio->format('d/m/Y').' Al '.$periodo->fecha_fin->format('d/m/Y').')';
   $pdf->SetFont('Arial','B',9);
   // Movernos a la derecha

   $pdf->Cell(60);
   // Título
   $pdf->Cell(65,8,utf8_decode($titulo),0,0,'C',false);

   // Salto de línea
   $pdf->Ln();
 // $pdf->SetXY(10, 10);
   $pdf->SetFont('Arial','B',8);
   //(rojo,azul,verde)
   //$pdf->SetFillColor(2,157,116);//Fondo verde de celda
   $pdf->SetFillColor(2,110,200);//Fondo verde de celda
   $pdf->setDrawColor(200, 220, 255);//color  delos bordes
   $pdf->SetTextColor(240, 255, 240); //Letra color blanco

   $pdf->Cell(21, 5,'',0,0,'C',false);
   $pdf->Cell(70, 5,'',0,0,'C',false);
   $pdf->Cell(90, 5,'IMPORTES',1,0,'C',true);

   $pdf->Ln();

   $pdf->Cell(6, 5,'#',1,0,'C',true);
   $pdf->Cell(15, 5,'DNI',1,0,'C',true);
   $pdf->Cell(70, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
   $pdf->Cell(20, 5,'DESAYUNO',1,0,'C',true);
   $pdf->Cell(20, 5,'ALMUERZO' ,1,0,'C',true);
   $pdf->Cell(20, 5,'CENA' ,1,0,'C',true);
   $pdf->Cell(30, 5,'TOTAL ' ,1,0,'C',true);
   $pdf->Ln();

    $pdf->SetFont('Arial','',7);
   $pdf->SetFillColor(200, 220, 255);
   $pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
   $bandera = false; //Para alternar el relleno
   $i=1;
   $total_importes=0.00;
   foreach ($estudiantes as $key => $estudiante) {
       $pdf->Cell(6, 5,$i++,1,0,'L',false);

       $pdf->Cell(15, 5,utf8_decode($estudiante['dni_estudiante']),1,0,'L',false);
       $pdf->Cell(70, 5,utf8_decode($estudiante['nombres'].' '.$estudiante['apellidos']),1,0,'L',false);
       $pdf->Cell(20, 5,utf8_decode($estudiante['importe_desayuno']) ,1,0,'R',false);
       $pdf->Cell(20, 5,utf8_decode($estudiante['importe_almuerzo']) ,1,0,'R',false);
       $pdf->Cell(20, 5,utf8_decode($estudiante['importe_cena']) ,1,0,'R',false);

       $total_fila=$estudiante['importe_desayuno']+$estudiante['importe_almuerzo']+$estudiante['importe_cena'];
       $total_importes=$total_importes+$total_fila;

       $pdf->Cell(30, 5,number_format($total_fila,2,'.','') ,1,0,'R',false);
       $pdf->Ln();
   }
       $pdf->Cell(21, 5,'',0,0,'R',false);

       $pdf->Cell(70, 5,'',0,0,'R',false);
       $pdf->Cell(20, 5,'',0,0,'R',false);
       $pdf->Cell(20, 5,'' ,0,0,'R',false);
       $pdf->SetFont('Arial','B',8);
       $pdf->Cell(20, 5,utf8_decode('TOTAL S/. ') ,1,0,'R',true);

       $pdf->Cell(30, 5,number_format($total_importes,2,'.','') ,1,0,'R',false);
       $pdf->Ln();


   $headers=['Content-Type' => 'application/pdf'];

   return $response->make($pdf->Output('I'), 200, $headers);
  }

  private function resumenVentas($id_periodo){
    //$escuelas=Escuela::withCount('ventas_cupo')->get();
    $escuelas = Escuela::select('id_escuela','nombre','siglas')->withCount([
      'ventas AS cupos_desayuno' => function ($query) use($id_periodo) {
          $query->where('id_servicio', 1)->where('id_periodoatencion',$id_periodo);
      },
      'ventas AS cupos_almuerzo' => function ($query) use($id_periodo) {
          $query->where('id_servicio', 2)->where('id_periodoatencion',$id_periodo);
      },
      'ventas AS cupos_cena' => function ($query) use($id_periodo) {
          $query->where('id_servicio', 3)->where('id_periodoatencion',$id_periodo);
      }
  ])->get();
      //filtramos : solo se dejan las escuelas q tienen alguna venta
      $filtered = $escuelas->filter(function ($escuela, $key) {
           $total=$escuela->cupos_desayuno_count+$escuela->cupos_almuerzo_count + $escuela->cupos_cena_count;
          if ($total>0){
               return $escuela;
          }
      });

      return $filtered;
  }

private function importeEscuela($id_escuela,$id_servicio,$id_periodo){
  //busca el importe acumulado por cada servicio
      $importe=DB::table('venta')
              ->select( DB::raw('SUM(importe) as total'))
            //  ->join('estudiante', 'venta.dni_estudiante', '=', 'estudiante.dni_estudiante')
              //->where('estudiante.id_escuela',$id_escuela)
              ->where('venta.id_servicio',$id_servicio)
              ->where('venta.id_periodoatencion',$id_periodo)
              ->where('venta.id_escuela',$id_escuela)
              ->first();
              if ($importe->total==null) {
                  return 0.00;
              }
              return $importe->total;

  }

 //funcion que busca el importe de el estudiante por cada servicio
  private function ventaCupo($dni_estudiante,$id_servicio,$id_periodo)
  {

      $venta = Venta::where('dni_estudiante',$dni_estudiante)
                ->where('id_servicio',$id_servicio)
                ->where('id_periodoatencion',$id_periodo)
                ->select('importe','tipo_venta')->first();
      if ($venta)
      {
          return [$venta->importe,($venta->tipo_venta) ? "R" : "L"];
      }
      else{
        return [0.00,''];
      }

  }
  //funcion que calcula el precio por persona
  private function calcularPrecios($tipo_beneficio,$periodo){
       if ($tipo_beneficio=='R') { //regular
           $porcentajeDescuento=1;
      }elseif($tipo_beneficio=='A'){//beca tipo A
           $porcentajeDescuento=0;
      }
      elseif($tipo_beneficio=='B'){//beca tipo B
           $porcentajeDescuento=0.3;
      }
      elseif($tipo_beneficio=='C'){// beca tipo C
           $porcentajeDescuento=0.5;
      }

      $precio_desayuno= empty($periodo->precio_desayuno) ? 0.00 : $periodo->precio_desayuno * $porcentajeDescuento;
      $precio_almuerzo= empty($periodo->precio_almuerzo) ? 0.00 : $periodo->precio_almuerzo * $porcentajeDescuento;
      $precio_cena= empty($periodo->precio_cena) ? 0.00 : $periodo->precio_cena * $porcentajeDescuento;

      $precio_servicios=[1=>$precio_desayuno,2=>$precio_almuerzo,3=>$precio_cena];
      return $precio_servicios;

  }
   public static function titulo($pdf, $titulo=null){
     // $pdf->Image('img/brand/unamba.png',10,8,33);
       // $this->pdf->Image('img/brand/logo_comedor.png',170,8,33);
      // Arial bold 15
      $pdf->SetFont('Arial','B',9);
      // Movernos a la derecha

      $pdf->Cell(60);
      // Título
      $pdf->Cell(65,8,utf8_decode($titulo),0,0,'C',false);

      // Salto de línea
      $pdf->Ln();
  }


}
