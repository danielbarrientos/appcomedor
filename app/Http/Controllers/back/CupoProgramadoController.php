<?php

namespace App\Http\Controllers\back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CupoProgramado;
use App\Model\Escuela;
use App\Model\Servicio;
use App\Model\Semestre;
use Session;
class CupoProgramadoController extends Controller
{
    public function index()
    {	 
        
        $semestre=Semestre::find(Session::get('id_semestre'),['id_semestre','descripcion','estado']);

        if ($semestre) {

             $escuelas = Escuela::select('id_escuela','nombre')->withCount([
                'cuposprogramados AS cupo_desayuno' => function ($query) use($semestre){
                    $query->where('id_servicio', 1)->where('id_semestre',$semestre->id_semestre);
                },
                'cuposprogramados AS cupo_almuerzo' => function ($query) use($semestre) {
                    $query->where('id_servicio', 2)->where('id_semestre',$semestre->id_semestre);
                },
                'cuposprogramados AS cupo_cena' => function ($query) use($semestre){
                    $query->where('id_servicio', 3)->where('id_semestre',$semestre->id_semestre);
                }
            ])->get();

            return view('cupo.index',compact('escuelas','semestre'));
        }
            return view('cupo.index');
        
       
    }
   public function insertar( Request $request){
      
        if ($request->ajax()) {
            $this->validate($request,
                [
                'cantidad'=>'bail|required|numeric|cupo_unico:'.$request->id_servicio.','.$request->id_escuela.','.Session::get('id_semestre'),
                'id_escuela'=>'bail|required|exists:escuela,id_escuela','id_servicio'=>'required|exists:servicio,id_servicio']);
        
            $cupoprogramado = new CupoProgramado();
            $cupoprogramado->cantidad=$request->cantidad;
            $cupoprogramado->id_servicio=$request->id_servicio;
            $cupoprogramado->id_escuela=$request->id_escuela;
            $cupoprogramado->id_semestre=Session::get('id_semestre');
            $cupoprogramado->save();
            return response()->json(["mensaje"=>" Cupo registrado correctamente !!","parametro"=>"insercion"]); 
        }
        
    }
    public function actualizar( Request $request){
      
        if ($request->ajax()) {
             $this->validate($request,['cantidad'=>'bail|required|numeric','id_cupoprogramado'=>'bail|required|numeric|exists:cupo_programado,id_cupoprogramado']);
            
           $cupoprogramado = CupoProgramado::find($request->id_cupoprogramado);
            $cupoprogramado->cantidad=$request->cantidad;
            $cupoprogramado->save();
            return response()->json(["mensaje"=>" Cupo actualizado correctamente !!","parametro"=>"edicion"]);
        
            
        }
        
    }


    
    		
    
}
