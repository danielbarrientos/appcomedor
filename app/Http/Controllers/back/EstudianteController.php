<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\Model\Escuela;
use App\Model\Estudiante;
use App\Model\Ficha;
use Illuminate\Http\Request;
use Excel;
use DB;
use PDF;

class EstudianteController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $estudiantes = Estudiante::with(['escuela'])
                                    ->nombres($request->nombres)
                                    ->apellidos($request->apellidos)
                                    ->codigo($request->codigo)
                                    ->dni($request->dni)
                                    ->orderBy('codigo', 'DESC')
                                    ->orderBy('matricula', 'DESC')
                                    ->orderBy('id_escuela', 'ASC')
                                    ->paginate(15); 
            $vista = view('estudiante.parcial.tabla_estudiantes',compact('estudiantes'))->render();
            
            return response()->json([
                "vista" => $vista
            ]);                        
        }
        
        $escuelas = Escuela::all();

        return view('estudiante.index', compact('estudiantes', 'escuelas', 'parametro'));
    }

    public function buscar(Request $request,$dni)
    {
        if($request->ajax())
        {
            $estudiante = Estudiante::find($dni);
            return response()->json([
                "datos" => $estudiante
            ]);

        }
    }
    public function insertar(Request $request)
    {
        if ($request->ajax()) 
        {
            $this->validate($request, [    
                'dni'         => 'bail|required|digits:8|unique:estudiante,dni',
                'codigo'      => 'bail|sometimes|nullable|digits:6|unique:estudiante,codigo',
                'codigo_rfid' => 'bail|sometimes|nullable|max:11|unique:estudiante,codigo_rfid',
                'nombres'     => 'bail|required|max:30',
                'apellidos'   => 'bail|required|max:30',
                'matricula'   => 'bail|in:0,1',
                'escuela'     => 'required|exists:escuela,id_escuela'
            ]);

            $estudiante              = new Estudiante();
            $estudiante->dni         = $request->dni;
            $estudiante->codigo      = $request->codigo;
            $estudiante->codigo_rfid = $request->codigo_rfid;
            $estudiante->nombres     = $request->nombres;
            $estudiante->apellidos   = $request->apellidos;
            $estudiante->id_escuela  = $request->escuela;
            $estudiante->matricula   = $request->matricula;
            $estudiante->save();
            
            return response()->json([
                "mensaje" => "Estudiante registrado correctamente !!",
                "accion" => "insercion"
            ]);

        }

    }
    public function actualizar(Request $request,$dni)
    {
        if ($request->ajax()) 
        {
            $this->validate($request, [
                'dni'         => 'bail|required|nullable|digits:8|unique:estudiante,dni,' . $dni. ',dni',
                'codigo'      => 'sometimes|nullable|digits:6|unique:estudiante,codigo,' . $dni . ',dni',
                'codigo_rfid' => 'sometimes|nullable|max:11|unique:estudiante,codigo_rfid,' . $dni . ',dni',
                'nombres'     => 'bail|required|max:30',
                'apellidos'   => 'bail|required|max:30',
                'matricula'   => 'bail|in:0,1',
                'escuela'     => 'bail|required|exists:escuela,id_escuela'
            ]);

            $estudiante              = Estudiante::find($dni);
            $estudiante->codigo      = $request->codigo;
            $estudiante->dni         = $request->dni;
            $estudiante->codigo_rfid = $request->codigo_rfid;
            $estudiante->nombres     = $request->nombres;
            $estudiante->apellidos   = $request->apellidos;
            $estudiante->id_escuela  = $request->escuela;
            $estudiante->matricula   = $request->matricula;
            $estudiante->save();

            return response()->json([
                "mensaje"   => "Datos actualizados correctamente !!",
                "accion" => "edicion"
            ]);
        }

    }
     
    public function exportarExcel()
    {   
        $estudiantes=Estudiante::All();
         $datos;//arreglo de arreglos
         $i=1;
           foreach ($estudiantes as $estudiante) {
            $i++;
                $datos[]=[$estudiante->dni,$estudiante->nombres,$estudiante->apellidos,$estudiante->id_escuela,$estudiante->sexo,$estudiante->email,$estudiante->fecha_nacimiento];
            }
        Excel::create('Lista de estudiantes', function($excel) use($datos,$i)
        {
            $excel->sheet('Hoja 1', function($sheet) use($datos,$i)
            {
                $sheet->setOrientation('landscape');
                //$sheet->mergeCells('A1:E1');
                $sheet->setAutoSize(true);
                //$sheet->setHeight(1,50);

                $sheet->cells('A1:G1',function($cells){
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    //$cells->setBackground('#1497cc');
                    $cells->setBackground('#2467BA');
                    $cells->setFontColor('#ffffff');
                    $cells->setFontSize(12);
                   
                });

                $sheet->setBorder('A2:E'.$i, 'thin');//borde de todo
                $sheet->cell('A1', function($cell){$cell->setValue('DNI');});
                $sheet->cell('B1', function($cell){$cell->setValue('NOMBRES');});
                $sheet->cell('C1', function($cell){$cell->setValue('APELLIDOS');});
                $sheet->cell('D1', function($cell){$cell->setValue('ESCUELA ID');});
                $sheet->cell('E1', function($cell){$cell->setValue('SEXO');});
                $sheet->cell('F1', function($cell){$cell->setValue('EMAIL');});  
                
                $sheet->cell('G1', function($cell){$cell->setValue('FECHA NACIMIENTO');});     

               $sheet->fromArray($datos,null,'A2',false,false);//aqui se forma la matriz con los datos

                          
            });
        })->export('xls');
        
        
    }
    public function registroMasivo(Request $request){
        if ($request->isMethod('get')) {
             $escuelas   = Escuela::all();
             return view('estudiante.registroMasivo',compact('escuelas'));
         }
         else{
            try {
                 if(!$request->file("archivo"))
                {
                    return redirect('estudiante/registroMasivo')->with('msg-error', 'El archivo es requerido');
                }

                DB::beginTransaction(); //iniciamos la transaccion

                set_time_limit(180);//180m segundos como maximo
                $archivo=$request->file('archivo');
                $nombreOriginal=$archivo->getClientOriginalName();
                //$r1=\Storage::disk('archivos')->put($nombreOriginal,\File::get($archivo));
                $r1= $request->file('archivo')->move(public_path().'/archivos/', $nombreOriginal);
               
                if ($r1) {
                   
                       Excel::selectSheetsByIndex(0)->load(public_path().'/archivos/'.$nombreOriginal,function($hoja){
                     
                        $hoja->each(function($fila){
                       
                            if (strlen($fila->dni)==8) 
                            {
                                $estudiante = Estudiante::find($fila->dni);

                                if (is_null($estudiante)) 
                                {
                                    $estudiante= new Estudiante();
                                    $estudiante->dni=trim($fila->dni);
                            
                                }
                               
                                    $estudiante->codigo=trim($fila->codigo);
                                    $estudiante->nombres=trim($fila->nombres);
                                    $estudiante->apellidos=$fila->apellido_paterno.' '.$fila->apellido_materno;
                                    $estudiante->matricula=1;
                                    $estudiante->sexo = trim( ($fila->sexo==1)?'M':'F');
                                    $estudiante->id_escuela = (integer)$fila->id_escuela;
                                    $estudiante->save();
                                
                                $ficha = Ficha::where('dni_estudiante',$estudiante->dni)->first();
			
                                if(! $ficha)
                                    $ficha = new Ficha();
                                
                                $ficha->dni_estudiante = $estudiante->dni;
                                $ficha->creditos_semestre_anterior = $fila->creditos_semestre_anterior;
                                $ficha->promedio_semestre_anterior = round($fila->promedio_semestre_anterior,2);
                                $ficha->save();
                            }    
                            
                        });

                    });
                    DB::commit(); //confirmamos la transaccion
                    $request->session()->flash('msg-success',' Alumnos registrados correctamente');
                    return redirect()->back();
                }
                
            } catch (Exception $e) {
                 DB::rollback(); //si se produce algun error al insertar, restablecemos la bd a como estaba antes
                 $request->session()->flash('msg-error',' Ocurrio un error al subir el archivo, revice que el archivo tenga el formato correcto y tenga todas las columnas necesarias');
                    return redirect()->back(); 
            }
                
               
         }
    }

    public function fichaPdf($dni_estudiante)
	{
		$ficha = Ficha::where('dni_estudiante',$dni_estudiante)->first();
		$ficha = formatoDatosFicha($ficha);
		$estudiante = Estudiante::find($dni_estudiante);
		$pdf = PDF::loadView('front.parcial.ficha_pdf',compact('ficha','estudiante'));
		//return $pdf->download('ficha.pdf');
		return $pdf->stream('ficha.pdf');
    }
    
    public function historialPdf($codigo_estudiante)
	{
		return view('estudiante.archivos_externos.historial', compact('codigo_estudiante'));
	}

}
