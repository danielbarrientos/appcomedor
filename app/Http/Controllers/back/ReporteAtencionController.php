<?php

namespace App\Http\Controllers\back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Venta;
use App\Model\PeriodoAtencion;
use App\Model\Estudiante;
use App\Model\Beneficiario;
use App\Model\CupoProgramado;
use App\Model\Escuela;
use App\Model\Servicio;
use App\Model\Atencion;
use Carbon\Carbon;
use Session;
use DB;
use Excel;
use Illuminate\Contracts\Routing\ResponseFactory as Response;
use Crabbly\FPDF\FPDF;

class ReporteAtencionController extends Controller
{
  public function __construct()
  {
      Carbon::setLocale('es');

      setlocale(LC_TIME, 'Spanish');
      Carbon::setUtf8(true);

  }
  public function index(Request $request){
      if ($request->isMethod('get')) {
           $escuelas   = Escuela::all();
          $periodos   = PeriodoAtencion::where('id_semestre',Session::get('id_semestre'))->orderBy('id_periodoatencion','DESC')->select('id_periodoatencion','fecha_inicio','fecha_fin')->get();
          return view('atencion.reportePeriodoAtencion', compact('escuelas','periodos'));
      }
      /*else{
          $this->validate($request, ['escuela' => 'bail|required|exists:escuela,id_escuela',
                                      'periodo'=>'bail|required|exists:periodo_atencion,id_periodoatencion',
                                      'servicio'=>'bail|required|in:1,2,3']);
          $id_periodo=$request->periodo;
          $id_escuela=$request->escuela;
          $id_servicio=$request->servicio;

          //capturamos todas las fechas que se atendieron en el periodo
          $fechas=DB::select('SELECT DISTINCT DATE_FORMAT(fecha, "%d-%m-%Y") AS dia FROM atencion WHERE id_periodoatencion=?',[$id_periodo]);

          $comensales= DB::table('venta_cupo')
                  ->join('estudiante', 'venta_cupo.dni_estudiante', '=', 'estudiante.dni_estudiante')
                  ->join('periodo_atencion', 'venta_cupo.id_periodoatencion', '=', 'periodo_atencion.id_periodoatencion')
                  ->where('periodo_atencion.id_periodoatencion',$id_periodo)->where('venta_cupo.id_servicio',$id_servicio)->where('estudiante.id_escuela',$id_escuela)
                  ->select('venta_cupo.*', 'estudiante.nombres','estudiante.apellidos','estudiante.dni_estudiante')->orderBy('tipo_venta','DESC')
                  ->get();

          //convertimos la coleccion en array
          $comensales=$comensales->toArray();

         if (count($comensales)>0) {
              //asignamos las columnas deasistencia por cada fecha
              for ($i=0;  $i <count($comensales) ; $i++) {
                  //convertimos el objeto en array
                  $datos[$i]=(array) $comensales[$i];
                  //generamos una columna por cada dia atendido y buscamos la asistencia
                  foreach ($fechas as $fecha){
                      $datos[$i]=array_add($datos[$i], $fecha->dia, buscar_asistencia($comensales[$i]->id_servicio,$comensales[$i]->dni_estudiante,$comensales[$i]->id_periodoatencion,$fecha->dia));
                  }

              }

              $comensales=$datos;

              $escuelas   = Escuela::all();

              $periodos   = PeriodoAtencion::where('id_semestre',Session::get('id_semestre'))->orderBy('id_periodoatencion','DESC')->select('id_periodoatencion','fecha_inicio','fecha_fin')->get();

              return view('atencion.reportePeriodoAtencion', compact('escuelas','periodos','comensales','fechas','id_periodo','id_escuela','id_servicio'));
         }
         else{
              return redirect()->back()->withInput()->with('msj-info','No se encontraron datos con los parametros ingresados');
         }
      }*/
  }

  public function rptAtencionExcel(Request $request)
  {

          $this->validate($request, ['escuela' => 'bail|required|exists:escuela,id_escuela',
                                      'periodo'=>'bail|required',
                                      'servicio'=>'bail|required|in:1,2,3']);
          $id_periodo=$request->periodo;
          $id_escuela=$request->escuela;
          $id_servicio=$request->servicio;

          $escuela=Escuela::find($id_escuela,['id_escuela','nombre']);

          if ($id_periodo==0) {
              $periodos=PeriodoAtencion::whereIn('estado',[3,4])->select('id_periodoatencion')->get();
          }
          else{
              $periodos=PeriodoAtencion::whereIn('estado',[3,4])->where('id_periodoatencion',$id_periodo)->select('id_periodoatencion')->get();
          }


          if ($periodos) {
              $conjuntoFechas=[];
              $periodosNoNulos=[];
               $conjuntoComensales=[];
              foreach ($periodos as $periodo) {

                 $fechas=DB::select('SELECT DISTINCT DATE_FORMAT(fecha, "%d-%m-%Y") AS dia FROM atencion WHERE id_periodoatencion=?  AND id_servicio',[$periodo->id_periodoatencion,$id_servicio]);


                  $comensales= DB::table('venta')
                  ->join('estudiante', 'venta.dni_estudiante', '=', 'estudiante.dni_estudiante')
                  ->join('periodo_atencion', 'venta.id_periodoatencion', '=', 'periodo_atencion.id_periodoatencion')
                  ->where('periodo_atencion.id_periodoatencion',$periodo->id_periodoatencion)->where('venta.id_servicio',$id_servicio)->where('estudiante.id_escuela',$id_escuela)
                  ->select('venta.*', 'estudiante.nombres','estudiante.apellidos','estudiante.dni_estudiante')->orderBy('tipo_venta','DESC')
                  ->get();

                  //convertimos la coleccion en array
                  $comensales=$comensales->toArray();
                 if (count($comensales)>0  && count($fechas)>0) {

                  $periodosNoNulos[]=$periodo->id_periodoatencion;
                  $conjuntoFechas[]=$fechas;
                  //asignamos las columnas deasistencia por cada fecha
                      for ($i=0;  $i <count($comensales) ; $i++) {
                          //convertimos el objeto en array
                          $datos[$i]=(array) $comensales[$i];
                          //generamos una columna por cada dia atendido y buscamos la asistencia
                          foreach ($fechas as $fecha){
                              $datos[$i]=array_add($datos[$i], $fecha->dia, buscar_asistencia($comensales[$i]->id_servicio,$comensales[$i]->dni_estudiante,$comensales[$i]->id_periodoatencion,$fecha->dia));
                          }

                      }


                      $conjuntoComensales[]=$datos;
                      $datos=[];//vaciamos el array
              }

          }
          if (count($periodosNoNulos)>0) {
              //dd($periodosNoNulos,$conjuntoComensales,$conjuntoFechas);

              Excel::create('Control de Atención '.Session::get('nombre_semestre').' '.$escuela->nombre, function($excel) use($periodosNoNulos,$conjuntoComensales,$conjuntoFechas,$escuela) {

                  $excel->sheet('hoja 1', function($sheet) use($periodosNoNulos,$conjuntoComensales,$conjuntoFechas,$escuela) {

                      $sheet->loadView('atencion.parcial.rptAtencionExcel',compact('periodosNoNulos','conjuntoComensales','conjuntoFechas','escuela'));

                  });

              })->download('xlsx');


          }
          else{
              return redirect()->back()->withInput()->with('msj-info','No se encontraron datos con los parametros ingresados');
         }

      }
      else{
              return redirect()->back()->withInput()->with('msj-info','No se encontraron datos con los parametros ingresados');
         }
  }
  public function rptAtencionPdf (Request $request,Response $response)
  {
       $this->validate($request,
          [
            'escuela' => 'bail|required|exists:escuela,id_escuela',
            'periodo'=>'bail|required',
            'servicio'=>'bail|required|in:1,2,3'
          ]);
          
          $id_periodo=$request->periodo;
          $id_escuela=$request->escuela;
          $id_servicio=$request->servicio;

            $escuela=Escuela::find($id_escuela,['id_escuela','nombre']);


          if ($id_periodo==0) {
              $periodos=PeriodoAtencion::whereIn('estado',[3,4])->select('id_periodoatencion')->get();
          }
          else{
              $periodos=PeriodoAtencion::whereIn('estado',[3,4])->where('id_periodoatencion',$id_periodo)->select('id_periodoatencion')->get();
          }


          //if ($periodos) {
              $conjuntoFechas=[];
              $periodosNoNulos=[];
               $conjuntoComensales=[];
              foreach ($periodos as $periodo) {

                 $fechas=DB::select('SELECT DISTINCT DATE_FORMAT(fecha, "%d-%m-%Y") AS dia FROM atencion WHERE id_periodoatencion=? AND id_servicio=? ',[$periodo->id_periodoatencion,$id_servicio]);


                  $comensales= DB::table('venta')
                  ->join('estudiante', 'venta.dni_estudiante', '=', 'estudiante.dni_estudiante')
                  ->join('periodo_atencion', 'venta.id_periodoatencion', '=', 'periodo_atencion.id_periodoatencion')
                  ->where('periodo_atencion.id_periodoatencion',$periodo->id_periodoatencion)->where('venta.id_servicio',$id_servicio)->where('estudiante.id_escuela',$id_escuela)
                  ->select('venta.*', 'estudiante.nombres','estudiante.apellidos','estudiante.dni_estudiante')->orderBy('tipo_venta','DESC')
                  ->get();

                  //convertimos la coleccion en array
                  $comensales=$comensales->toArray();
                 if (count($comensales)>0 && count($fechas)>0) {

                  $periodosNoNulos[]=$periodo->id_periodoatencion;
                   $conjuntoFechas[]=$fechas;
                  //asignamos las columnas deasistencia por cada fecha
                      for ($i=0;  $i <count($comensales) ; $i++) {
                          $contador=0;
                          //convertimos el objeto en array
                          $datos[$i]=(array) $comensales[$i];
                          //generamos una columna por cada dia atendido y buscamos la asistencia
                          foreach ($fechas as $fecha){
                              $asistencia=buscar_asistencia($comensales[$i]->id_servicio,$comensales[$i]->dni_estudiante,$comensales[$i]->id_periodoatencion,$fecha->dia);
                              //contamos faltas
                              if ($asistencia=='-') {
                                 $contador++;
                              }

                              $datos[$i]=array_add($datos[$i], $fecha->dia, $asistencia);
                          }
                          //agregamos la columna faltas al array
                            $datos[$i]=array_add($datos[$i], 'faltas', $contador);

                      }
                      $conjuntoComensales[]=$datos;
                      $datos=[];//vaciamos el array
              }

          }
         //dd($periodosNoNulos,$conjuntoFechas,$conjuntoComensales);
        //  if (count($periodosNoNulos)>0) {

              $pdf = new FPDF('P','mm','A4');//P:vertical,L: hirizontal
            $pdf->SetTopMargin(15.0);
              $pdf->SetMargins(6.0, 10.0, 6.0);
              $pdf->AliasNbPages();

              foreach ($periodosNoNulos as $index => $periodo) {
                   $pdf->AddPage();
                  $titulo='CONTROL DE ATENCIÓN  EAP-'.$escuela->nombre.' '.Session::get('nombre_semestre');

                  $this->titulo($pdf,$titulo);

                  // $pdf->SetXY(10, 10);
                  $pdf->SetFont('Arial','B',7);

                  $pdf->SetFillColor(2,110,200);//Fondo verde de celda
                  $pdf->setDrawColor(200, 220, 255);//color  delos bordes
                  $pdf->SetTextColor(240, 255, 240); //Letra color blanco

                  $pdf->Cell(70, 5,utf8_decode('PERIODO DE ATENCIÓN  Nº').' '.($index+1) ,1,0,'C',true);
                  $pdf->Ln();

                  $pdf->Cell(6, 5,'#',1,0,'C',true);
                  $pdf->Cell(12, 5,'DNI',1,0,'C',true);
                  $pdf->Cell(42, 5,'APELLIDOS Y NOMBRES',1,0,'C',true);
                  $pdf->Cell(10, 5,'VENTA' ,1,0,'C',true);
                  foreach ($conjuntoFechas[$index] as $fecha) {
                       $pdf->Cell(8, 5,date("d/m", strtotime($fecha->dia)) ,1,0,'C',true);
                  }
                   $pdf->Cell(12, 5,'FALTAS' ,1,0,'C',true);
                   $pdf->Ln();

                  $pdf->SetFont('Arial','',6);
                 $pdf->SetFillColor(255, 228, 225); //Gris tenue de cada fila
                  $pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
                  $bandera = false; //Para alternar el relleno

                  $i=1;
                 foreach ($conjuntoComensales[$index] as $comensal) {
                      if ($comensal['faltas']>2) {
                          $bandera=true;
                      }

                      $pdf->Cell(6, 4,$i++ ,1,0,'L',$bandera);
                      $pdf->Cell(12, 4,utf8_decode($comensal['dni_estudiante']) ,1,0,'L',false);
                      $pdf->Cell(42, 4,utf8_decode(str_limit($comensal['apellidos'].' '.$comensal['nombres'],30)) ,1,0,'L',false);
                      $pdf->Cell(10, 4,utf8_decode($comensal['tipo_venta']==1) ? 'Regular':'Libre' ,1,0,'L',false);
                      //mostramos las fechas
                      foreach ($conjuntoFechas[$index] as $fecha) {
                       $pdf->Cell(8, 4,$comensal[$fecha->dia] ,1,0,'C',false);
                      }

                      $pdf->Cell(12, 4,$comensal['faltas'] ,1,0,'C',$bandera);
                      $bandera = false; //Alterna el valor de la bandera
                       $pdf->Ln();
                 }
              }


              $headers=['Content-Type' => 'application/pdf'];

              return $response->make($pdf->Output('I'), 200, $headers);


        /*  }
          else{
              return redirect()->back()->withInput()->with('msj-info','No se encontraron datos con los parametros ingresados');
         }*/

      /*}
      else{
              return redirect()->back()->withInput()->with('msj-info','No se encontraron datos con los parametros ingresados');
         }*/
  }

  private static function titulo($pdf, $titulo=null){
   // $pdf->Image('img/brand/unamba.png',10,8,33);
     // $pdf->Image('img/brand/logo_comedor.png',170,8,33);
    // Arial bold 15
    $pdf->SetFont('Arial','B',9);
    // Movernos a la derecha

    $pdf->Cell(60);
    // Título
    $pdf->Cell(65,8,utf8_decode($titulo),0,0,'C',false);

    // Salto de línea
    $pdf->Ln();
}

}
