<?php

namespace App\Http\Controllers\back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Rol;
use App\Model\Permiso;
use App\Model\PermisoRol;
use DB;

class RolController extends Controller
{
    public function index(Request $request)
    {

    	 $parametro = $request->get('parametro');
        if ($parametro != null) {
            $roles = Rol::where('nombre',$request->parametro)->orderBy('id_rol','ASC')->paginate(15);
                
        } else {

            $roles = Rol::paginate(15);
        }

        return view('rol.index', compact('roles', 'parametro'));
    	
    }

    public function registrar(Request $request)
    {
    	if ($request->isMethod('get')){
    		$permisos=Permiso::all();
    		return view('rol.registrar')->with('permisos',$permisos);	    		
    	}
    	//si viene por metodo POST
    	$this->validate($request, [
                'nombre' => 'required|unique:rol|max:30',
                'descripcion' => 'bail|sometimes|nullable|max:100|'
            ]);
    	//registramos el rol
    	try {
    		  DB::beginTransaction(); //iniciamos la transaccion
    		$rol= new Rol();
	    	$rol->nombre=$request->nombre;
	    	$rol->descripcion=$request->descripcion;
	    	$rol->save();

	    	//registramos los permisos
	    	$rol->permisos()->sync($request->permisos);
	    	 
	    	DB::commit(); //confirmamos la transaccion

	    	return redirect('/rol')->with('msj-success','Nuevo Rol registrado !!Correctamente');	
    	} catch (Exception $e) {
    		  DB::rollback(); //si se produce algun error al insertar, restablecemos la bd a como estaba antes
                
            return redirect('/rol')->with('msj-error','Ocurrio un error al registrar el rol');	    
    	}
    	

    }
    public function editar(Request $request,$id_rol)
    {
    	if ($request->isMethod('get')){
    		$permisos=Permiso::all();
    		$permisosRol=PermisoRol::where('id_rol',$id_rol)->select('id_permiso')->get();

    		//$permisosRol=$permisosRol->toArray();
    			$permisoAsignado=[];

				for ($i=0; $i < count($permisosRol) ; $i++) {
					$permisoAsignado[$permisosRol[$i]->id_permiso]=$permisosRol[$i]->id_permiso; 
				}
				
    		$rol=Rol::find($id_rol);
    		return view('rol.registrar',compact('permisos','rol','permisoAsignado'));	    		
    	}
    	//si viene por metodo POST
    	$this->validate($request, [
                'nombre' => 'required|max:30|unique:rol,id_rol,' . $id_rol. ',id_rol',
                'descripcion' => 'bail|sometimes|nullable|max:100|'
            ]);

    	$rol= Rol::findOrfail($id_rol);
    	$rol->nombre=$request->nombre;
    	$rol->descripcion=$request->descripcion;
    	$rol->save();

    	$rol->permisos()->sync($request->permisos);

    	return redirect('/rol')->with('msj-success','Nuevo Rol registrado !!Correctamente');

    }
}
