<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Estudiante;
use App\Model\Departamento;
use App\Model\Provincia;
use App\Model\Distrito;
use App\Model\Escuela;
use App\Model\Ficha;
use Session;
use DB;
use PDF;

class PanelController extends Controller
{
   	public function index(Request $request)
   	{ 
		if($request->isMethod('get'))
		{
			return view('front.index');
		}
   	}

   	public function iniciarSession(Request $request)
   	{
		$this->validate($request,[
			'codigo'=>'bail|required|digits:6',
			'dni'=>'bail|required|digits:8'
		]);
		
		$estudiante=Estudiante::whereRaw('codigo=? and dni=?',[$request->codigo,$request->dni])->first();//consulta a la BD

		
		if ($estudiante) 
		{
			$request->session()->put('dni_estudiante', $estudiante->dni);
			$request->session()->put('nombre', $estudiante->nombres);
			$request->session()->flash('msg-info','Bienvenido(a) '.$estudiante->nombres);
			
			return redirect('/fichaEstudiante');
		}
		
		Session::flash('msg-error','Datos incorrectos');
		
		return redirect()->back();
   	}

   	public function ficha(Request $request)
   	{
		$ficha = Ficha::where('dni_estudiante',Session::get('dni_estudiante'))->first();   
		if ($request->ajax()) 
		{
			//$estudiante = Estudiante::find(Session::get('dni_estudiante'));
			$estudiante = Estudiante::with('escuela')->where('dni',Session::get('dni_estudiante'))->first();

			return response()->json([
				'status' => 200,
				'estudent' => $estudiante,
				'file'     => $ficha
			]);
		}		
		if($ficha)
		{
			$composicion_familiar =json_decode($ficha->composicion_familiar);
			$nucleo_familiar =json_decode($ficha->nucleo_familiar);
		}
		
		$departamentos = Departamento::all();

		return view('front.ficha',compact('departamentos','nucleo_familiar','composicion_familiar'));
   	}

   	public function fichaTab1(Request $request)
   	{
		if($request->ajax())
		{
			$this->validate($request, [
				
				'nro_celular'        => 'bail|required|digits:9|unique:estudiante,nro_celular,' . Session::get('dni_estudiante'). ',dni',
				'email'        		 => 'bail|required|email|max:50|unique:estudiante,email,' . Session::get('dni_estudiante'). ',dni',
                'nombre_contacto' 	 => 'bail|required|max:50',
                'telefono_contacto'  => 'bail|required|digits_between:6,9',
				'distrito_vivienda'  => 'required|exists:distrito,id_distrito',
				'anexo_vivienda' 	 => 'bail|required|max:50',
				'direccion_vivienda' => 'bail|required|max:50',
			]);

			try 
			{
				\DB::beginTransaction();

				$estudiante= Estudiante::find(Session::get('dni_estudiante'));

				$estudiante->nro_celular = $request->nro_celular;
				$estudiante->email = $request->email;
				$estudiante->save();

				$ficha = Ficha::where('dni_estudiante',Session::get('dni_estudiante'))->first();
			
				if(! $ficha)
				{
					$ficha = new Ficha();
				}	

				$contacto = [];		
				$contacto['nombre_contacto'] = $request->input('nombre_contacto');
				$contacto['telefono_contacto'] = $request->input('telefono_contacto');

				$ubicacion_domicilio = [];		
				$ubicacion_domicilio['id_distrito'] = $request->input('distrito_vivienda');
				$ubicacion_domicilio['anexo_vivienda'] = $request->input('anexo_vivienda');
				$ubicacion_domicilio['direccion_vivienda'] = $request->input('direccion_vivienda');

				$ficha->dni_estudiante      = $estudiante->dni;
				$ficha->contacto_emergencia = json_encode($contacto);
				$ficha->ubicacion_domicilio = json_encode($ubicacion_domicilio);
				$ficha->save(); 

				\DB::commit();	

				return response()->json([
					'status' => 200,
					'message' => 'Datos registrados correctamente'
				]);
	
			} catch (Exception $e) 
			{
				\DB::rollback();
				abort(500);
			}
			
		}	
   	} 
	  
	public function fichaTab2(Request $request)
   	{
		if($request->ajax())
		{
			$this->validate($request, [
				'sexo'                    => 'bail|in:M,F',
				'fecha_nacimiento'  	  => 'bail|required|date',
                'estado_civil' 	          => 'bail|in:Soltero,Casado,Conviviente,Separado,Divorciado,Viudo',
				'departamento_nacimiento' => 'required|exists:departamento,id_departamento',
				'provincia_nacimiento'    => 'required|exists:provincia,id_provincia',
				'distrito_nacimiento'     => 'required|exists:distrito,id_distrito',
				'departamento_procedencia' => 'required|exists:departamento,id_departamento',
				'provincia_procedencia'    => 'required|exists:provincia,id_provincia',
				'distrito_procedencia'     => 'required|exists:distrito,id_distrito',
				'comunidad_procedencia'    => 'max:50',
				'comunidad_nacimiento'     => 'max:50',
				'primer_idioma'            => 'required|in:Castellano,Quechua,Aymara,Otro',
				'limitacion_fisica'        => 'required|in:Para ver,Para oír,Para hablar,Para usar extremidades,Otra dificultad,Ninguna',
				'vive_con'                 => 'required|in:Padres,Tios,Abuelos,Hermanos,Independiente',
				'tiempo_horas'             => 'required|numeric',
				'tiempo_minutos'           => 'required|numeric',
				'medio_transporte'         => 'required|in:Vehículo propio,Vehículo de amigos,Transporte público,Transporte de la universidad,Bicicleta,A pie,Motocicleta,Otros',
			]);

			try 
			{
				\DB::beginTransaction();

				$estudiante= Estudiante::find(Session::get('dni_estudiante'));

				$estudiante->sexo = $request->sexo;
				$estudiante->fecha_nacimiento = $request->fecha_nacimiento;
				$estudiante->estado_civil = $request->estado_civil;
				$estudiante->save();

				$ficha = Ficha::where('dni_estudiante',Session::get('dni_estudiante'))->first();
			
				if(! $ficha)
				{
					$ficha = new Ficha();
				}	

				$lugar_nacimiento = [];		
				$lugar_nacimiento['id_departamento'] = $request->input('departamento_nacimiento');
				$lugar_nacimiento['id_provincia'] = $request->input('provincia_nacimiento');
				$lugar_nacimiento['id_distrito'] = $request->input('distrito_nacimiento');
				$lugar_nacimiento['comunidad'] = $request->input('comunidad_nacimiento');

				$lugar_procedencia = [];		
				$lugar_procedencia['id_departamento'] = $request->input('departamento_procedencia');
				$lugar_procedencia['id_provincia'] = $request->input('provincia_procedencia');
				$lugar_procedencia['id_distrito'] = $request->input('distrito_procedencia');
				$lugar_procedencia['comunidad'] = $request->input('comunidad_procedencia');

				$transporte = [];		
				$transporte['tiempo_horas'] = $request->input('tiempo_horas');
				$transporte['tiempo_minutos'] = $request->input('tiempo_minutos');
				$transporte['medio_transporte'] = $request->input('medio_transporte');

				$ficha->lugar_nacimiento      = json_encode($lugar_nacimiento);
				$ficha->lugar_procedencia     = json_encode($lugar_procedencia);
				$ficha->primer_idioma         = $request->primer_idioma;
				$ficha->limitacion_fisica     = $request->limitacion_fisica;
				$ficha->vive_con              = $request->vive_con;
				$ficha->transporte 			  = json_encode($transporte);
				$ficha->save(); 

				\DB::commit();	

				return response()->json([
					'status' => 200,
					'message' => 'Datos registrados correctamente',
				
				]);
	
			} catch (\Exception $e) 
			{
				\DB::rollback();
				return response()->json([
					'status' => 500,
					'message' => $e,
				]);
				abort(500);
			}
			
		}
	}   

	public function fichaTab3(Request $request)
   	{
		if($request->ajax())
		{
			$this->validate($request, [
				
				'vivienda_tenencia' => 'bail|required|in:Propia,Alquilada,Familiares,Otros',
				'vivienda_material' => 'bail|required|in:Noble,Adobe,Madera,Otros',
				'vivienda_tipo'     => 'bail|required|in:Casa independiente,Departamento,Quinta,Cuarto solo',
				'nro_habitaciones'  => 'bail|required|in:1,2,3,4 o más',
		
			]);

			try 
			{
				\DB::beginTransaction();

				$ficha = Ficha::where('dni_estudiante',Session::get('dni_estudiante'))->first();
			
				if(! $ficha)
				{
					$ficha = new Ficha();
				}	

				$servicios = null;
				if ( is_array($request->servicios_vivienda) && count($request->servicios_vivienda)>0) 
				{
					$servicios = implode(',',$request->input('servicios_vivienda'));
				}

				$posesiones = null;
				if ( is_array($request->posesiones) && count($request->posesiones)>0) 
				{
					$posesiones = implode(',',$request->input('posesiones'));
				}
				
				$vivienda = [];		
				$vivienda['tenencia'] = $request->input('vivienda_tenencia');
				$vivienda['material'] = $request->input('vivienda_material');
				$vivienda['tipo']    = $request->input('vivienda_tipo');
				$vivienda['nro_habitaciones'] = $request->input('nro_habitaciones');

				$ficha->vivienda           = json_encode($vivienda);
				$ficha->servicios_vivienda = $servicios;
				$ficha->posesiones         = $posesiones;
				$ficha->save(); 

				\DB::commit();	

				return response()->json([
					'status' => 200,
					'message' => 'Datos registrados correctamente',
					'request' => $request->all()
				]);
	
			} catch (Exception $e) 
			{
				\DB::rollback();
				return response()->json([
					'status' => 500,
					'message' => $e,
					
				]);
				//abort(500);
			}
			
		}
	}

	public function fichaTab4(Request $request)
   	{
		if($request->ajax())
		{
			$this->validate($request, [
				'lista_composicion.*.edad' =>'required|numeric',
				'lista_composicion.*.dni' =>'nullable|digits:8',
				'lista_composicion.*.estado_civil' =>'required|in:Soltero,Casado,Conviviente,Divorciado,Viudo,Fallecido',
				'lista_composicion.*.grado_instruccion' =>'required|in:Superior universitario,Superior técnico,Secundaria,Primaria,Ninguno',
				'lista_composicion.*.parentesco' => 'bail|required|in:Padre,Madre,Hermano,Otros,Yo',
				'lista_nucleo.*.edad' =>'required|numeric',
				'lista_nucleo.*.dni' =>'nullable|digits:8',
				'lista_nucleo.*.estado_civil' =>'required|in:Soltero,Casado,Conviviente,Divorciado,Viudo,Fallecido',
				'lista_nucleo.*.grado_instruccion' =>'required|in:Superior universitario,Superior técnico,Secundaria,Primaria,Ninguno',
				'lista_gastos.*.monto' =>'required|numeric',
				'situacion_padres'   => 'bail|required|in:Ninguna,Padres viven juntos,Padres separados/divorciados,Padre/Madre privados de su libertad,Padre y Madre solteros,Padres ancianos',
				'situacion_orfandad' => 'bail|required|in:Ninguna,Huerfano de padre,Huerfano de madre,Huerfano de ambos padres',
				'victima_terrorismo' => 'bail|required|in:Si,No',
				'desayuno'           => 'bail|required|in:Hogar-Casa,Restaurant,Casa de parientes,Comedor universitario,Prepara usted,Comedor popular',
				'almuerzo'           => 'bail|required|in:Hogar-Casa,Restaurant,Casa de parientes,Comedor universitario,Prepara usted,Comedor popular',
				'cena'               => 'bail|required|in:Hogar-Casa,Restaurant,Casa de parientes,Comedor universitario,Prepara usted,Comedor popular', 
				'dependencia_economica' => 'bail|required|in:Solo del padre,Solo de la madre,De ambos padres,De usted mismo,De otro familiar',
				'ingreso_familiar'      => 'bail|required|in:Menos de 500,De 500 a 800,De 900 a 1100,De 1200 a 1500,De 1600 a 2000,Más de 2000',
				'seguro_salud'         => 'bail|required|in:Seguro privado de salud,Seguro integral de salud(SIS/AUS),EsSalud,Ninguno,Otros',
				'discapacidad'         => 'bail|required|in:Física,Mental,Sensorial,Ninguna',
				'embarazo'             => 'bail|required|in:Si,No',
				'meses_embarazo'       => 'bail|sometimes|nullable|in:1,2,3,4,5,6,7,8,9'

			],[
				'lista_composicion.*.dni.digits' =>'El campo dni debe tener 8 dígitos',
				'lista_composicion.*.numeric' =>'El campo edad debe ser un número',
				'lista_composicion.*.estado_civil.in' =>'El estado civil es incorrecto.',
				'lista_nucleo.*.dni.digits' =>'El campo dni debe tener 8 dígitos',
				'lista_nucleo.*edad.numeric' =>'El campo edad debe ser un número',
				'lista_nucleo.*.estado_civil.in' =>'El estado civil es incorrecto.'
			]);

			try 
			{
				\DB::beginTransaction();

				$ficha = Ficha::where('dni_estudiante',Session::get('dni_estudiante'))->first();
			
				if(! $ficha)
				{
					$ficha = new Ficha();
				}	

				$situacion_familiar = [];		
				$situacion_familiar['situacion_padres'] = $request->input('situacion_padres');
				$situacion_familiar['situacion_orfandad'] = $request->input('situacion_orfandad');

				$violencia_politica = [];		
				$violencia_politica['victima_terrorismo'] = $request->input('victima_terrorismo');
				$violencia_politica['resolucion_terrorismo'] = $request->input('resolucion_terrorismo');
				
				$alimentacion = [];		
				$alimentacion['desayuno'] = $request->input('desayuno');
				$alimentacion['almuerzo'] = $request->input('almuerzo');
				$alimentacion['cena'] = $request->input('cena');

				$economia_familiar = [];		
				$economia_familiar['dependencia_economica'] = $request->input('dependencia_economica');
				$economia_familiar['ingreso_familiar'] = $request->input('ingreso_familiar');

				$seguro_salud = [];		
				$seguro_salud['seguro_salud'] = $request->input('seguro_salud');
				$seguro_salud['especificacion_salud'] = $request->input('especificacion_salud');

				$discapacidad_familiar = [];		
				$discapacidad_familiar['discapacidad'] = $request->input('discapacidad');
				$discapacidad_familiar['especificacion_discapacidad'] = $request->input('especificacion_discapacidad');
				$discapacidad_familiar['carne_conadis'] = $request->input('carne_conadis');
				$discapacidad_familiar['nro_conadis'] = $request->input('nro_conadis');
				$discapacidad_familiar['resolucion_conadis'] = $request->input('resolucion_conadis');

				$embarazo = [];		
				$embarazo['embarazo'] = $request->input('embarazo');
				$embarazo['meses_embarazo'] = $request->input('meses_embarazo');
				
				//insertando datos
				$ficha->composicion_familiar   = json_encode($request->lista_composicion);
				$ficha->nucleo_familiar        = json_encode($request->lista_nucleo);
				$ficha->situacion_familiar     = json_encode($situacion_familiar);
				$ficha->violencia_politica     = json_encode($violencia_politica);
				$ficha->alimentacion           = json_encode($alimentacion);
				$ficha->economia_familiar      = json_encode($economia_familiar);
				$ficha->gastos                 = json_encode($request->lista_gastos);
				$ficha->seguro_salud           = json_encode($seguro_salud);
				$ficha->salud_familia          = json_encode($request->lista_salud);
				$ficha->discapacidad_familiar  = json_encode($discapacidad_familiar);
				$ficha->embarazo               = json_encode($embarazo);
				$ficha->save(); 

				\DB::commit();	
				
				return response()->json([
					'status' => 200,
					'message' => 'Datos registrados correctamente',
				]);
	
			} catch (Exception $e) 
			{
				\DB::rollback();
				return response()->json([
					'status' => 500,
					'message' => $e,
				]);
				//abort(500);
			}
			
		}
	}

	public function fichaTab5(Request $request)
   	{
		if($request->ajax())
		{
			$this->validate($request, [
				'semestre' => 'bail|required|in:Primero,Segundo,Tercero,Cuarto,Quinto,Sexto,Septimo,Octavo,Noveno,Décimo',
				'ultimo_promedio' => 'bail|nullable|numeric',
				'penultimo_promedio' => 'bail|nullable|numeric',
				'total_creditos'  => 'bail|nullable|numeric',
				'modalidad_ingreso'  => 'bail|required|in:Exámen ordinario,Exámen extraordinario,Cpu,Deportistas calificados,Primeros puestos,Víctimas del terrorismo,Personas con discapacidad, Otros',				
				
			]);

			try 
			{
				\DB::beginTransaction();

				$ficha = Ficha::where('dni_estudiante',Session::get('dni_estudiante'))->first();
			
				if(! $ficha)
				{
					$ficha = new Ficha();
				}	

				$antecedente_academico = [];		
				$antecedente_academico['semestre'] = $request->input('semestre');
				$antecedente_academico['ultimo_promedio'] = $request->input('ultimo_promedio');
				$antecedente_academico['penultimo_promedio'] = $request->input('penultimo_promedio');
				$antecedente_academico['total_creditos'] = $request->input('total_creditos');
				$antecedente_academico['modalidad_ingreso'] = $request->input('modalidad_ingreso');
				
				//insertando datos
			
				$ficha->antecedente_academico = json_encode($antecedente_academico);
				$ficha->save(); 

				\DB::commit();	
				
				return response()->json([
					'status' => 200,
					'message' => 'Datos registrados correctamente'
				]);
	
			} catch (Exception $e) 
			{
				\DB::rollback();
				return response()->json([
					'status' => 500,
					'message' => $e,
				
				]);
				//abort(500);
			}
			
		}
	}


	public function listProvincias(Request $request,$id_departamento)
	{
		if($request->ajax())
		{
			$provincias = Provincia::where('id_departamento',$id_departamento)->select('id_provincia AS id','nombre')->get();

			return response()->json([
				'status' => 200,
				'data' => $provincias
			]);
		}

	}

	public function listDistritos(Request $request,$id_provincia)
	{
		if($request->ajax())
		{
			$distritos = Distrito::where('id_provincia',$id_provincia)->select('id_distrito AS id','nombre')->get();

			return response()->json([
				'status' => 200,
				'data' => $distritos
			]);
		}
		
	}

	public function fichaPdf()
	{
		$ficha = Ficha::where('dni_estudiante',Session::get('dni_estudiante'))->first();
		$ficha = formatoDatosFicha($ficha);
		$estudiante = Estudiante::find(Session::get('dni_estudiante'));
		$pdf = PDF::loadView('front.parcial.ficha_pdf',compact('ficha','estudiante'));

		//return $pdf->download('ficha.pdf');
		return $pdf->stream('ficha.pdf');
	}

	// private function formatoDatosFicha($ficha)
	// {
	// 	$data = [];
		
	// 	$data['contacto'] = json_decode($ficha->contacto_emergencia);
	// 	$data['lugar_nacimiento'] = json_decode($ficha->lugar_nacimiento);
	// 	$data['lugar_procedencia'] = json_decode($ficha->lugar_procedencia);
	// 	$data['ubicacion_domicilio'] = json_decode($ficha->ubicacion_domicilio);
	// 	$data['alimentacion'] = json_decode($ficha->alimentacion);
	// 	$data['composicion_familiar'] = json_decode($ficha->composicion_familiar);
	// 	$data['discapacidad_familar'] = json_decode($ficha->discapacidad_familiar);
	// 	$data['economia_familiar'] = json_decode($ficha->economia_familiar);
	// 	$data['embarazo'] = json_decode($ficha->embarazo);
	// 	$data['gastos'] = json_decode($ficha->gastos);
	// 	$data['primer_idioma'] = $ficha->primer_idioma;
	// 	$data['limitacion_fisica'] = $ficha->limitacion_fisica;
	// 	$data['posesiones'] = $ficha->posesiones;
	// 	$data['servicios_vivienda'] = $ficha->servicios_vivienda;
	// 	$data['transporte']            = json_decode($ficha->transporte);;
	// 	$data['nucleo_familiar'] = json_decode($ficha->nucleo_familiar);
	// 	$data['composicion_familiar'] = json_decode($ficha->composicion_familiar);
	// 	$data['salud_familia'] = json_decode($ficha->salud_familia);
	// 	$data['seguro_salud'] = json_decode($ficha->seguro_salud);
	// 	$data['situacion_familiar'] = json_decode($ficha->situacion_familiar);
	// 	$data['violencia_politica'] = json_decode($ficha->violencia_politica);
	// 	$data['vive_con'] = $ficha->vive_con;
	// 	$data['vivienda'] = json_decode($ficha->vivienda);
	// 	$data['antecedente_academico'] = json_decode($ficha->antecedente_academico);
		
	// 	return $data;
	// }
}
