<?php

namespace App\Http\Middleware;

use Closure;

class PermisoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$ruta)
    {
        //usamos el helper ermiso
        if (permiso($ruta)) 
        {
             return $next($request); 
        }    
        //si no se encuentra el permiso se aborta la peticion
        abort(403);
    }
}
