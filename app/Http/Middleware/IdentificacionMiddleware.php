<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Usuario;
use Session;

class IdentificacionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
        public function handle($request, Closure $next)
        {
            
            if ($request->session()->has('id_usuario'))
            {
                return $next($request);         
            }
            
            if ($request->ajax()) 
            {
                return response()->json([
                    'status'  => 302,
                    'message' => 'Su sessión ha expirado,inicie sessión nuevamente.'
                ]);
            }
            return redirect('/iniciarSesionAdmin');
            
        }
    
}
